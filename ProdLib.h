
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the CDDLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// CDDLIB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
//#ifdef CDDLIB_EXPORTS
//#define CDDLIB_API __declspec(dllexport)
//#else
//#define CDDLIB_API __declspec(dllimport)
//#endif

#define NUMELEM      18
#define FLD_ID       0
#define FLD_TAG      1
#define FLD_TYPE     2
#define FLD_OFFSET   3
#define FLD_LENGTH   4
#define FLD_INDEXED  5
#define FLD_SORTED   6
#define FLD_ARRAYLEN 7
#define FLD_TERMS    8
#define FLD_UTERMS   9
#define FLD_GROUP    10
#define FLD_INDEX    11
#define FLD_DISPLAY  12
#define FLD_TERMFILE 13
#define FLD_NDXFILE  14
#define FLD_RECFILE  15
#define FLD_WORDFILE 16
#define FLD_MEMBEROF 17

#define CONV_ADDLF   1
#define CONV_ADDCR   2
#define CONV_ADDCRLF 3
#define CONV_REMLF   4
#define CONV_REMCR   8
#define CONV_REMCRLF 12
#define CONV_SPLIT   16
#define CONV_ASC2EBC 32
#define CONV_EBC2ASC 64
#define CONV_APPEND  128

#define TAGLEN       40
#define NAMELEN      16
#define WORD         unsigned short
#define DWORD        unsigned long

typedef struct _FldDef
{
   short iID;
   short iType;
   short iOffset;
   short iLength;
   short bIndexed;
   short bSorted;
   short iRecordArrayLength;
   short bGroup;
   short bIndex;
   short bDisplay;
   long  lTerms;
   long  lUniqueTerms;
   char  acTag[TAGLEN];
   char  acTermFile[NAMELEN];
   char  acIndexFile[NAMELEN];
   char  acRecordFile[NAMELEN];
   char  acWordFile[NAMELEN];
   char  acMemberOf[TAGLEN];
} CDA_FLDDEF;

typedef struct _MapLink
{
   long  iFldNum;
   long  iStart;
   long  iLength;
   char  acPrefix[4];
   char  acPostfix[4];
} MAPLINK;

typedef struct _TFileStat
{
   long dwFileAttributes;
   time_t ftCreationTime;
   time_t ftLastAccessTime;
   time_t ftLastWriteTime;
   //unsigned long nFileSizeHigh;
   //unsigned long nFileSizeLow;
   double nFileSize;
} FILESTAT;

int  __stdcall INIWriteFile(void);
int  __stdcall CddOpen(LPCSTR lpFilename);
int  __stdcall CddWriteFile();
int  __stdcall CddClose(void);
int  __stdcall CddGetRec(CDA_FLDDEF *lpResult, int iID, LPCSTR lpTag);
int  __stdcall CddPutRec(CDA_FLDDEF *lpResult);
int  __stdcall CddRecLen(void);
int  __stdcall CddNumFlds(void);
int  __stdcall CddNewFldList(int iNumRecs, LPCSTR lpFilename);
int  __stdcall CddBuildNdx(LPCSTR lpInfile, LPCSTR lpOutfile, long lNumRecs);
long __stdcall CddGetOffset(LPCSTR lpInfile, long lRecNum);
int  __stdcall ParseMapLink(MAPLINK *pMapLink, char *pString);
int  __stdcall ParseLink(MAPLINK *pMapLink, char *pString);
int  __stdcall ParseString(char *pString, char Delimiter, int MaxFields, char *Fields[]);
// ParseStringNQ is the same as ParseString except that it strips
// off double quote around a token.
int  __stdcall ParseStringNQ(char *pString, char Delimiter, int MaxFields, char *Fields[]);
// Special version that handle double quote within quote.  When this happen, it replaces
// the odd double quote with space.
int  __stdcall ParseStringNQ1(char *pString, char Delimiter, int MaxFields, char *Fields[]);
// ParseStringIQ is the same as ParseString except that it ignore any quote in or
// around token.  No trim for input string
int  __stdcall ParseStringIQ(char *pString, char Delimiter, int MaxFields, char *Fields[]);

void __stdcall timeString(LPCSTR strTime, time_t lTime);
void __stdcall dateString(LPCSTR strTime, time_t lTime);
__int64 __stdcall fileStat(LPCSTR lpFilename, FILESTAT *lpResult);
DWORD __stdcall GetRefPtr(LPVOID lpVoid);

WORD  __stdcall LoByte(WORD w);
WORD  __stdcall HiByte(WORD w);
WORD  __stdcall MakeWord(WORD bHi, WORD bLo);
WORD  __stdcall RShiftWord(WORD w, WORD c);
WORD  __stdcall LShiftWord(WORD w, WORD c);
WORD  __stdcall HiWord(DWORD dw);
WORD  __stdcall LoWord(DWORD dw);
DWORD __stdcall MakeDWord(DWORD wHi, DWORD wLo);
DWORD __stdcall RShiftDWord(DWORD dw, unsigned c);
DWORD __stdcall LShiftDWord(DWORD dw, unsigned c);
short __stdcall ExistFile(LPSTR lpFileName);
int   __stdcall CountBytes(LPSTR lpStr, char c);
int   __stdcall CountRecs(LPCSTR lpFilename, int iRecLen);


void __stdcall ebc2asc(unsigned char *to, unsigned char *from, unsigned int len);
void __stdcall asc2ebc(char *to, char *from, unsigned int len);
void __stdcall pd2num(char *to, char *from, int len);
int  __stdcall doConvertEx(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, 
                          int iRemBytes, int iAddMode, int iConvMode);
int  __stdcall doConvert(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, 
                          int iPadding, int iNumRecs, int iConvMode);
int  __stdcall doCopyFixed(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, int iPad);
int  __stdcall doCopy(LPCSTR strInFile, LPCSTR strOutFile);
int  __stdcall doSplit(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize, int iNumRecsPerFile);
int  __stdcall doEBC2ASCAddCR(LPCSTR strInFile, LPCSTR strOutFile, int iRecSize);
int  __stdcall doEBC2ASC(LPCSTR strInFile, LPCSTR strOutFile);
int  __stdcall doASC2EBC(LPCSTR strInFile, LPCSTR strOutFile);
int  __stdcall doAppendFix(LPCSTR strFile1, LPCSTR strFile2, LPCSTR strOutFile, int iRecSize);

int  __stdcall SN_Encrypt(LPSTR pStr, LPSTR pEncryptStr);
int  __stdcall SN_UnEncrypt(LPSTR pEncryptStr, LPSTR pStr);
int  __stdcall PQCompress(LPCSTR pCnty);

void __stdcall ParseOwnerName(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
int __stdcall  ParseOwnerName1(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast);
int  __stdcall ParseOwnerName2(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
int  __stdcall ParseOwnerName3(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
int  __stdcall ParseOwnerName4(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
int  __stdcall ParseOwnerName5(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
int  __stdcall ParseOwnerName6(LPCSTR pOwner, LPSTR pOwnerFirst, LPSTR pOwnerMiddle,
               LPSTR pOwnerLast, LPSTR pSpouseFirst, LPSTR pSpouseMiddle, LPSTR pSpouseLast,
               LPSTR pTitle=NULL);
void __stdcall MergeFF(LPCSTR pName1, LPCSTR pName2, LPSTR pMergeName);
void __stdcall MergeLF(LPCSTR pName1, LPCSTR pName2, LPSTR pMergeName);
void __stdcall Geo2Long(LPSTR strGeo, LPSTR strLong);
void __stdcall Long2Geo(LPSTR strLong, LPSTR strGeo);

int  __stdcall CountBytes(LPSTR lpStr, char c);
char* __stdcall swapName(LPCSTR pOwner, LPSTR pSwapName);
void __stdcall rTrim(char *pString);
void __stdcall lTrim(char *pString);

CDA_FLDDEF* __stdcall CddGetFldList();

// Global buffer
extern char *acLogMsg;
