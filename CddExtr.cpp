/*****************************************************************************
 * Input file:	.G01 file
 *
 *	Process extract data from G01 file 
 *
 * Options:
 *    -C : County code
 *    -D : Delimiter (comma, vertical bar, ...)
 *    -Es: Extract sale
 *    -Eg: Extract GrGr (can be use with -TBulk to generate bulk sale file)
 *    -F : Output fix length records
 *    -H : Header(Y/N) 
 *    -Q : Remove single quote from specfied fields
 *    -Nu: Don't extract unsecured parcels
 *    -T:  Extracted module
 *    -U[r|g|v|s] : input file type
 *    -Vi: Check .chk file and extract data for import
 *    -Xa: Extract Alt Apn or Fee Parcel
 *    -Xl: Extract long/lat file
 *    -Xv: Extract value file
 *    -Xe: Extract exemption code
 *    -Xf: Extract full exemption
 *    -Xd: Extract long legal description file
 *    -Xeo Extract EO,EOC,EOSC for ESRI
 *    -Xm  Extract "M" file
 *    -Xmi Extract Char file
 *    -Xo1 Extract Owner & AltApn
 *    -Xo2 Extract Owner & PrevApn
 *    -Xoc Extract Owner & Char
 *    -Xos Extract Owner & Sales
 *    -Xxc Extract extra Char fields
 *    -XAll: Extract all web bulk module
 *    -Y<yyyy>: ldr year.  Require for ITrac
 *    -Z[r]: Zip output files (option to remove existing zip file before compress)
 *
 * LDR extract: 
 *    -C<XXX> -Q -Zr -XAll -T<Bulk|WebImport>
 *
 * Normal extract: 
 *    -C<XXX> -Q -Z -Xd -T<Bulk|WebImport>
 *
 * ITRaC extract:
 *    -C<XXX> -Q -Zr -TITrac -Y<yyyy>
 *
 * Revision:
 * 6/17/2007  1.0.0    First version by Tung Bui
 * 03/10/2009 2.0.0    First version matching output with PQExtract and ready
 *                     for production.
 * 03/23/2009 2.1      Adding -Nu option (no unsecured parcels).
 * 04/29/2009 2.2      Final for production
 * 05/07/2009 2.2.1    Use store procedure spUpdAutoExtract to update state instead of
 *                     direct SQL statement.  Both CSV and fixed length are now working.
 * 05/10/2009 2.2.1.1  Fix bug when city not found.
 * 06/01/2009 2.2.2    Output Owner layout.
 * 06/02/2009 2.2.3    Add option -Z to zip output file to specified zip folder.
 * 11/23/2009 2.2.5    Remove '|' and all quotes in string or QUOTEREM fields.
 * 02/16/2010 2.2.6    Add -Xd option to extract OSCD & OD in addition to normal extract.
 * 02/25/2010 2.3.0    Fix Situs city problem by using LoadExCities() instead of LoadCities()
 *                     and use ???City.dat instead of ???City_N2C.csv
 * 03/07/2010 2.4.0    Adding option to export for SQL import.
 * 03/12/2010 2.4.1    Adding new data type YMD_XLAT to filter out bad date.
 * 04/25/2010 2.5.0    Automatically add GEOMpt for WebImport output.
 * 05/06/2010 2.5.1    Add options to add GEOMpt & SDOCDATE fields in INI file.
 * 05/19/2010 2.6      Add -Xg option to extract OSC with LAT/LONG
 *                     Remove -Au option and make STDUSE standard field
 * 05/31/2010          Add -TITrac to extract ITrac data.  Add -XAll to extract all
 *                     bulk modules.  This option should be used for LDR.
 * 06/15/2010 2.6.1    Check all files before creating zip file.
 *            2.6.2    Remove hardcoded output files by using templates defined in INI.  Add
 *                     option to remove existing zip file.
 * 06/23/2010 2.7.0    Add option to extract FeeParcel/AltApn.
 * 06/30/2010 2.8.0    Add summary file.  Change logic to "E" file to include all parcels,
 *                     not just the full exempt ones.  Also output to geo file only when
 *                     Gis_Flg='Y'.
 * 07/13/2010 2.8.1    Fix bug in createCsvRec() to correctly capture long legal, modify formatLegal().
 * 08/04/2010 2.8.2    Fix bug in formatLegal() to correct KER issue.
 * 08/09/2010 2.8.3    Add option -Xe to extract EXE_CD?.  This option is not included in -XAll.
 *                     You have to run it in addition to other options.
 * 10/19/2010 2.8.4    Add iLdrYear to allow extract non-standard input file name in prior year.
 * 11/16/2010 2.8.5    Add RecMonth to web import roll.
 * 11/29/2010 2.8.7    Add option to extract E module (full exempt) from INI file.
 *                     In county section, set FullExe=Y
 * 12/01/2010 2.8.7.1  Bug fix
 * 12/10/2010 2.8.8    Add 'E' module to zip output file.
 * 12/13/2010 2.8.8.1  Add log message.
 * 05/24/2011 2.9.0    Do not update AutoExtract flag when creating zip failed.
 * 06/03/2011 2.10.0   Modify getCntyList() to select counties from v_BulkExtr instead of Products table.
 *                     This allows user to change extract selection without changing program.
 * 06/16/2011 2.11.0   Add option to reload layout from SQL table BulkExtrLayout to 
 *                     support county specific rec layout.
 * 12/20/2011 2.12.0   Export AltApn when -Xall is specified.  Modify Usage().
 * 12/21/2011 2.13.0   Add -Xp option to extract APN_D only.
 * 01/30/2012 2.14.0   Add -X8 and -U[g|r|s] for P8 extract
 * 09/26/2012 2.15.0   Add -L option to add DocLinks to output of WebImport files
 *            2.15.0.1 Remove -L option and make AddDocLink an option in INI file.
 * 10/01/2012 2.15.0.2 Change DocLink format in addDocLinks().
 * 10/20/2012 2.15.1   Add AddDocLink option in INI file
 *                     Process new type DOCNUM_XLAT.
 * 10/27/2012 3.0.0    Add option -Vi to verify .chk file and extract for sql import.
 *                     Output will be used by ImportDB to update SQL.
 * 11/02/2012 3.0.1    Fix bug that creates Bulk OSC file name with county abbr.
 * 11/13/2012 3.0.2    Fix ITrac problem that didn't export Geompt field.
 * 11/14/2012 3.0.3    Add option to extract PQ3 data file and format APN_D.
 * 03/11/2013 3.1.0    Adding CountyInfo.cpp and CSale2Csv.cpp to support sale extract for SQL import.
 *                     Use -E{s|g} for sale or GrGr extract.
 * 04/28/2013 3.1.2    Fix bug in writeHdr2() that write the same field name twice.
 * 05/31/2013 3.1.2.1  Modify addDocDescs() & addDocLinks() to extract links for all docs
 *                     even when Doc1 is not available.
 * 08/07/2013 3.2.0    Add CARE_XLAT to extract extended CARE_OF
 * 08/29/2013 3.3.0    Add -Xoc option to extract OC file.
 * 09/25/2013 3.4.0    Add -Xxc option to extract extended CHAR XC file.
 * 10/16/2013 3.4.1    Replace memcmp() with strcmp() in GetLayoutName() to fix similar
 *                     field name issue like in BATH, BATH_1Q, ...
 * 10/18/2013 3.4.2    Remove InitProduct() since it's not being used.
 *                     Add -Xmi option to create MI module.
 * 12/02/2013 3.5.0    Rebuild with new STDCHAR layout.  Add more debug msg.
 *                     Comment out LoadLayout() since it's redundant
 * 09/22/2014 14.2.1   Remove unused MFC and change configuration for VS2010.
 *            14.2.1.1 Fix bug in _tmain() - sharing variable works in VC6 but not in vs2010.
 * 12/03/2014 14.3.0   Modify addDocLinks() to handle DocLinks extension.
 * 12/31/2014 14.4.0   Add -Xm option to create "M" file.  Also fix theapp bug by initialize MFC.
 *                     Add formatSAddr(), formatMAddr(), and formatMCtySt() to handle
 *                     SADDR_XLAT, MADDR_XLAT, and MCTYST_XLAT field type.
 * 03/14/2015 14.5.0   Modify createCsvRec() and add formatComboFlds() to support extended DOCLINK.
 *                     To use this option, just replace CHAR_XLAT with DOCLNK_XLAT for
 *                     DOCLINKS and DOCLINKX fields in Webimport.csv
 * 06/30/2015 15.0.0   Add DBA to webimport output.  Add OS module.  Remove DocDesc
 * 07/12/2015 15.0.2   Modify createCsvRec() to set RATIO=0 instead of NULL when data not available.
 * 10/19/2015 15.1.0   Add CONDITION_XLAT to translate IMPR_COND
 * 10/23/2015 15.2.0   Add O1 & O2 modules for Denise
 * 12/21/2015 15.2.1   Automatically rename ALT_APN and PREV_APN to MODAPN for O1 and O2 extract.
 * 01/11/2016 15.2.2   Add Extr4d=o1 to [KER].  This is the same as using -Xo1 option from command line.
 * 03/29/2016 15.2.3   Add bUseSfxXlat to quiet compiler.  It's not being used in this app.
 * 06/26/2016 16.0.0   Add USEC_XLAT.  If no value, set it to 999.
 * 01/12/2018 17.2.0   Add option to output OC1 & OC2 output format for Denise
 * 03/16/2018 17.3.0   Modify initCounty() to include opption to output OSC without sale data
 * 03/30/2018 17.4.0   Modify initCounty() to include opption to output OS without sale data
 * 04/24/2018 17.5.0   Add 4D extract for OSC file.
 * 02/18/2019 18.0.0   Modify initCounty() to allow webimport output LDR data to specific folder.
 * 04/12/2019 18.1.0   Modify createCsvRec() to support S_UN_XLAT & M_UN_XLAT.
 * 04/20/2019 18.2.0   Add -Xz and formatZoning(), modify createCsvRec() to support extended Zoning with ZONE_XLAT.
 * 05/17/2019 18.3.0   Add -Eg to create bulk sale file.
 *            18.3.1   Increase acLegal size from 256 to 512 in createCsvRec()
 * 06/18/2019 18.4.0   Add running option to log file.
 * 07/01/2019 19.0.0   Modify createCsvRec() to remove duplicate words in NAMES (ALL_REM)
 * 07/02/2019 19.1.0   Fix bug that blank out NAMES
 * 08/05/2019 19.2.0   Add formatFloor() and format createCsvRec() to handle FLOOR1 overflows into FLOOR2.
 *                     Modify initCounty() to allow county specific [InrecLayout].
 * 08/09/2019 19.2.1   Modify createCsvRec() to fix bug on M_UN_XLAT & S_UN_XLAT.  UnitNo is not
 *                     always left justified.  So we cannot just check first character.
 * 01/17/2020 19.2.2   Modify createCsvRec() to change the ALL_REM option not to remove dup words.
 * 03/10/2020 19.3.1   Modify -XAll to include -Xe
 * 07/23/2020 20.0.0   Add PQZONING to the end of WebImport.  Other module can add it to the ExtrLayout.
 *                     Add AddPQZ() and modify FormatZoning().
 * 10/19/2020 20.1.0   Fix 32-bit issue in INT_XLAT value.
 * 05/12/2021 20.1.2   Add LEGAL50 to extend LEGAL field to 50 bytes.
 * 06/26/2021 21.0.0   Add -Xeo to extract EO, EOC, EOSC modules for ESRI database.
 *                     Remove -Xo1 & -Xo2 options.
 * 08/23/2021 21.1.0   Add -Xgr to extract GRGR for SQL import. Add PQZoningType.
 * 09/13/2021 21.1.1   Modify addPQZ() to extract PQZoningType from G01 file.
 * 09/24/2021 21.2.0   Add addQualClass() to extract QualityClass for webimport.
 * 11/15/2021 21.2.1   Modify addPQZ() to limit PQZONING at 60 bytes long.
 * 11/30/2021 21.3.0   Modify -Xeo to add EOS output file.
 * 02/25/2022 21.4.0   Add option -Uv to extract using V01 file
 * 03/17/2022 21.5.0   Allow delimiter to be set in county section. Use -D option to override it.
 *            21.5.1   Check for delimiter in initCounty() do we can specify for individual county.  This
 *                     option is ony available for WEBIMPORT.
 * 10/15/2022 22.0.0   Add doExpApnList() and -I option to support creating bulk file based on an APN list.
 * 10/20/2022 22.0.1   Rename doExpApnList() to doExpCsvList().
 * 11/01/2022 22.0.2   Modify initCounty() to allow -Xf to run independent from -XAll
 * 12/05/2022 22.1.0   Fix createCsvRec() to keep variable APN size (RIV).
 * 01/23/2023 22.2.0   Add -TFulltext to generate import file with all code fields translated.
 *                     This option is used for SnowFlake import, no PQZ, DocLinks, or GEOMPT.
 * 03/06/2023 22.3.0   If INI file is not in current folder, use default C:\Tools\CddExre\CddExtr.ini.
 *
 *    Need to modify to move all added fields to WIExtrLayout.csv, no exception.
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "hlAdo.h"
#include "Logs.h"
#include "getopt.h"
#include "CountyInfo.h"
#include "Tables.h"
#include "Utils.h"
#include "R01.h"
#include "doZip.h"
#include "CddExtr.h"
#include "formatApn.h"
#include "CSale2Csv.h"
#include "CChr2Csv.h"

char  acRawTmpl[_MAX_PATH], acLayoutPath[_MAX_PATH];
char  acFlgTmpl[_MAX_PATH];
char  acRollFile[_MAX_PATH];
char  acLogFile[_MAX_PATH];
char  acIniFile[_MAX_PATH];
char  acCntyTbl[_MAX_PATH];
char	acOutPath[_MAX_PATH];
char	acOutFile[_MAX_PATH];
char	acZipPath[_MAX_PATH];
char	sDelim[16];             // Delimiter string
char	acOutType[32];          // Output type: [Bulk|WebImport|PQ3|ITrac]
char  acRecMonth[16];
char  acDocPath[_MAX_PATH], cDelim;
char  acApnFile[_MAX_PATH];   // File contains APN list

char  g_sOscTmpl[_MAX_PATH], g_sOTmpl[_MAX_PATH],
      //g_sO1Tmpl[_MAX_PATH], g_sO2Tmpl[_MAX_PATH], 
      //g_sOC1Tmpl[_MAX_PATH], g_sOC2Tmpl[_MAX_PATH],g_sOSC1Tmpl[_MAX_PATH], g_sOSC2Tmpl[_MAX_PATH], 
      g_sOCTmpl[_MAX_PATH],  g_sOSTmpl[_MAX_PATH], g_sP8Tmpl[_MAX_PATH], g_sExtrOpt[4], g_sSTmpl[_MAX_PATH],
      g_sEOSCTmpl[_MAX_PATH],g_sEOCTmpl[_MAX_PATH],g_sEOTmpl[_MAX_PATH];
char  g_sGeoTmpl[_MAX_PATH], g_sLglTmpl[_MAX_PATH], g_sSumTmpl[_MAX_PATH], g_sPrclTmpl[_MAX_PATH];
char  g_sValTmpl[_MAX_PATH], g_sExeTmpl[_MAX_PATH], g_sFexTmpl[_MAX_PATH], g_sAltTmpl[_MAX_PATH];
char  g_sChkTmpl[_MAX_PATH], g_sXCTmpl[_MAX_PATH], g_sMailTmpl[_MAX_PATH], g_sZoneTmpl[_MAX_PATH];


int   iRecLen, iApnLen, iCounties, iNumOsc, iNumO, iNumOC, 
      iNumEOSC, iNumOS, iNumXC, iNumVal, 
      iNumExe, iNumGeo, iNumLgl, iNumFex, iNumAlt, iNumPrcl, iNumMail, iNumZone;
      //iNumO1, iNumO2, iNumOC1, iNumOC2, iNumOSC1, iNumOSC2;
int   iCntOsc, iCntO, iCntO1, iCntO2, iCntOC, iCntOS, iCntXC, iCntMI, iCntVal, iCntExe, iCntGeo, iCntZone,
      iCntLgl, iCntFex, iCntPrcl, iCntAlt, iCntMail, iUnSec, iLdrYear, g_iMaxImport, iSqlImpType;
bool  bEnCode, bOverwriteLogfile, bDebug, bFixLength, bInclHdr, bIgnoreQuote, bNoUnsPrcls;
bool  bBulkCompatible, bWebImport, bITrac, bZip, bRemZip, bChkWebImport, bPQ3, bFullText;
bool  bCreateExe, bCreateVal, bCreateLgl, bCreateGeo, bCreateAlt, bCreateFex, bExtrAll, bCreatePrcl, 
      //bCreateO1, bCreateO2, 
      bCreateOC, bCreateOS, bCreateXC, bCreateMI, bCreateMail, bCreateZ, bCreateEO, bExtrGrgr;

// bAddGeompt is to reserve blank field for Geompt in SQL.
// bUseSfxXlat is dummy for Tables.cpp
bool  bAddGeompt, bAddRecMonth, bAuto, bAddDocLinks, bUseSfxXlat, bAddPQZ;
long  lRecCnt, lToday;
FILE  *fdApn;

COUNTY_INFO myCounty;
XREFTBL2  	altLayout [MAX_FLDS];
XREFTBL2  	oscLayout [MAX_FLDS];
XREFTBL2  	valLayout [MAX_FLDS];
XREFTBL2  	exeLayout [MAX_FLDS];
XREFTBL2  	fexLayout [MAX_FLDS];
XREFTBL2  	lglLayout [MAX_FLDS];
XREFTBL2  	geoLayout [MAX_FLDS];
XREFTBL2  	oLayout   [MAX_FLDS];
//XREFTBL2  	osc1Layout[MAX_FLDS];
//XREFTBL2  	osc2Layout[MAX_FLDS];
//XREFTBL2  	o1Layout  [MAX_FLDS];
//XREFTBL2  	o2Layout  [MAX_FLDS];
//XREFTBL2  	oc1Layout [MAX_FLDS];
//XREFTBL2  	oc2Layout [MAX_FLDS];
XREFTBL2  	ocLayout  [MAX_FLDS];
XREFTBL2  	osLayout  [MAX_FLDS];
XREFTBL2  	prclLayout[MAX_FLDS];
XREFTBL2  	xcLayout  [MAX_FLDS];
XREFTBL2  	mailLayout[MAX_FLDS];
XREFTBL2  	zoneLayout[MAX_FLDS];

CString     lstCnty[100];
INOUT_REC   lstLayout[100];

LU_ENTRY    luAirCond[MAX_AIRCOND+1];
LU_ENTRY    luPrclStat[MAX_PRCLSTAT+1];
LU_ENTRY    luCondition[MAX_CONDITION+1];
LU_ENTRY    luHeating[MAX_HEATING+1];
LU_ENTRY    luUseCode[MAX_USECODE+1];
LU_ENTRY    luQuality[MAX_QUALITY+1];
LU_ENTRY    luView[MAX_VIEW+1];
LU_ENTRY    luParkType[MAX_PARKTYPE+1];
LU_ENTRY    luPoolType[MAX_POOLTYPE+1];
LU_ENTRY    luWater[MAX_WATER+1];
LU_ENTRY    luSewer[MAX_SEWER+1];
LU_ENTRY    luDeedType[MAX_DEEDTYPE+1];
LU_ENTRY    luStrType[MAX_SUFFIX+1];
LU_ENTRY    luRoofMat[MAX_ROOFMAT+1];
LU_ENTRY    luTopo[MAX_TOPO+1];

int         aiOutFlds[_MAX_EXTRFLD]; // List of output fields
int         iOutFlds, iOutRollLen, iOutValLen, iOutFexLen, iOutExeLen, iOutLglLen, iOutZoneLen,
            iOutGeoLen, iOutAltLen, iOutOCLen, iOutOSLen, iOutXCLen, iOutPrclLen, iOutMailLen, 
            iDocDate, iDocLink, iDocLinx, iDoc1Num, iDoc2Num, iDoc3Num, iXferNum,
            iDoc1Date, iDoc2Date, iDoc3Date, iXferDate;

hlAdo       dbConn;
hlAdoRs     dbRs;
bool        bConnected;

CWinApp theApp;
using namespace std;

/******************************* getLdrCount() ******************************
 *
 * Return LDR record count.
 *
 ****************************************************************************/

int getLdrCount(LPCSTR pCntyCode)
{
   char     acProvider[256], acTmp[256];
   CString	sTmp;
   int      iRet = 0;

   try
   {
      if (!bConnected)
      {
         // open the database connection
         GetPrivateProfileString("Database", "Provider", "", acProvider, 256, acIniFile);
         LogMsg("Connecting to %s", acProvider);
         bConnected = dbConn.Connect(acProvider);
      }
   }
   AdoCatch (e)
   {
      LogMsg("***** Error connecting to DB [%s] : %s", acProvider, ComError(e));
      return -1;
   }

   if (bConnected)
   {
      try
      {
         sprintf(acTmp, "SELECT * FROM County WHERE CountyCode='%s';", pCntyCode);
         dbRs.Open(dbConn, acTmp);

         if (dbRs.next())
         {
            sTmp = dbRs.GetItem("LDRRecCount");
            if (!sTmp.IsEmpty())
               iRet = atol(sTmp);
         }
         dbRs.Close();
      }
      AdoCatch (e)
      {
         LogMsg("***** Error executing sql [%s] : %s", acTmp, ComError(e));
         iRet = -2;
      }
   }

   return iRet;
}

/*********************************** execSqlCmd *****************************
 *
 *
 ****************************************************************************/

void createSummary()
{
   char  acBuf[1024];
   FILE  *fdSum;
   int   iRet;

   LogMsg("Create summary file");
   sprintf(acBuf, g_sSumTmpl, myCounty.acCntyName);

   // Open Output file
   LogMsg("Open output file %s", acBuf);
   if (!(fdSum = fopen(acBuf, "w")))
   {
      LogMsg("***** Error creating summary file: %s", acBuf);
      return;
   }

   // Retrieve LDR record count
   iRet = getLdrCount(myCounty.acCntyCode);
   sprintf(acBuf, "Number of LDR records: %d\n", iRet);
   fputs(acBuf, fdSum);

   sprintf(acBuf, "Number of OSC records: %d\n", iCntOsc);
   fputs(acBuf, fdSum);

   if (bCreateOS)
   {
      sprintf(acBuf, "Number of OS records:  %d\n", iCntOS);
      fputs(acBuf, fdSum);
   }

   if (bCreateOC)
   {
      sprintf(acBuf, "Number of OC records:  %d\n", iCntOC);
      fputs(acBuf, fdSum);
   }

   if (bCreateXC)
   {
      sprintf(acBuf, "Number of XC records:  %d\n", iCntXC);
      fputs(acBuf, fdSum);
   }

   if (bCreateMI)
   {
      sprintf(acBuf, "Number of MI records:  %d\n", iCntMI);
      fputs(acBuf, fdSum);
   }

   if (bCreateVal)
   {
      sprintf(acBuf, "Number of V records:   %d\n", iCntVal);
      fputs(acBuf, fdSum);
   }

   if (bCreateFex)
   {
      sprintf(acBuf, "Number of E records:   %d\n", iCntFex);
      fputs(acBuf, fdSum);
   }

   if (bCreateAlt)
   {
      sprintf(acBuf, "Number of A records:   %d\n", iCntAlt);
      fputs(acBuf, fdSum);
   }

   if (bCreateLgl)
   {
      sprintf(acBuf, "Number of D records:   %d\n", iCntLgl);
      fputs(acBuf, fdSum);
   }

   if (bCreateGeo)
   {
      sprintf(acBuf, "Number of L records:   %d\n", iCntGeo);
      fputs(acBuf, fdSum);
   }

   if (bCreatePrcl)
   {
      sprintf(acBuf, "Number of P records:   %d\n", iCntPrcl);
      fputs(acBuf, fdSum);
   }

   if (bCreateZ)
   {
      sprintf(acBuf, "Number of Z records:   %d\n", iCntZone);
      fputs(acBuf, fdSum);
   }

   if (!bNoUnsPrcls && iUnSec > 0)
   {
      sprintf(acBuf, "Number of unsecured records included: %d\n", iUnSec);
      fputs(acBuf, fdSum);
   }

   fclose(fdSum);
}

/*********************************** execSqlCmd *****************************
 *
 *
 ****************************************************************************/

int execSqlCmd(LPCTSTR strCmd)
{
   int      iRet = 0;
   char     acProvider[256];
   CString  strSql = strCmd;

   try
   {
      if (!bConnected)
      {
         // open the database connection
         GetPrivateProfileString("Database", "Provider", "", acProvider, 256, acIniFile);
         LogMsg("Connecting to %s", acProvider);
         bConnected = dbConn.Connect(acProvider);
      }
      dbConn.ExecuteCommand(strSql);
   }

   AdoCatch (e)
   {
      LogMsg("***** Error executing command [%s] : %s", strSql, ComError(e));
      iRet = -3;
   }
   return iRet;
}

/******************************* getCntyList() ******************************
 *
 * Return number of counties ready for extract.
 *
 ****************************************************************************/

int getCntyList(void)
{
   char     acProvider[256], acTmp[256];
   CString	sTmp;
   int      iRet = 0;

   GetPrivateProfileString("Database", "Provider", "", acProvider, 256, acIniFile);
   try
   {
      // open the database connection
      LogMsg("Connecting to %s", acProvider);
      bConnected = dbConn.Connect(acProvider);
   }

   // catch any ADO errors
   AdoCatch(e)
   {
      LogMsg("***** SQL connect error: %s", ComError(e));
      bConnected = false;
   }

   if (bConnected)
   {
      try
      {
         sprintf(acTmp, "SELECT * FROM v_BulkExtr");
         dbRs.Open(dbConn, acTmp);

         while (dbRs.next())
         {
            sTmp = dbRs.GetItem("CountyCode");
            lstCnty[iRet] = sTmp;
            lstLayout[iRet].sCntyCode = sTmp;

            sTmp = dbRs.GetItem("InrecLayout");
            lstLayout[iRet].sInrecLayout = sTmp;
            sTmp = dbRs.GetItem("LayOutTbl");
            lstLayout[iRet].sLayOutTbl = sTmp;
            sTmp = dbRs.GetItem("OwnerTbl");
            lstLayout[iRet].sOwnerTbl = sTmp;
            sTmp = dbRs.GetItem("AltApnTbl");
            lstLayout[iRet].sAltApnTbl = sTmp;
            sTmp = dbRs.GetItem("ValueTbl");
            lstLayout[iRet].sValueTbl = sTmp;
            sTmp = dbRs.GetItem("ExemptTbl");
            lstLayout[iRet].sExemptTbl = sTmp;
            sTmp = dbRs.GetItem("FullExemptTbl");
            lstLayout[iRet].sFullExemptTbl = sTmp;
            sTmp = dbRs.GetItem("LegalTbl");
            lstLayout[iRet].sLegalTbl = sTmp;
            sTmp = dbRs.GetItem("GeoTbl");
            lstLayout[iRet].sGeoTbl = sTmp;
            sTmp = dbRs.GetItem("OCTbl");
            lstLayout[iRet].sOCTbl = sTmp;
            sTmp = dbRs.GetItem("XCTbl");
            lstLayout[iRet].sXCTbl = sTmp;

            iRet++;
         }
         dbRs.Close();
      }
      AdoCatch (e)
      {
         LogMsg("***** Error accessing sql [%s] : %s", acTmp, ComError(e));
      }
   }

   return iRet;
}

/********************************* getDate() ********************************
 *
 * Return current date + number of extended day.
 *
 ****************************************************************************/

void getDate(char *pDate, long lExtend)
{
   CString strTmp;

   CTime today = CTime::GetCurrentTime();
   CTimeSpan tsExtend(lExtend);

   today += tsExtend;
   strcpy(pDate, today.Format("%Y%m%d"));
}

/******************************* LoadLkupTables() ****************************
 *
 * Return 1 if error.  Otherwise return 0
 *
 *****************************************************************************/

int LoadLkupTables(LPSTR pLkupFile)
{
   int iRet;

   iRet = LoadLUTable(pLkupFile, "[A/C]", &luAirCond[0], MAX_AIRCOND);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [A/C] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Parcel Status]", &luPrclStat[0], MAX_PRCLSTAT);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Parcel Status] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Condition]", &luCondition[0], MAX_CONDITION);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Condition] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Heating]", &luHeating[0], MAX_HEATING);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Heating] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Use Description]", &luUseCode[0], MAX_USECODE);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Use Description] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[SiteInfluence]", &luView[0], MAX_VIEW);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [SiteInfluence] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Parking Type]", &luParkType[0], MAX_PARKTYPE);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Parking Type] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Pool Code]", &luPoolType[0], MAX_POOLTYPE);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Pool Code] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Quality]", &luQuality[0], MAX_QUALITY);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Quality] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Water Service]", &luWater[0], MAX_WATER);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Water Service] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Sewer Service]", &luSewer[0], MAX_SEWER);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Sewer Service] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Deed Type Index]", &luDeedType[0], MAX_DEEDTYPE);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Deed Type Index] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Street Type Index]", &luStrType[0], MAX_SUFFIX);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Street Type Index] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Roof Material]", &luRoofMat[0], MAX_ROOFMAT);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Roof Material] in %s", pLkupFile);
      return 1;
   }
   iRet = LoadLUTable(pLkupFile, "[Topo]", &luTopo[0], MAX_TOPO);
   if (!iRet)
   {
      LogMsg("*** Error Looking for table [Topo] in %s", pLkupFile);
      return 1;
   }

   return 0;
}

/******************************* GetLayoutName() ****************************
 *
 * Return the index of field name.
 *
 ****************************************************************************/

int GetLayoutName(char *pName)
{
   int iTmp, iIdx=-1;

   for (iTmp = 0; iTmp <= _MAX_EXTRFLD && extrFlds[iTmp].fldName; iTmp++)
   {
      if (!strcmp(pName, extrFlds[iTmp].fldName))
      {
         iIdx = iTmp;
         break;
      }
   }
   return iIdx;
}

/****************************** LoadPQExtrLayout *****************************
 *
 *
 *****************************************************************************/

int LoadPQExtrLayout(LPCSTR pLayoutTbl, XREFTBL2 *pLayout, int *iMaxLen)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_LO_FLDS];
   int   iRet=0, iTmp, iLen;
   FILE  *fd;

   iLen = 0;
   LogMsg("Loading extract layout: %s", pLayoutTbl);
   fd = fopen(pLayoutTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            iTmp = ParseString(myTrim(acTmp), ',', MAX_LO_FLDS, pFlds);
            if (iTmp > 0)
            {
               pLayout->iOffset = iLen;
               pLayout->iIdx = GetLayoutName(pFlds[FLD_NAME]);
               strcpy(pLayout->acName, pFlds[FLD_NAME]);
               strcpy(pLayout->acNameDesc, pFlds[FLD_NAME_DESC]);
               pLayout->iMaxLen = atoi(pFlds[FLD_MAXLEN]);
               iLen += pLayout->iMaxLen;
               iRet++;
               pLayout++;
            } else
            {
               LogMsg("***** Bad layout table file: %s", pLayoutTbl);
            }
         }
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening layout table %s", pLayoutTbl);

   *iMaxLen = iLen;
   return iRet;
}

/****************************** LoadInputRecLayout ***************************
 *
 *
 *****************************************************************************/

int GetXlatType(LPCSTR pStr)
{
   int   iRet=-1, iTmp=0;

   while (XlatType[iTmp].Value >= 0)
   {
      if (!strcmp(pStr, XlatType[iTmp].Name))
      {
         iRet = XlatType[iTmp].Value;
         break;
      }
      iTmp++;
   }

   return iRet;
}

int LoadInputRecLayout(LPCSTR pLayoutTbl, EXTRFLD *pLayout, int *iMaxLen)
{
   char  acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_LO_FLDS];
   int   iRet=0, iTmp, iLen;
   FILE  *fd;

   iLen = 0;
   LogMsg("Loading input record layout: %s", pLayoutTbl);
   fd = fopen(pLayoutTbl, "r");
   if (fd)
   {
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && isalpha(*pTmp))
         {
            iTmp = ParseString(myTrim(acTmp), ',', MAX_LO_FLDS, pFlds);
            if (iTmp > 0)
            {
               strcpy(pLayout->fldName, pFlds[0]);
               pLayout->iOffset = atol(pFlds[1]);
               pLayout->iLen = atol(pFlds[2]);
               pLayout->iIdx = atol(pFlds[3]);
               pLayout->iDataFmt = GetXlatType(pFlds[4]);
               iLen += pLayout->iLen;
               iRet++;
               pLayout++;
            } else
            {
               LogMsg("***** Bad layout table file: %s", pLayoutTbl);
            }
         }
      }

      *pLayout->fldName = 0;
      pLayout->iOffset = 0;
      pLayout->iLen = 0;
      pLayout->iIdx = -1;
      pLayout->iDataFmt = -1;
      fclose(fd);
   } else
      LogMsg("***** Error opening layout table %s", pLayoutTbl);

   *iMaxLen = iLen;
   return iRet;
}

/********************************** LoadLayout *******************************
 *
 *****************************************************************************/

//int LoadLayout(INOUT_REC *pRecLayout)
//{
//   int   iRet, iTmp;
//   char  acTmp[_MAX_PATH];
//
//   // Get input layout
//   sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sInrecLayout);
//   iRet = LoadInputRecLayout(acTmp, &extrFlds[0], &iTmp);
//   if (iTmp > iRecLen)
//      LogMsg("*** Warning: Input record differs from defined.  Please check INI file (%d)", iTmp);
//
//   // Get output layout
//   sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sLayOutTbl);
//   iNumOsc = LoadPQExtrLayout(acTmp, &oscLayout[0], &iOutRollLen);
//   if (iNumOsc > 0)
//   {
//      if (bBulkCompatible)
//      {
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sOwnerTbl);
//         iNumO = LoadPQExtrLayout(acTmp, &oLayout[0], &iOutRollLen);
//
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sValueTbl);
//         iRet = LoadPQExtrLayout(acTmp, &valLayout[0], &iOutValLen);
//         if (iRet > 0)
//            iNumVal = iRet;
//         else
//         {
//            bCreateVal =  false;
//            LogMsg("***  Invalid layout %s, Skip Value extract\n", acTmp);
//         }
//
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sExemptTbl);
//         iRet = LoadPQExtrLayout(acTmp, &exeLayout[0], &iOutExeLen);
//         if (iRet > 0)
//            iNumExe = iRet;
//         else
//         {
//            bCreateExe =  false;
//            LogMsg("*** Invalid layout %s, Skip EXE extract\n", acTmp);
//         }
//
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sLegalTbl);
//         iRet = LoadPQExtrLayout(acTmp, &lglLayout[0], &iOutLglLen);
//         if (iRet > 0)
//            iNumLgl = iRet;
//         else
//         {
//            bCreateLgl =  false;
//            LogMsg("*** Invalid layout %s, Skip LEGAL extract\n", acTmp);
//         }
//
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sOCTbl);
//         iRet = LoadPQExtrLayout(acTmp, &ocLayout[0], &iOutOCLen);
//         if (iRet > 0)
//            iNumOC = iRet;
//         else
//         {
//            bCreateOC =  false;
//            LogMsg("*** Invalid layout %s, Skip OC extract\n", acTmp);
//         }
//
//         sprintf(acTmp, "%s\\%s", acLayoutPath, pRecLayout->sXCTbl);
//         iRet = LoadPQExtrLayout(acTmp, &xcLayout[0], &iOutXCLen);
//         if (iRet > 0)
//            iNumXC = iRet;
//         else
//         {
//            bCreateXC =  false;
//            LogMsg("*** Invalid layout %s, Skip XC extract\n", acTmp);
//         }
//      }
//   }
//   return iNumOsc;
//}

/********************************** initGlobal *******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int initGlobal(void)
{
   char  acTmp[_MAX_PATH];

   GetIniString("Data", "P8Tmpl", "", g_sP8Tmpl, _MAX_PATH, acIniFile);
   if (iLdrYear > 2000)
      sprintf(acTmp, "%d", iLdrYear);
   else
      acTmp[0] = 0;
   replStr(g_sP8Tmpl, "[year]", acTmp);

   // Get Lookup file name
   GetIniString("System", "LookUpTbl", "", acTmp, _MAX_PATH, acIniFile);
   if (_access(acTmp, 0))
   {
      LogMsg("***** Lookup file [%s] is missing.", acTmp);
      return -1;
   }

   // Loading lookup tables
   if (LoadLkupTables(acTmp))
      return 1;

   // Initialize direction table
   InitDirs();
   lRecCnt = 0;

   // Set debug flag
   GetPrivateProfileString("System", "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   return 0;
}

/******************************* writeHdr ************************************
 *
 * Write header using name column in extract format
 *
 *****************************************************************************/

void writeHdr(FILE *fd, XREFTBL2 *pLayout, int iCnt, bool bInclFips=true)
{
   char     acOutBuf[2048];
   int      iTmp;

   if (bInclFips)
      strcpy(acOutBuf, "FIPS");
   else
      acOutBuf[0] = 0;

   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (pLayout->iIdx >= 0)
      {
         if (acOutBuf[0])
            strcat(acOutBuf, sDelim);
         strcat(acOutBuf, pLayout->acName);
      } else if (iTmp > 0)
      {
         LogMsg("* Skip field: %s", pLayout->acName);
      }

      pLayout++;
   }

   strcat(acOutBuf, "\n");

   // Write output record
   fputs(acOutBuf, fd);
}

void writeHdrO(FILE *fd, XREFTBL2 *pLayout, int iCnt, bool bInclFips=true)
{
   char     acOutBuf[2048];
   int      iTmp;

   if (bInclFips)
      strcpy(acOutBuf, "FIPS");
   else
      acOutBuf[0] = 0;

   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (pLayout->iIdx >= 0)
      {
         if (acOutBuf[0])
            strcat(acOutBuf, sDelim);
         //if (((bCreateO1 || bCreateEO) && !memcmp(pLayout->acName, "ALT_APN", 7)) || (bCreateO2 && !memcmp(pLayout->acName, "PREV_APN", 8)) )
         if (bCreateEO && (!memcmp(pLayout->acName, "ALT_APN", 7) || !memcmp(pLayout->acName, "PREV_APN", 8)) )
            strcat(acOutBuf, "MODAPN");
         else if (bCreateEO && !_stricmp(pLayout->acName, "APN"))
            strcat(acOutBuf, "MODAPN");
         else
            strcat(acOutBuf, pLayout->acName);
      } else if (iTmp > 0)
      {
         LogMsg("* Skip field: %s", pLayout->acName);
      }

      pLayout++;
   }

   strcat(acOutBuf, "\n");

   // Write output record
   fputs(acOutBuf, fd);
}

/******************************* writeHdr1 ***********************************
 *
 * Write header using description column in extract format
 * This method is used for any purpose other then bulk client and web import.
 *
 *****************************************************************************/

void writeHdr1(FILE *fd, XREFTBL2 *pLayout, int iCnt, bool bInclFips=true)
{
   char     acOutBuf[2048];
   int      iTmp;

   if (bInclFips)
      strcpy(acOutBuf, "FIPS");
   else
      acOutBuf[0] = 0;

   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (pLayout->iIdx >= 0)
      {
         if (acOutBuf[0])
            strcat(acOutBuf, sDelim);
         strcat(acOutBuf, pLayout->acNameDesc);
      }
      pLayout++;
   }

   strcat(acOutBuf, "\n");

   // Write output record
   fputs(acOutBuf, fd);
}

/******************************* writeHdr2 ***********************************
 *
 * This method is used for web import only.  It includes calculated fields.
 *
 *****************************************************************************/

void writeHdr2(FILE *fd, XREFTBL2 *pLayout, int iCnt)
{
   char     acOutBuf[2048];
   int      iTmp;

   acOutBuf[0] = 0;

   for (iTmp = 0; iTmp < iCnt; iTmp++)
   {
      if (pLayout->iIdx >= 0)
      {
         if (acOutBuf[0])
            strcat(acOutBuf, sDelim);
         strcat(acOutBuf, pLayout->acNameDesc);
      }
      pLayout++;
   }

   if (iDocDate > 0)
   {
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "SDOCDATE");
   }

   if (bAddGeompt)
   {
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "GEOMPT");
   }
 
   if (bAddRecMonth)
   {
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "RECMONTH");
   }

   if (bAddDocLinks)
   {
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "DOC1LINK");
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "DOC2LINK");
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "DOC3LINK");
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "XFERLINK");
   }

   strcat(acOutBuf, sDelim);
   strcat(acOutBuf, "DBA");
   if (bAddPQZ)
   {
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "PQZONING");
      strcat(acOutBuf, sDelim);
      strcat(acOutBuf, "PQZONINGTYPE");
   }
   strcat(acOutBuf, sDelim);
   strcat(acOutBuf, "QUALITYCLASS\n");

   // Write output record
   fputs(acOutBuf, fd);
}

/******************************* formatLegal *********************************
 *
 * Format long legal
 *
 *****************************************************************************/

void formatLegal(char *pInrec, char *pLegal)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[512];

   *pLegal = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != LEGAL_XLAT; iTmp++);
   while (extrFlds[iTmp].iDataFmt == LEGAL_XLAT)
   {
      memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
      pTmp += extrFlds[iTmp].iLen;
      iTmp++;
   }

   *pTmp = 0;
   myTrim(sTmp);
   strcpy(pLegal, sTmp);
}

void formatLegal50(char *pInrec, char *pLegal)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[512];

   *pLegal = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != LEGAL50_XLAT; iTmp++);
   while (extrFlds[iTmp].iDataFmt == LEGAL50_XLAT)
   {
      memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
      pTmp += extrFlds[iTmp].iLen;
      iTmp++;
   }

   *pTmp = 0;
   myTrim(sTmp, 50);
   strcpy(pLegal, sTmp);
}

/******************************* formatCareOf ********************************
 *
 * Format extended careof
 *
 *****************************************************************************/

void formatCareOf(char *pInrec, char *pCareOf)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[256];

   *pCareOf = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != CARE_XLAT; iTmp++);
   while (extrFlds[iTmp].iDataFmt == CARE_XLAT)
   {
      memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
      pTmp += extrFlds[iTmp].iLen;
      iTmp++;
   }

   *pTmp = 0;
   myTrim(sTmp);
   strcpy(pCareOf, sTmp);
}

/***************************** formatComboFlds *******************************
 *
 * Format combo field by combining multiple fields with the same xlat type.
 * i.e. LEGAL, CAREOF, DOCLINKS.
 *
 *****************************************************************************/

void formatComboFlds(char *pInrec, char *pOutrec, int iFldXlat)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[512];

   *pOutrec = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != iFldXlat; iTmp++);
   while (extrFlds[iTmp].iDataFmt == iFldXlat)
   {
      memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
      pTmp += extrFlds[iTmp].iLen;
      iTmp++;
   }
   *pTmp = 0;
   myTrim(sTmp);
   strcpy(pOutrec, sTmp);
}

/****************************** formatSAddr() ********************************
 *
 * Format S_ADDR_D and place it in buffer
 *
 *****************************************************************************/

void formatSAddr(char *pInbuf, char *pSAddr)
{
   char  sTmp[256], sAddr[256];
   int   iTmp;

   // Get StrNum, StrFra, StrDir, StrName
   memcpy(sAddr, pInbuf+OFF_S_STRNUM, 36);
   sAddr[36] = 0;

   // Get StrSfx
   iTmp = atoin(pInbuf+OFF_S_SUFF, SIZ_S_SUFF);
   if (iTmp > 0)
   {
      strcat(sAddr, " ");
      strcat(sAddr, luStrType[iTmp-1].acValue);
   }

   // Get Unit#
   if (*(pInbuf+OFF_S_UNITNO) > ' ')
   {
      if (*(pInbuf+OFF_S_UNITNO) != '#')
         sprintf(sTmp, " #%.*s", SIZ_S_UNITNO, pInbuf+OFF_S_UNITNO);
      else
         sprintf(sTmp, " %.*s", SIZ_S_UNITNO, pInbuf+OFF_S_UNITNO);
      strcat(sAddr, sTmp);
   }
   blankRem(sAddr);
   strcpy(pSAddr, sAddr);
}

/****************************** formatMAddr() ********************************
 *
 * Format M_ADDR_D and place it in buffer
 *
 *****************************************************************************/

void formatMAddr(char *pInbuf, char *pMAddr)
{
   memcpy(pMAddr, pInbuf+OFF_M_STRNUM, SIZ_M_ADDR1);
   blankRem(pMAddr, SIZ_M_ADDR1);
}

/****************************** formatMCtySt() *******************************
 *
 * Format M_CTY_ST_D and place it in buffer
 *
 *****************************************************************************/

void formatMCtySt(char *pInbuf, char *pCtySt)
{
   char  acCity[32], *pTmp;

   memcpy(acCity, pInbuf+OFF_M_CITY, SIZ_M_CITY);
   myTrim(acCity, SIZ_M_CITY);
   *pCtySt = 0;
   if (acCity[0] > ' ')
   {
      if (pTmp = strchr(acCity, ','))
         sprintf(pCtySt, "%s %.5s", acCity, pInbuf+OFF_M_ZIP);
      else
         sprintf(pCtySt, "%s, %.2s %.5s", acCity, pInbuf+OFF_M_ST, pInbuf+OFF_M_ZIP);
   }
}

/****************************** formatZoning() *******************************
 *
 * Format Zoning and place it in buffer
 *
 *****************************************************************************/

void formatZoning(char *pInrec, char *pZoning)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[512];

   *pZoning = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != ZONE_XLAT; iTmp++);
   while (extrFlds[iTmp].iDataFmt == ZONE_XLAT)
   {
      if (memcmp(pInrec+extrFlds[iTmp].iOffset-1, "  ", 2))
      {
         memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
         pTmp += extrFlds[iTmp].iLen;
      } else
         break;
      iTmp++;
   }

   *pTmp = 0;
   iTmp = blankRem(sTmp);
   if (iTmp > 0)
      strcpy(pZoning, sTmp);
}

void formatZoning_Old(char *pInrec, char *pZoning)
{
   int   iTmp, iIdx=-1;
   char  *pTmp, sTmp[512];

   *pZoning = sTmp[0] = 0;
   pTmp = &sTmp[0];
   for (iTmp = 0; extrFlds[iTmp].iDataFmt != ZONE_XLAT; iTmp++);
   while (extrFlds[iTmp].iDataFmt == ZONE_XLAT)
   {
      if (*(pInrec+extrFlds[iTmp].iOffset-1) > ' ')
      {
         if (sTmp[0] > ' ')
            *pTmp++ = ',';
         memcpy(pTmp, pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);
         pTmp += extrFlds[iTmp].iLen;
      } else
         break;
      iTmp++;
   }

   *pTmp = 0;
   iTmp = remChar(sTmp, ' ');
   if (iTmp > 0)
      strcpy(pZoning, sTmp);
}

void formatFloor(char *pInrec, char *pFloor)
{
   int   iTmp, iFloor1, iFloor2;

   for (iTmp = 0; extrFlds[iTmp].iDataFmt != FLOOR_XLAT; iTmp++);
   iFloor2 = atoin(pInrec+extrFlds[iTmp+1].iOffset-1, extrFlds[iTmp].iLen);

   if (iFloor2 > 0 && iFloor2 < 10)
   {
      iFloor1 = atoin(pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen+1);
      *(pInrec+extrFlds[iTmp+1].iOffset-1) = ' ';    // Floor2 is a continuation of Floor1
   } else
      iFloor1 = atoin(pInrec+extrFlds[iTmp].iOffset-1, extrFlds[iTmp].iLen);

   sprintf(pFloor, "%d", iFloor1);
}

/******************************* createCsvRec ********************************
 *
 * In this function, treat LEGAL_XLAT the same way as CHAR_XLAT.
 *
 *****************************************************************************/

void createCsvRec(char *pOutbuf, char *pInbuf, XREFTBL2 *pLayout, int iCnt, bool bInclFips=true)
{
   int      iTmp, iTmp1, iIdx;
   LONGLONG lTmp;
   double   dVal;
   char     acTmp[256], acTmp1[256], acLegal[512], acZoning[64], acCareOf[256], *pTmp, *pTmp1;

   // Processing
   acLegal[0] = 0;
   acCareOf[0] = 0;

   if (bInclFips)
      strcpy(pOutbuf, myCounty.acFipsCode);
   else
      *pOutbuf = 0;

   // Check all fields
   for (iTmp = 0; iTmp < iCnt; iTmp++, pLayout++)
   {
      if (pLayout->iIdx >= 0)
      {
         iIdx = pLayout->iIdx;
         if (iTmp == 1)
         {
            memcpy(acTmp, pInbuf+extrFlds[iIdx].iOffset-1, extrFlds[iIdx].iLen);
            pTmp = myTrim(acTmp, extrFlds[iIdx].iLen);
         } else
         {
            memcpy(acTmp, pInbuf+extrFlds[iIdx].iOffset-1, extrFlds[iIdx].iLen);
            if (extrFlds[iIdx].iDataFmt == INT_XLAT || 
                extrFlds[iIdx].iDataFmt == DBL_XLAT ||
                extrFlds[iIdx].iDataFmt == NO_XLAT)
            {
               acTmp[extrFlds[iIdx].iLen] = 0;
               pTmp = acTmp;
            } else 
            {
               pTmp = myTrim(acTmp, extrFlds[iIdx].iLen);
            } 
         }

#ifdef _DEBUG
         //if (!memcmp(extrFlds[iIdx].fldName, "S_STRNUM", 5) || !memcmp(extrFlds[iIdx].fldName, "HSENO", 5))
         //   iTmp = 0;
         //if (!memcmp(pLayout->acName, "LOTSIZE", 4))
         //   iTmp1 = 0;
         //if (!memcmp(sApn, "689040017001", 12))
         //   iTmp1 = 0;
#endif

         // Append delimiter
         if (*pOutbuf)
            strcat(pOutbuf, sDelim);

         if (pLayout->iMaxLen > 0)
         {
            if (extrFlds[iIdx].iDataFmt >= CITY_XLAT)
            {
               switch (extrFlds[iIdx].iDataFmt)
               {
                  case YMD_XLAT:
                     // Verify date YYYYMMDD
                     lTmp = atol(acTmp);
                     if (lTmp > 19000101 && lTmp < lToday)
                     {
                        sprintf(acTmp, "%d", lTmp);
                        strcat(pOutbuf, acTmp);
                     }
                     break;
                  case CITY_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        iTmp1 = atoi(acTmp);
                        if (iTmp1 > 0)
                        {
                           pTmp1 = GetCityName(iTmp1);
                           if (pTmp1)
                              strcat(pOutbuf, pTmp1);
                           else
                              LogMsg("*** Unknown city code: %d [%.*s]", iTmp1, myCounty.iApnLen, pInbuf);
                        }
                     }
                     break;
                  case MADDR_XLAT:    // Reformat M_ADDR_D
                     if (acTmp[0] > ' ')
                     {
                        formatMAddr(pInbuf, acTmp);
                        if (acTmp[0] > ' ')
                           strcat(pOutbuf, acTmp);
                     }
                     break;
                  case MCTYST_XLAT:    // Reformat M_CTY_ST_D
                     if (acTmp[0] > ' ')
                     {
                        formatMCtySt(pInbuf, acTmp);
                        if (acTmp[0] > ' ')
                           strcat(pOutbuf, acTmp);
                     }
                     break;
                  case SADDR_XLAT:    // Reformat S_ADDR_D
                     if (acTmp[0] > ' ')
                     {
                        formatSAddr(pInbuf, acTmp);
                        if (acTmp[0] > ' ')
                           strcat(pOutbuf, acTmp);
                     }
                     break;
                  case SFX_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        iTmp1 = atoi(acTmp);
                        strcat(pOutbuf, luStrType[iTmp1-1].acValue);
                     }
                     break;
                  case DOCNUM_XLAT: // Clean up DOCNUM
                     if (acTmp[0] > ' ')
                     {
                        replCharEx(&acTmp[5], "+*`./{}&%$,;:", ' ', 7, false);
                        remChar(acTmp, ' ');
                        strcat(pOutbuf, acTmp);
                     }
                     break;
                  case DOCN_XLAT:   // Correct DOCNUM format
                     // to be done ...
                     //if (acTmp[0] > ' ' && acTmp[2] == ' ')
                     //{
                     //   iTmp1 = atoin(acTmp, 2);
                     //   if (iTmp1 < 5)
                     //      sprintf(acTmp1, "200%c%.6s", acTmp[1], &acTmp[3]
                     //   remChar(acTmp, ' ');
                     //   strcat(pOutbuf, acTmp);
                     //} else
                     {
                        strcat(pOutbuf, acTmp);
                     }
                     break;
                  case DOCTYPE_XLAT:
                     if (acTmp[0] > ' ')
                     {
#ifdef _DEBUG
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luDeedType[0], acTmp1));
#else
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luDeedType[0]));
#endif
                     }
                     break;

                  case USE_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luUseCode[0]));
                     break;

                  case USEC_XLAT:
                     if (acTmp[0] < '0')
                        strcat(pOutbuf, "999");
                     else
                        strcat(pOutbuf, acTmp);
                     break;

                  case LEGAL_XLAT:
                     //if (!memcmp(pInbuf, "8235007001", 10))
                     //   iTmp1 = 0;
                     if (acTmp[0] > ' ')
                     {
                        formatLegal(pInbuf, acLegal);
                        strcat(pOutbuf, acLegal);
                     }
                     break;
                     
                  case LEGAL50_XLAT:
                     //if (!memcmp(pInbuf, "8235007001", 10))
                     //   iTmp1 = 0;
                     if (acTmp[0] > ' ')
                     {
                        formatLegal50(pInbuf, acLegal);
                        strcat(pOutbuf, acLegal);
                     }
                     break;

                  case CARE_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        formatCareOf(pInbuf, acCareOf);
                        strcat(pOutbuf, acCareOf);
                     }
                     break;

                  case EXEMPT_XLAT:
                     if (acTmp[0] == '1')
                        strcat(pOutbuf, "Y");
                     else
                        strcat(pOutbuf, "N");
                     break;
                  case PSTAT_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luPrclStat[0]));
                     break;
                  case QUALITY_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luQuality[0]));
                     break;
                  case VIEW_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luView[0]));
                     break;
                  case WATER_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luWater[0]));
                     break;
                  case SEWER_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luSewer[0]));
                     break;
                  case POOL_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luPoolType[0]));
                     break;
                  case PARKTYPE_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luParkType[0]));
                     break;
                  case AIR_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luAirCond[0]));
                     break;
                  case HEAT_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luHeating[0]));
                     break;
                  case RFM_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luRoofMat[0]));
                     break;
                  case TOPO_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luTopo[0]));
                     break;
                  case RATIO_XLAT:
                     iTmp1 = atoi(acTmp);
                     sprintf(acTmp, "%d", iTmp1);
                     strcat(pOutbuf, acTmp);
                     break;
                  case CONDITION_XLAT:
                     if (acTmp[0] > ' ')
                        strcat(pOutbuf, Code2Value(acTmp, (LU_ENTRY *)&luCondition[0]));
                     break;

                  case ALL_REM:
                     blankRem(acTmp);
                     // Removed 01/17/2020 due to missing char in NAMES "H T H Learning"
                     //remDupWords(acTmp);

                  case QUOTE_REM:
                     // Replace '|' with '1'
                     replChar(pTmp, '|', '1', 0);

                     // Remove single/double quote
                     if (bIgnoreQuote)
                        quoteRem(pTmp);
                     strcat(pOutbuf, pTmp);
                     break;

                  case APN_D_XLAT:
                     iTmp1 = formatApn(acTmp, acTmp1, &myCounty);
                     strcat(pOutbuf, acTmp1);
                     break;

                  case DOCLNK_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        formatComboFlds(pInbuf, acTmp, DOCLNK_XLAT);
                        strcat(pOutbuf, acTmp);
                     }
                     break;

                  case M_UN_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        strcat(pOutbuf, acTmp);
                     } else if (memcmp(pInbuf+OFF_M_UNITNO, "      ", SIZ_M_UNITNO))
                     {
                        memcpy(acTmp, pInbuf+OFF_M_UNITNO, SIZ_M_UNITNO);
                        myTrim(acTmp, SIZ_M_UNITNO);
                        strcat(pOutbuf, acTmp);
                     }
                     break;

                  case S_UN_XLAT:
                     if (acTmp[0] > ' ')
                     {
                        strcat(pOutbuf, acTmp);
                     } else if (memcmp(pInbuf+OFF_S_UNITNO, "      ", SIZ_S_UNITNO))
                     {
                        memcpy(acTmp, pInbuf+OFF_S_UNITNO, SIZ_S_UNITNO);
                        myTrim(acTmp, SIZ_S_UNITNO);
                        strcat(pOutbuf, acTmp);
                     }
                     break;

                  case ZONE_XLAT:
                     //if (!memcmp(pInbuf, "001010040000", 11))
                     //   iTmp1 = 0;
                     if (acTmp[0] > ' ')
                     {
                        formatZoning(pInbuf, acZoning);
                        strcat(pOutbuf, acZoning);
                     }
                     break;

                  case FLOOR_XLAT:
                     //if (!memcmp(pInbuf, "001010040000", 11))
                     //   iTmp1 = 0;
                     if (acTmp[0] > ' ')
                     {
                        formatFloor(pInbuf, acTmp);
                        strcat(pOutbuf, acTmp);
                     }
                     break;

                  case D1K_XLAT:
                     iTmp1 = atol(acTmp);
                     if (iTmp1 > 0)
                     {
                        sprintf(acTmp, "%.3f", (double)iTmp1/1000);
                        strcat(pOutbuf, acTmp);
                     }
                     break;
                  default:
                     LogMsg("*** Unknown data format on field: %s", extrFlds[iIdx].fldName); 
               }
            } else if (extrFlds[iIdx].iDataFmt == INT_XLAT)
            {
#ifdef _DEBUG
               //if (!memcmp(sApn, "519 174701101", 13))
               //   iTmp1 = 0;
#endif
               if (acTmp[0] > '1' && strlen(acTmp) == 10)
                  strcat(pOutbuf, acTmp);
               else
               {
                  // To handle value greater 32-bit value, use strtoul() function
                  lTmp = atol(acTmp);
                  if (lTmp > 0)
                  {
                     sprintf(acTmp, "%u", lTmp);
                     strcat(pOutbuf, acTmp);
                  }
               }
            } else if (extrFlds[iIdx].iDataFmt == DBL_XLAT)
            {
               lTmp = atol(acTmp);
               if (lTmp > 0.0)
               {
                  dVal = (double)(lTmp/1000.0);
                  sprintf(acTmp, "%.3f", dVal);
                  strcat(pOutbuf, acTmp);
               }
            } else if (extrFlds[iIdx].iDataFmt == SNG_XLAT)
            {  // Special case - Stories
               iTmp1 = atoi(acTmp);
               if (iTmp1 > 0)
               {
                  if (!strchr(acTmp, '.'))
                  {
                     dVal = (double )(iTmp1/10.0);
                     sprintf(acTmp, "%.1f", dVal);
                  } else if (*pTmp == ' ')
                     pTmp++;
                  strcat(pOutbuf, pTmp);
               }
            } else if (extrFlds[iIdx].iDataFmt == GEO_XLAT)
            {
               Long2Geo(acTmp, acTmp1);
               if (acTmp1[0] != '0')
                  strcat(pOutbuf, acTmp1);
            } else if (extrFlds[iIdx].iDataFmt == NO_XLAT)
            {
               strcat(pOutbuf, acTmp);
            } else 
            {
               // Remove leading space
               while (*pTmp && *pTmp == ' ')
                  pTmp++;

               // Make sure output string within limit
               *(pTmp + min(pLayout->iMaxLen, extrFlds[iIdx].iLen)) = 0;

               // Remove single/double quote
               if (bIgnoreQuote)
                  quoteRem(pTmp);

               // Replace '|' with '1'
               iTmp1 = replChar(pTmp, '|', '1', 0);
#ifdef _DEBUG
               //if (iTmp1 > 0)
               //   iTmp1 = 0;
#endif
               strcat(pOutbuf, pTmp);
            }
         } // If pLayout->iMaxLen > 0
      }
   }

   // Add SDOCDATE
   acRecMonth[0] = 0;
   if (iDocDate > 0)
   {
      memcpy(acTmp, pInbuf+extrFlds[iDocDate].iOffset-1, extrFlds[iDocDate].iLen);
      acTmp[extrFlds[iDocDate].iLen] = 0;
      strcat(pOutbuf, sDelim);
      if (isdigit(acTmp[0]))
      {
         strcat(pOutbuf, acTmp);
         sprintf(acRecMonth, "%.2s", &acTmp[4]);
      }
   }
}

/******************************* formatDocLink *******************************
 *
 * Format DocLink.
 * If DocLink is empty, copy delimeter to output
 *
 *****************************************************************************/

void formatDocLink(char *pFmtDocLink, char *pSource, char *pDocLink, char *pID)
{
   if (pDocLink && *pDocLink > ' ')
      sprintf(pFmtDocLink, "%s%s;%s;%s", sDelim, pSource, pDocLink, pID);
   else
      strcpy(pFmtDocLink, sDelim);
}

/******************************* addDocLinks *********************************
 *
 * Add Doc?Link.
 * If DocLink is empty, add empty fields to output
 *
 *****************************************************************************/

void addDocLinks(char *pOutbuf, char *pInbuf, XREFTBL2 *pLayout, int iCnt)
{
   int      iTmp, iIdx;
   char     acTmp[256], acFmtDocLink[256];
   char     *apItems[5];

   // Parse current DocLinks    
   if (*(pInbuf+extrFlds[iDocLink].iOffset-1) >= ' ')
   {
      memcpy(acTmp, pInbuf+extrFlds[iDocLink].iOffset-1, extrFlds[iDocLink].iLen);
      if (*(pInbuf+extrFlds[iDocLinx].iOffset-1) > ' ')
      {
         memcpy(&acTmp[extrFlds[iDocLink].iLen], pInbuf+extrFlds[iDocLinx].iOffset-1, extrFlds[iDocLinx].iLen);
         acTmp[extrFlds[iDocLink].iLen+extrFlds[iDocLinx].iLen] = 0;
      } else
         acTmp[extrFlds[iDocLink].iLen] = 0;

      iTmp = ParseString(acTmp, ',', 4, apItems);
   } else
      iTmp = 0;

   iIdx = 0;
   // Add Doc1Link
   if (iTmp > 0)
   {
      iIdx = 1;
      formatDocLink(acFmtDocLink, "pq", apItems[0], "");
      strcat(pOutbuf, acFmtDocLink);
      if (iTmp > 1)
      {
         iIdx++;
         formatDocLink(acFmtDocLink, "pq", apItems[1], "");
         strcat(pOutbuf, acFmtDocLink);
         if (iTmp > 2)
         {
            iIdx++;
            formatDocLink(acFmtDocLink, "pq", apItems[2], "");
            strcat(pOutbuf, acFmtDocLink);
            if (iTmp > 3)
            {
               iIdx++;
               formatDocLink(acFmtDocLink, "pq", apItems[3], "");
               strcat(pOutbuf, acFmtDocLink);
            }
         }
      }
   }

   // Append blank field
   while (iIdx < 4)
   {
      strcat(pOutbuf, sDelim);
      iIdx++;
   }
}

/************************************ addDba *********************************
 *
 * Add DBA.
 * If DBA is empty, add empty fields to output
 *
 *****************************************************************************/

void addDba(char *pOutbuf, char *pInbuf)
{
   char     acTmp[SIZ_DBA+2];

   strcat(pOutbuf, sDelim);
   if (*(pInbuf+OFF_DBA) > ' ')
   {
      memcpy(acTmp, pInbuf+OFF_DBA, SIZ_DBA);
      myTrim(acTmp, SIZ_DBA);
      strcat(pOutbuf, acTmp);
   }
}

/************************************ addPQZ *********************************
 *
 * Add PQZoning.
 * If Zoning is empty, add empty fields to output
 *
 *****************************************************************************/

void addPQZ(char *pOutbuf, char *pInbuf)
{
   char     acTmp[265];
   int      iTmp = 0;

#ifdef _DEBUG
   //if (!memcmp(pInbuf, "036220013", 9))
   //   iTmp = 0;
#endif

   strcat(pOutbuf, sDelim);
   if (*(pInbuf+OFF_ZONE_X1) > ' ')
   {
      iTmp = SIZ_ZONE_X1;
      memcpy(acTmp, pInbuf+OFF_ZONE_X1, SIZ_ZONE_X1);
      if (memcmp(pInbuf+OFF_ZONE_X2, "  ", 2))
      {
         memcpy(&acTmp[iTmp], pInbuf+OFF_ZONE_X2, SIZ_ZONE_X2);
         iTmp += SIZ_ZONE_X2;
         if (memcmp(pInbuf+OFF_ZONE_X3, "  ", 2))
         {
            memcpy(&acTmp[iTmp], pInbuf+OFF_ZONE_X3, SIZ_ZONE_X3);
            iTmp += SIZ_ZONE_X3;
            if (memcmp(pInbuf+OFF_ZONE_X4, "  ", 2))
            {
               memcpy(&acTmp[iTmp], pInbuf+OFF_ZONE_X4, SIZ_ZONE_X4);
               iTmp += SIZ_ZONE_X4;
            }
         }
      }

      if (iTmp > 59)
         acTmp[59] = 0;
      else
         myTrim(acTmp, iTmp);
      strcat(pOutbuf, acTmp);
   }

   // Add PQZoningType
   strcat(pOutbuf, sDelim);
   memcpy(acTmp, pInbuf+OFF_ZONECAT, SIZ_ZONECAT);
   myTrim(acTmp, SIZ_ZONECAT);
   strcat(pOutbuf, acTmp);
}

/********************************* addQualClass ******************************
 *
 * Add QualityClass.
 * If QualityClass is empty, add empty fields to output
 *
 *****************************************************************************/

void addQualClass(char *pOutbuf, char *pInbuf)
{
   char     acTmp[265];
   int      iTmp = 0;

   strcat(pOutbuf, sDelim);
   if (*(pInbuf+OFF_QUALITYCLASS) > ' ')
   {
      memcpy(acTmp, pInbuf+OFF_QUALITYCLASS, SIZ_QUALITYCLASS);
      myTrim(acTmp, SIZ_QUALITYCLASS);
      strcat(pOutbuf, acTmp);
   }
}

/******************************** isFullExempt *******************************
 *
 * Return true if record is fully exempt
 *
 *****************************************************************************/

bool isFullExempt(char *pBuf)
{
   char  *pUseCode;
   int   iTmp;
   bool  bRet=false;

   switch (myCounty.iCntyID)
   {
      case 1:  // ALA
         pUseCode = pBuf+OFF_USE_CO;
         iTmp = atoin(pUseCode, 2);
         if (iTmp > 2 && iTmp < 6)
            bRet = true;
         break;
      case 19: // LAX
         iTmp = atoin(pBuf+7, 3);
         if ((iTmp >= 300 && iTmp < 400) || (iTmp >= 800))
            bRet = true;
         break;
      default:  // CCX
         if (*(pBuf+OFF_FULL_EXEMPT) == 'Y')
            bRet = true;
         break;
   }

   return bRet;
}

/******************************** doExpCsv **********************************
 *
 * Return < 0 if error occured.  Otherwise number of records extracted.
 *
 *****************************************************************************/

int doExpCsv(LPCSTR pCnty, bool bZipIt)
{
   char     acInBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE], acXCFile[_MAX_PATH], acZoneFile[_MAX_PATH],
            //acOC1File[_MAX_PATH], acOC2File[_MAX_PATH], acOSC1File[_MAX_PATH], acOSC2File[_MAX_PATH], acO1File[_MAX_PATH], acO2File[_MAX_PATH], 
            acOCFile[_MAX_PATH], acOSFile[_MAX_PATH], acOscFile[_MAX_PATH], acOFile[_MAX_PATH], acPrclFile[_MAX_PATH],
            acGeoFile[_MAX_PATH], acLglFile[_MAX_PATH], acExeFile[_MAX_PATH],
            acValFile[_MAX_PATH], acFexFile[_MAX_PATH], acAltFile[_MAX_PATH], acMailFile[_MAX_PATH];
   char     cFileCnt=1;

   DWORD    nBytesRead;
   BOOL     bRet, bEof;
   int      iWritten;
   long     lCnt=0;

   HANDLE   fhIn;
   FILE     *fdOSC, *fdVal, *fdExe, *fdO, *fdLgl, *fdGeo, *fdFex, *fdAlt, *fdPrcl, *fdOC, *fdOS, *fdXC, *fdMail, *fdZone;
            //*fdO1, *fdO2, *fdOC1, *fdOC2, *fdOSC1, *fdOSC2;

   //fdZone=fdMail=fdXC=fdOC=fdOS=fdO=fdO1=fdO2=fdOC1=fdOC2=fdOSC1=fdOSC2=fdLgl=fdGeo=fdVal=fdExe=fdFex=fdAlt=fdPrcl= (FILE *)NULL;
   fdZone=fdMail=fdXC=fdOC=fdOS=fdO=fdLgl=fdGeo=fdVal=fdExe=fdFex=fdAlt=fdPrcl= (FILE *)NULL;
   strcpy(acRollFile, acRawTmpl);
   replStrAll(acRollFile, "[cntycode]", myCounty.acCntyCode);

   LogMsg("Extracting CSV files for %s\n", pCnty);
   if (bBulkCompatible)
   {
      sprintf(acOFile,   g_sOTmpl,   myCounty.acCntyName);
      sprintf(acOCFile,  g_sOCTmpl,  myCounty.acCntyName);
      sprintf(acOSFile,  g_sOSTmpl,  myCounty.acCntyName);
      sprintf(acLglFile, g_sLglTmpl, myCounty.acCntyName);
      sprintf(acGeoFile, g_sGeoTmpl, myCounty.acCntyName);
      sprintf(acValFile, g_sValTmpl, myCounty.acCntyName);
      sprintf(acAltFile, g_sAltTmpl, myCounty.acCntyName);
      sprintf(acFexFile, g_sFexTmpl, myCounty.acCntyName);
      sprintf(acExeFile, g_sExeTmpl, myCounty.acCntyName);
      sprintf(acPrclFile,g_sPrclTmpl,myCounty.acCntyName);
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyName);
      sprintf(acXCFile,  g_sXCTmpl,  myCounty.acCntyName);
      sprintf(acMailFile,g_sMailTmpl,myCounty.acCntyName);
      sprintf(acZoneFile,g_sZoneTmpl,myCounty.acCntyName);
   } else if (bPQ3)
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyName);
   else
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyCode);

   // Open Output file
   LogMsg("Open OSC file %s", acOscFile);
   if (!(fdOSC = fopen(acOscFile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOscFile);
      return -1;
   }

   if (bBulkCompatible)
   {
      LogMsg("Open O file %s", acOFile);
      if (!(fdO = fopen(acOFile, "w")))
         return -1;

      if (bCreateVal)
      {
         LogMsg("Open value file %s", acValFile);
         if (!(fdVal = fopen(acValFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acValFile);
            return -2;
         }
      }

      if (bCreateFex)
      {
         LogMsg("Open full exemption file %s", acFexFile);
         if (!(fdFex = fopen(acFexFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acFexFile);
            return -3;
         }
      }

      if (bCreateExe)
      {
         LogMsg("Open exemption file %s", acExeFile);
         if (!(fdExe = fopen(acExeFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acExeFile);
            return -3;
         }
      }

      if (bCreateAlt)
      {
         LogMsg("Open AltApn file %s", acAltFile);
         if (!(fdAlt = fopen(acAltFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acAltFile);
            return -3;
         }
      }

      if (bCreatePrcl)
      {
         LogMsg("Open Parcel file %s", acPrclFile);
         if (!(fdPrcl = fopen(acPrclFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acPrclFile);
            return -3;
         }
      }

      if (bCreateLgl)
      {
         LogMsg("Open legal file %s", acLglFile);
         if (!(fdLgl = fopen(acLglFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acLglFile);
            return -4;
         }
      }

      if (bCreateGeo)
      {
         LogMsg("Open geopoint file %s", acGeoFile);
         if (!(fdGeo = fopen(acGeoFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acGeoFile);
            return -5;
         }
      }

      if (bCreateEO)
      {
         LogMsg("Open EOC file %s", acOCFile);
         if (!(fdOC = fopen(acOCFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acOCFile);
            return -5;
         }

         LogMsg("Open EOS file %s", acOSFile);
         if (!(fdOS = fopen(acOSFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acOSFile);
            return -5;
         }
         bCreateOC = true;
         bCreateOS = true;
      } else 
      {
         if (bCreateOS)
         {
            LogMsg("Open OS file %s", acOSFile);
            if (!(fdOS = fopen(acOSFile, "w")))
            {
               LogMsg("***** Error creating output file %s", acOSFile);
               return -5;
            }
         }
         if (bCreateOC)
         {
            LogMsg("Open OC file %s", acOCFile);
            if (!(fdOC = fopen(acOCFile, "w")))
            {
               LogMsg("***** Error creating output file %s", acOCFile);
               return -5;
            }
         }
      }

      if (bCreateXC)
      {
         LogMsg("Open extended CHAR file %s", acXCFile);
         if (!(fdXC = fopen(acXCFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acXCFile);
            return -5;
         }
      }

      if (bCreateMail)
      {
         LogMsg("Open Mailing file %s", acMailFile);
         if (!(fdMail = fopen(acMailFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acMailFile);
            return -5;
         }
      }

      if (bCreateZ)
      {
         LogMsg("Open Zoning file %s", acZoneFile);
         if (!(fdZone = fopen(acZoneFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acZoneFile);
            return -5;
         }
      }

      if (bInclHdr)
      {
         if (bCreateVal)
            writeHdr(fdVal, &valLayout[0], iNumVal, true);
         if (bCreateFex)
            writeHdr(fdFex, &fexLayout[0], iNumFex, true);
         if (bCreateExe)
            writeHdr(fdExe, &exeLayout[0], iNumExe, true);
         if (bCreateLgl)
            writeHdr(fdLgl, &lglLayout[0], iNumLgl, true);
         if (bCreateGeo)
            writeHdr(fdGeo, &geoLayout[0], iNumGeo, true);
         if (bCreateAlt)
            writeHdr(fdAlt, &altLayout[0], iNumAlt, true);
         if (bCreatePrcl)
            writeHdr(fdPrcl,&prclLayout[0],iNumPrcl,true);

         if (bCreateEO)
         {
            writeHdrO(fdOC  ,&ocLayout[0],  iNumOC,  true);
            writeHdrO(fdOS  ,&osLayout[0],  iNumOS,  true);
         } else 
         {
            if (bCreateOC)
               writeHdr(fdOC  ,&ocLayout[0],  iNumOC,  true);
            if (bCreateOS)
               writeHdr(fdOS  ,&osLayout[0],  iNumOS,  true);
         }
         if (bCreateXC)
            writeHdr(fdXC  ,&xcLayout[0],  iNumXC,  true);
         if (bCreateMail)
            writeHdr(fdMail,&mailLayout[0],iNumMail,  true);
         if (bCreateZ)
            writeHdr(fdZone,&zoneLayout[0],iNumZone,  true);
      }
   }

   if (bInclHdr)
   {
      // Output OSC format
      if (bPQ3)
         writeHdr(fdOSC, &oscLayout[0], iNumOsc, true);
      else if (bBulkCompatible)
      {
         if (bCreateEO)
         {
            writeHdrO(fdOSC, &oscLayout[0], iNumOsc, true);
            writeHdrO(fdO, &oLayout[0], iNumO, true);
         } else
         {
            writeHdr(fdOSC, &oscLayout[0], iNumOsc, true);
            writeHdr(fdO, &oLayout[0], iNumO, true);
         }
      } else if(bITrac)
         writeHdr2(fdOSC, &oscLayout[0], iNumOsc);
      else if (bWebImport)
         writeHdr2(fdOSC, &oscLayout[0], iNumOsc);
      else if (bFullText)
         writeHdr2(fdOSC, &oscLayout[0], iNumOsc);
      else
         writeHdr1(fdOSC, &oscLayout[0], iNumOsc, false);
   }

   LogMsg("Open input file %s", acRollFile);
   fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file %s", acRollFile);
      return -3;
   }

   // Skip header
   if (iRecLen == 1900)
      bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);

   nBytesRead = iRecLen;

   // Copy skip record 
   memset(acInBuf, ' ', iRecLen);

   bEof = false;
   iUnSec=iCntOsc=iCntO=iCntVal=iCntExe=iCntGeo=iCntLgl=iCntFex=iCntAlt=iCntOC=iCntXC=iCntMail=iCntZone=0;

  // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);
#ifdef _DEBUG
      //if (!memcmp(acInBuf, "001110014000", 9))
      //   iWritten = 0;
#endif
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRollFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRollFile[strlen(acRollFile)-1] = cFileCnt | 0x30;
         if (!_access(acRollFile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRollFile);
            fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);

         } else
            break;
      }

      // Check extract options
      if (acInBuf[OFF_UNS_FLG] == 'Y')
      {
         iUnSec++;

         if (bNoUnsPrcls)
            continue;
      }

#ifdef _DEBUG
      //if (!memcmp(acInBuf, "00812307", 8))
      //   bRet = true;
#endif
      // Initialize
      if (bBulkCompatible || bPQ3)
      {
         // Form OSC record
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, true);
         iCntOsc++;
      } else if (bWebImport || bITrac)
      {        
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, false);
         // append field geompt
         if (bAddGeompt)
            strcat(acOutBuf, sDelim);
         if (bAddRecMonth)
         {
            strcat(acOutBuf, sDelim);
            strcat(acOutBuf, acRecMonth);
         }
         // Add Doc1Link, ...
         if (bAddDocLinks)
            addDocLinks(acOutBuf, acInBuf, &oscLayout[0], iNumOsc);

         // Add DBA
         addDba(acOutBuf, acInBuf);

         // Add PQZoning
         if (bAddPQZ)
            addPQZ(acOutBuf, acInBuf);

         // Add QualityClass
         if (bWebImport)
            addQualClass(acOutBuf, acInBuf);
      } else if (bFullText)
      {        
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, false);

         if (bAddGeompt)
            strcat(acOutBuf, sDelim);

         // Add rec month
         if (bAddRecMonth)
         {
            strcat(acOutBuf, sDelim);
            strcat(acOutBuf, acRecMonth);
         }

         if (bAddDocLinks)
            addDocLinks(acOutBuf, acInBuf, &oscLayout[0], iNumOsc);

         // Add DBA
         addDba(acOutBuf, acInBuf);

         // Add PQZoning
         if (bAddPQZ)
            addPQZ(acOutBuf, acInBuf);

         // Add QualityClass
         addQualClass(acOutBuf, acInBuf);
      } else
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, false);

      // Write output record
      strcat(acOutBuf, "\n");
      iWritten = fputs(acOutBuf, fdOSC);
      if (iWritten == EOF)
      {
         LogMsg("***** Writing error occurs: %d\n", GetLastError());
         break;
      }

      // Form O record
      if (bBulkCompatible)
      {
         strcpy(acOutBuf, myCounty.acFipsCode);
         createCsvRec(acOutBuf, acInBuf, &oLayout[0], iNumO);

         // Write output record
         strcat(acOutBuf, "\n");
         iWritten = fputs(acOutBuf, fdO);

         // Form AltApn rec
         if (bCreateAlt)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &altLayout[0], iNumAlt);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdAlt);
            iCntAlt++;
         }

         // Form Values rec
         if (bCreateVal)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &valLayout[0], iNumVal);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdVal);
            iCntVal++;
         }

         // Form Full Exemption rec
         if (bCreateFex)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Fex record
            createCsvRec(acOutBuf, acInBuf, &fexLayout[0], iNumFex);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdFex);
            iCntFex++;
         }

         // Form Exemption rec
         if (bCreateExe)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Exe record
            createCsvRec(acOutBuf, acInBuf, &exeLayout[0], iNumExe);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdExe);
            iCntExe++;
         }

         // Form Legal rec
         if (bCreateLgl)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &lglLayout[0], iNumLgl);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdLgl);
            iCntLgl++;
         }

         // Form Values rec
         if (bCreateGeo)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &geoLayout[0], iNumGeo);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdGeo);
            iCntGeo++;
         }

         // Form APN_D rec
         if (bCreatePrcl)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &prclLayout[0], iNumPrcl);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdPrcl);
            iCntPrcl++;
         }

         // Form OC rec
         if (bCreateOC)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &ocLayout[0], iNumOC);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdOC);
            iCntOC++;
         }

         // Form OS rec
         if (bCreateOS)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &osLayout[0], iNumOS);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdOS);
            iCntOS++;
         }

         // Form O1 & OC1 rec
         //if (bCreateO1)
         //{
         //   // Initialize
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &o1Layout[0], iNumO1);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdO1);
         //   iCntO1++;

         //   // Initialize OC1 rec
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &oc1Layout[0], iNumOC1);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdOC1);

         //   // Initialize OSC1 rec
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &osc1Layout[0], iNumOSC1);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdOSC1);
         //}

         // Form O2 rec
         //if (bCreateO2)
         //{
         //   // Initialize
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &o2Layout[0], iNumO2);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdO2);
         //   iCntO2++;

         //   // Initialize OC2 rec
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &oc2Layout[0], iNumOC2);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdOC2);

         //   // Initialize OSC2 rec
         //   strcpy(acOutBuf, myCounty.acFipsCode);

         //   // Form Values record
         //   createCsvRec(acOutBuf, acInBuf, &osc2Layout[0], iNumOSC2);

         //   // Write output record
         //   strcat(acOutBuf, "\n");
         //   fputs(acOutBuf, fdOSC2);
         //}

         // Form XC rec
         if (bCreateXC)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &xcLayout[0], iNumXC);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdXC);
            iCntXC++;
         }

         // Form Mailing rec
         if (bCreateMail)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &mailLayout[0], iNumMail);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdMail);
            iCntMail++;
         }

         // Form Z rec
         if (bCreateZ)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &zoneLayout[0], iNumZone);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdZone);
            iCntZone++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdOSC)
      fclose(fdOSC);
   //if (fdOSC1)
   //   fclose(fdOSC1);
   //if (fdOSC2)
   //   fclose(fdOSC2);
   if (fdO)
      fclose(fdO);
   //if (fdO1)
   //   fclose(fdO1);
   //if (fdO2)
   //   fclose(fdO2);
   if (fdOC)
      fclose(fdOC);
   if (fdOS)
      fclose(fdOS);
   if (fdXC)
      fclose(fdXC);
   if (fdLgl)
      fclose(fdLgl);
   if (fdGeo)
      fclose(fdGeo);
   if (fdVal)
      fclose(fdVal);
   if (fdFex)
      fclose(fdFex);
   if (fdExe)
      fclose(fdExe);
   if (fdAlt)
      fclose(fdAlt);
   if (fdPrcl)
      fclose(fdPrcl);
   if (fdMail)
      fclose(fdMail);
   if (fdZone)
      fclose(fdZone);
   if (fhIn)
      CloseHandle(fhIn);

   // Remove blank file
   if (bCreateFex && !iCntFex)
      DeleteFile(acFexFile);
   if (bCreateExe && !iCntExe)
      DeleteFile(acExeFile);
   if (bCreateZ && !iCntZone)
      DeleteFile(acZoneFile);


   // Create summary file
   if (bBulkCompatible)
      createSummary();

   // Zip output files
   if (bZipIt && iWritten != EOF)
   {
      char  acZipFile[_MAX_PATH];
      int   iRet;

      sprintf(acZipFile, "%s\\Bulk_%s.zip", acZipPath, pCnty);

      // Make sure all files are there before creating zip list
      strcpy(acInBuf, acOscFile);
      if (!_access(acOFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOFile);
      }
      if (!_access(acValFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acValFile);
      }
      if (!_access(acExeFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acExeFile);
      }
      if (!_access(acFexFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acFexFile);
      }
      if (!_access(acLglFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acLglFile);
      }
      if (!_access(acGeoFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acGeoFile);
      }
      if (!_access(acOCFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOCFile);
      }
      if (!_access(acOSFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOSFile);
      }
      if (!_access(acXCFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acXCFile);
      }
      if (!_access(acZoneFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acZoneFile);
      }

      LogMsg("Create zip file %s", acZipFile);

      // Initialize zip
      doZipInit();
      setReplaceIfExist(true);

      // Remove existing zip file
      if (bRemZip && !_access(acZipFile, 0))
         DeleteFile(acZipFile);

      // Zip it up
      iRet = startZip(acZipFile, acInBuf);
      if (!iRet)
         LogMsg("Zip file is created successfully!!!");

      // Shutdown Zip engine
      doZipShutdown();
   }

   LogMsg("Total output records        : %u", lCnt);
   printf("\r%u\n", lCnt);

   return lCnt;
}

/******************************** doExpCsvList *******************************
 *
 * Export bulk file based on list of sorted APN.  This is used with -I option
 * when customer asks for certain parcels or a community.
 *
 * Return < 0 if error occured.  Otherwise number of records extracted.
 *
 *****************************************************************************/

int doExpCsvList(LPCSTR pCnty, bool bZipIt)
{
   char     acInBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE], acXCFile[_MAX_PATH], acZoneFile[_MAX_PATH],
            acOCFile[_MAX_PATH], acOSFile[_MAX_PATH], acOscFile[_MAX_PATH], acOFile[_MAX_PATH], acPrclFile[_MAX_PATH],
            acGeoFile[_MAX_PATH], acLglFile[_MAX_PATH], acExeFile[_MAX_PATH], 
            acValFile[_MAX_PATH], acFexFile[_MAX_PATH], acAltFile[_MAX_PATH], acMailFile[_MAX_PATH];
   char     acApn[256], *pTmp, cFileCnt=1;

   DWORD    nBytesRead;
   BOOL     bRet, bEof;
   int      iWritten, iRet, iBadApn=0;
   long     lCnt=0;

   HANDLE   fhIn;
   FILE     *fdOSC, *fdVal, *fdExe, *fdO, *fdLgl, *fdGeo, *fdFex, *fdAlt, *fdPrcl, *fdOC, *fdOS, *fdXC, *fdMail, *fdZone;

   fdZone=fdMail=fdXC=fdOC=fdOS=fdO=fdLgl=fdGeo=fdVal=fdExe=fdFex=fdAlt=fdPrcl= (FILE *)NULL;
   strcpy(acRollFile, acRawTmpl);
   replStrAll(acRollFile, "[cntycode]", myCounty.acCntyCode);

   LogMsg("Extracting data for %s based on APN list %s\n", pCnty, acApnFile);
   if (bBulkCompatible)
   {
      sprintf(acOFile,   g_sOTmpl,   myCounty.acCntyName);
      sprintf(acOCFile,  g_sOCTmpl,  myCounty.acCntyName);
      sprintf(acOSFile,  g_sOSTmpl,  myCounty.acCntyName);
      sprintf(acLglFile, g_sLglTmpl, myCounty.acCntyName);
      sprintf(acGeoFile, g_sGeoTmpl, myCounty.acCntyName);
      sprintf(acValFile, g_sValTmpl, myCounty.acCntyName);
      sprintf(acAltFile, g_sAltTmpl, myCounty.acCntyName);
      sprintf(acFexFile, g_sFexTmpl, myCounty.acCntyName);
      sprintf(acExeFile, g_sExeTmpl, myCounty.acCntyName);
      sprintf(acPrclFile,g_sPrclTmpl,myCounty.acCntyName);
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyName);
      sprintf(acXCFile,  g_sXCTmpl,  myCounty.acCntyName);
      sprintf(acMailFile,g_sMailTmpl,myCounty.acCntyName);
      sprintf(acZoneFile,g_sZoneTmpl,myCounty.acCntyName);
   } else if (bPQ3)
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyName);
   else
      sprintf(acOscFile, g_sOscTmpl, myCounty.acCntyCode);

   // Open Output file
   LogMsg("Open OSC file %s", acOscFile);
   if (!(fdOSC = fopen(acOscFile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOscFile);
      return -1;
   }

   if (bBulkCompatible)
   {
      LogMsg("Open O file %s", acOFile);
      if (!(fdO = fopen(acOFile, "w")))
         return -1;

      if (bCreateVal)
      {
         LogMsg("Open value file %s", acValFile);
         if (!(fdVal = fopen(acValFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acValFile);
            return -2;
         }
      }

      if (bCreateFex)
      {
         LogMsg("Open full exemption file %s", acFexFile);
         if (!(fdFex = fopen(acFexFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acFexFile);
            return -3;
         }
      }

      if (bCreateExe)
      {
         LogMsg("Open exemption file %s", acExeFile);
         if (!(fdExe = fopen(acExeFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acExeFile);
            return -3;
         }
      }

      if (bCreateAlt)
      {
         LogMsg("Open AltApn file %s", acAltFile);
         if (!(fdAlt = fopen(acAltFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acAltFile);
            return -3;
         }
      }

      if (bCreatePrcl)
      {
         LogMsg("Open Parcel file %s", acPrclFile);
         if (!(fdPrcl = fopen(acPrclFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acPrclFile);
            return -3;
         }
      }

      if (bCreateLgl)
      {
         LogMsg("Open legal file %s", acLglFile);
         if (!(fdLgl = fopen(acLglFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acLglFile);
            return -4;
         }
      }

      if (bCreateGeo)
      {
         LogMsg("Open geopoint file %s", acGeoFile);
         if (!(fdGeo = fopen(acGeoFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acGeoFile);
            return -5;
         }
      }

      if (bCreateEO)
      {
         LogMsg("Open EOC file %s", acOCFile);
         if (!(fdOC = fopen(acOCFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acOCFile);
            return -5;
         }

         LogMsg("Open EOS file %s", acOSFile);
         if (!(fdOS = fopen(acOSFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acOSFile);
            return -5;
         }
         bCreateOC = true;
         bCreateOS = true;
      } else 
      {
         if (bCreateOS)
         {
            LogMsg("Open OS file %s", acOSFile);
            if (!(fdOS = fopen(acOSFile, "w")))
            {
               LogMsg("***** Error creating output file %s", acOSFile);
               return -5;
            }
         }
         if (bCreateOC)
         {
            LogMsg("Open OC file %s", acOCFile);
            if (!(fdOC = fopen(acOCFile, "w")))
            {
               LogMsg("***** Error creating output file %s", acOCFile);
               return -5;
            }
         }
      }

      if (bCreateXC)
      {
         LogMsg("Open extended CHAR file %s", acXCFile);
         if (!(fdXC = fopen(acXCFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acXCFile);
            return -5;
         }
      }

      if (bCreateMail)
      {
         LogMsg("Open Mailing file %s", acMailFile);
         if (!(fdMail = fopen(acMailFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acMailFile);
            return -5;
         }
      }

      if (bCreateZ)
      {
         LogMsg("Open Zoning file %s", acZoneFile);
         if (!(fdZone = fopen(acZoneFile, "w")))
         {
            LogMsg("***** Error creating output file %s", acZoneFile);
            return -5;
         }
      }

      if (bInclHdr)
      {
         if (bCreateVal)
            writeHdr(fdVal, &valLayout[0], iNumVal, true);
         if (bCreateFex)
            writeHdr(fdFex, &fexLayout[0], iNumFex, true);
         if (bCreateExe)
            writeHdr(fdExe, &exeLayout[0], iNumExe, true);
         if (bCreateLgl)
            writeHdr(fdLgl, &lglLayout[0], iNumLgl, true);
         if (bCreateGeo)
            writeHdr(fdGeo, &geoLayout[0], iNumGeo, true);
         if (bCreateAlt)
            writeHdr(fdAlt, &altLayout[0], iNumAlt, true);
         if (bCreatePrcl)
            writeHdr(fdPrcl,&prclLayout[0],iNumPrcl,true);

         if (bCreateEO)
         {
            writeHdrO(fdOC  ,&ocLayout[0],  iNumOC,  true);
            writeHdrO(fdOS  ,&osLayout[0],  iNumOS,  true);
         } else 
         {
            if (bCreateOC)
               writeHdr(fdOC  ,&ocLayout[0],  iNumOC,  true);
            if (bCreateOS)
               writeHdr(fdOS  ,&osLayout[0],  iNumOS,  true);
         }
         if (bCreateXC)
            writeHdr(fdXC  ,&xcLayout[0],  iNumXC,  true);
         if (bCreateMail)
            writeHdr(fdMail,&mailLayout[0],iNumMail,  true);
         if (bCreateZ)
            writeHdr(fdZone,&zoneLayout[0],iNumZone,  true);
      }
   }

   if (bInclHdr)
   {
      // Output OSC format
      if (bPQ3)
         writeHdr(fdOSC, &oscLayout[0], iNumOsc, true);
      else if (bBulkCompatible)
      {
         if (bCreateEO)
         {
            writeHdrO(fdOSC, &oscLayout[0], iNumOsc, true);
            writeHdrO(fdO, &oLayout[0], iNumO, true);
         } else
         {
            writeHdr(fdOSC, &oscLayout[0], iNumOsc, true);
            writeHdr(fdO, &oLayout[0], iNumO, true);
         }
      } else if(bITrac)
         writeHdr2(fdOSC, &oscLayout[0], iNumOsc);
      else if (bWebImport||bFullText)
         writeHdr2(fdOSC, &oscLayout[0], iNumOsc);
      else
         writeHdr1(fdOSC, &oscLayout[0], iNumOsc, false);
   }

   LogMsg("Open input file %s", acRollFile);
   fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file %s", acRollFile);
      return -3;
   }

   fdApn = fopen(acApnFile, "r");
   if (!fdApn)
   {
      LogMsg("***** Error open APN input file %s", acApnFile);
      return -4;
   }

   // Skip header
   if (iRecLen == 1900)
      bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);

   // Get first rec
   bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);

   nBytesRead = iRecLen;

   // Copy skip record 
   memset(acInBuf, ' ', iRecLen);

   bEof = false;
   iUnSec=iCntOsc=iCntO=iCntVal=iCntExe=iCntGeo=iCntLgl=iCntFex=iCntAlt=iCntOC=iCntXC=iCntMail=iCntZone=0;

  // Merge loop
   while (!bEof && !feof(fdApn))
   {
      if (!(pTmp = fgets(acApn, 255, fdApn)))
         break;
      iApnLen = iTrim(acApn);

#ifdef _DEBUG
      //if (!memcmp(acApn, "07059220", iApnLen))
      //   iRet = 0;
#endif
      while ((iRet = memcmp(acInBuf, acApn, iApnLen)) < 0)
      {
         bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);
      }

      if (iRet > 0)
      {
         LogMsg("Bad APN: %s", acApn);
         iBadApn++;
         continue;
      }

      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRollFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRollFile[strlen(acRollFile)-1] = cFileCnt | 0x30;
         if (!_access(acRollFile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRollFile);
            fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acInBuf, iRecLen, &nBytesRead, NULL);

         } else
            break;
      }

      // Check extract options
      if (acInBuf[OFF_UNS_FLG] == 'Y')
      {
         iUnSec++;

         if (bNoUnsPrcls)
            continue;
      }

#ifdef _DEBUG
      //if (!memcmp(acInBuf, "00812307", 8))
      //   bRet = true;
#endif
      // Initialize
      if (bBulkCompatible || bPQ3)
      {
         // Form OSC record
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, true);
         iCntOsc++;
      } else if (bWebImport || bITrac)
      {        
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, false);
         // append field geompt
         if (bAddGeompt)
            strcat(acOutBuf, sDelim);
         if (bAddRecMonth)
         {
            strcat(acOutBuf, sDelim);
            strcat(acOutBuf, acRecMonth);
         }
         // Add Doc1Link, ...
         if (bAddDocLinks)
            addDocLinks(acOutBuf, acInBuf, &oscLayout[0], iNumOsc);

         // Add DBA
         addDba(acOutBuf, acInBuf);

         // Add PQZoning
         if (bWebImport)
         {
            addPQZ(acOutBuf, acInBuf);
            addQualClass(acOutBuf, acInBuf);
         }
      } else
         createCsvRec(acOutBuf, acInBuf, &oscLayout[0], iNumOsc, false);

      // Write output record
      strcat(acOutBuf, "\n");
      iWritten = fputs(acOutBuf, fdOSC);
      if (iWritten == EOF)
      {
         LogMsg("***** Writing error occurs: %d\n", GetLastError());
         break;
      }

      // Form O record
      if (bBulkCompatible)
      {
         strcpy(acOutBuf, myCounty.acFipsCode);
         createCsvRec(acOutBuf, acInBuf, &oLayout[0], iNumO);

         // Write output record
         strcat(acOutBuf, "\n");
         iWritten = fputs(acOutBuf, fdO);

         // Form AltApn rec
         if (bCreateAlt)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &altLayout[0], iNumAlt);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdAlt);
            iCntAlt++;
         }

         // Form Values rec
         if (bCreateVal)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &valLayout[0], iNumVal);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdVal);
            iCntVal++;
         }

         // Form Full Exemption rec
         if (bCreateFex)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Fex record
            createCsvRec(acOutBuf, acInBuf, &fexLayout[0], iNumFex);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdFex);
            iCntFex++;
         }

         // Form Exemption rec
         if (bCreateExe)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Exe record
            createCsvRec(acOutBuf, acInBuf, &exeLayout[0], iNumExe);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdExe);
            iCntExe++;
         }

         // Form Legal rec
         if (bCreateLgl)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &lglLayout[0], iNumLgl);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdLgl);
            iCntLgl++;
         }

         // Form Values rec
         if (bCreateGeo)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &geoLayout[0], iNumGeo);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdGeo);
            iCntGeo++;
         }

         // Form APN_D rec
         if (bCreatePrcl)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &prclLayout[0], iNumPrcl);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdPrcl);
            iCntPrcl++;
         }

         // Form OC rec
         if (bCreateOC)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &ocLayout[0], iNumOC);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdOC);
            iCntOC++;
         }

         // Form OS rec
         if (bCreateOS)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &osLayout[0], iNumOS);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdOS);
            iCntOS++;
         }

         // Form XC rec
         if (bCreateXC)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &xcLayout[0], iNumXC);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdXC);
            iCntXC++;
         }

         // Form Mailing rec
         if (bCreateMail)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &mailLayout[0], iNumMail);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdMail);
            iCntMail++;
         }

         // Form Z rec
         if (bCreateZ)
         {
            // Initialize
            strcpy(acOutBuf, myCounty.acFipsCode);

            // Form Values record
            createCsvRec(acOutBuf, acInBuf, &zoneLayout[0], iNumZone);

            // Write output record
            strcat(acOutBuf, "\n");
            fputs(acOutBuf, fdZone);
            iCntZone++;
         }
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }

   // Close files
   if (fdOSC)
      fclose(fdOSC);
   if (fdApn)
      fclose(fdApn);
   if (fdO)
      fclose(fdO);
   if (fdOC)
      fclose(fdOC);
   if (fdOS)
      fclose(fdOS);
   if (fdXC)
      fclose(fdXC);
   if (fdLgl)
      fclose(fdLgl);
   if (fdGeo)
      fclose(fdGeo);
   if (fdVal)
      fclose(fdVal);
   if (fdFex)
      fclose(fdFex);
   if (fdExe)
      fclose(fdExe);
   if (fdAlt)
      fclose(fdAlt);
   if (fdPrcl)
      fclose(fdPrcl);
   if (fdMail)
      fclose(fdMail);
   if (fdZone)
      fclose(fdZone);
   if (fhIn)
      CloseHandle(fhIn);

   // Remove blank file
   if (bCreateFex && !iCntFex)
      DeleteFile(acFexFile);
   if (bCreateExe && !iCntExe)
      DeleteFile(acExeFile);
   if (bCreateZ && !iCntZone)
      DeleteFile(acZoneFile);


   // Create summary file
   if (bBulkCompatible)
      createSummary();

   // Zip output files
   if (bZipIt && iWritten != EOF)
   {
      char  acZipFile[_MAX_PATH];
      int   iRet;

      sprintf(acZipFile, "%s\\Bulk_%s.zip", acZipPath, pCnty);

      // Make sure all files are there before creating zip list
      strcpy(acInBuf, acOscFile);
      if (!_access(acOFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOFile);
      }
      if (!_access(acValFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acValFile);
      }
      if (!_access(acExeFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acExeFile);
      }
      if (!_access(acFexFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acFexFile);
      }
      if (!_access(acLglFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acLglFile);
      }
      if (!_access(acGeoFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acGeoFile);
      }
      if (!_access(acOCFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOCFile);
      }
      if (!_access(acOSFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acOSFile);
      }
      if (!_access(acXCFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acXCFile);
      }
      if (!_access(acZoneFile, 0))
      {
         strcat(acInBuf, "|");
         strcat(acInBuf, acZoneFile);
      }

      LogMsg("Create zip file %s", acZipFile);

      // Initialize zip
      doZipInit();
      setReplaceIfExist(true);

      // Remove existing zip file
      if (bRemZip && !_access(acZipFile, 0))
         DeleteFile(acZipFile);

      // Zip it up
      iRet = startZip(acZipFile, acInBuf);
      if (!iRet)
         LogMsg("Zip file is created successfully!!!");

      // Shutdown Zip engine
      doZipShutdown();
   }

   LogMsg("Total output records  : %u", lCnt);
   if (iBadApn > 0)
      LogMsg("Number of bad APN     : %u", iBadApn);

   printf("\n");

   return lCnt;
}

/******************************* createFixRec ********************************
 *
 *
 *****************************************************************************/

void createFixRec(char *pOutbuf, char *pInbuf, XREFTBL2 *pLayout, int iCnt)
{
   int      iTmp, iTmp1, iIdx;
   double   dVal;
   unsigned long lTmp;
   char     acTmp[256], *pTmp;

   // Processing
   memcpy(pOutbuf, myCounty.acFipsCode, 5);
   for (iTmp = 0; iTmp < iCnt; iTmp++, pLayout++)
   {
      if (pLayout->iIdx >= 0)
      {
         iIdx = pLayout->iIdx;
         memcpy(acTmp, pInbuf+extrFlds[iIdx].iOffset-1, extrFlds[iIdx].iLen);
         acTmp[extrFlds[iIdx].iLen] = 0;

         if (extrFlds[iIdx].iDataFmt >= CITY_XLAT)
         {
            switch (extrFlds[iIdx].iDataFmt)
            {
               case CITY_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     iTmp1 = atoi(acTmp);
                     if (iTmp1 > 0)
                     {
                        pTmp = GetCityName(iTmp1);
                        iTmp1 = strlen(pTmp);
                        if (iTmp1 > pLayout->iMaxLen)
                           iTmp1 = pLayout->iMaxLen;
                        memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                     }
                  }
                  break;
               case SFX_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     iTmp1 = atoi(acTmp);
                     memcpy(pOutbuf+pLayout->iOffset, luStrType[iTmp1-1].acValue, strlen(luStrType[iTmp1-1].acValue));
                  }
                  break;
               case DOCTYPE_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luDeedType[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case USE_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luUseCode[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case EXEMPT_XLAT:
                  if (acTmp[0] == '1')
                     *(pOutbuf+pLayout->iOffset) = 'Y';
                  else
                     *(pOutbuf+pLayout->iOffset) = 'N';
                  break;
               case PSTAT_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luPrclStat[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case QUALITY_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luQuality[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case VIEW_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luView[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case WATER_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luWater[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case SEWER_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luSewer[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case POOL_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luPoolType[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case PARKTYPE_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luParkType[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case AIR_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luAirCond[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case HEAT_XLAT:
                  if (acTmp[0] > ' ')
                  {
                     pTmp = Code2Value(acTmp, (LU_ENTRY *)&luHeating[0]);
                     iTmp1 = strlen(pTmp);
                     if (iTmp1 > pLayout->iMaxLen)
                        iTmp1 = pLayout->iMaxLen;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
                  }
                  break;
               case RATIO_XLAT:
                  while (*pTmp && *pTmp == ' ')
                     pTmp++;
                     memcpy(pOutbuf+pLayout->iOffset, pTmp, strlen(pTmp));
                  break;
               case QUOTE_REM:
                  // Remove single quote
                  if (bIgnoreQuote)
                     remChar(acTmp, 39);
                  memcpy(pOutbuf+pLayout->iOffset, acTmp, strlen(acTmp));
                  break;

               default:
                  LogMsg("*** Unknown data format on field: %s", extrFlds[iIdx].fldName); 
            }
         } else if (extrFlds[iIdx].iDataFmt == INT_XLAT)
         {
            lTmp = atoi(acTmp);
            if (lTmp > 0)
            {
               iTmp1 = sprintf(acTmp, "%u", lTmp);
               memcpy(pOutbuf+pLayout->iOffset, acTmp, iTmp1);
            }
         } else if (extrFlds[iIdx].iDataFmt == DBL_XLAT)
         {
            iTmp1 = atoi(acTmp);
            if (iTmp1 > 0)
            {
               dVal = (double)(iTmp1/1000.0);
               iTmp1 = sprintf(acTmp, "%.3f", dVal);
               memcpy(pOutbuf+pLayout->iOffset, acTmp, iTmp1);
            }
         } else if (extrFlds[iIdx].iDataFmt == SNG_XLAT)
         {  // Special case - Stories
            iTmp1 = atoi(acTmp);
            if (iTmp1 > 0)
            {
               if (!strchr(acTmp, '.'))
               {
                  dVal = (double )(iTmp1/10.0);
                  iTmp1 = sprintf(acTmp, "%.1f", dVal);
                  memcpy(pOutbuf+pLayout->iOffset, acTmp, iTmp1);
               } else 
               {
                  pTmp = acTmp;
                  if (*pTmp == ' ')
                     pTmp++;
                  memcpy(pOutbuf+pLayout->iOffset, pTmp, strlen(pTmp));
               }
            }
         } else
         {
            // Remove leading space
            pTmp = acTmp;
            while (*pTmp && *pTmp == ' ')
               pTmp++;

            iTmp1 = strlen(pTmp);
            if (iTmp1 > pLayout->iMaxLen)
               iTmp1 = pLayout->iMaxLen;
            memcpy(pOutbuf+pLayout->iOffset, pTmp, iTmp1);
         }
      }
   }
}

/********************************* doExpFix **********************************
 *
 * ... to be implemented ...
 *
 *****************************************************************************/

int doExpFix(LPCSTR pCnty, bool bZipIt)
{
   char     acBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE];
   char     acOutFile[_MAX_PATH], acValFile[_MAX_PATH], acFexFile[_MAX_PATH];
   char     cFileCnt=1;

   DWORD    nBytesRead;
   BOOL     bRet, bEof;
   int      iWritten;
   long     lCnt=0;

   HANDLE   fhIn;
   FILE     *fdOut, *fdVal, *fdFex;

   //sprintf(acRollFile, acRawTmpl, pCnty, "G01");
   strcpy(acRollFile, acRawTmpl);
   replStrAll(acRollFile, "[cntycode]", myCounty.acCntyCode);

   sprintf(acOutFile, "%s\\%s, CA OSC.dat", acOutPath, myCounty.acCntyName);
   sprintf(acValFile, "%s\\%s, CA Values.dat", acOutPath, myCounty.acCntyName);
   sprintf(acFexFile, "%s\\%s, CA E.dat", acOutPath, myCounty.acCntyName);
   fdVal=fdFex= (FILE *)NULL;

   // Open Output file
   LogMsg("Open output file %s", acOutFile);
   if (!(fdOut = fopen(acOutFile, "wb")))
      return -1;
   if (bCreateVal && !(fdVal = fopen(acValFile, "wb")))
      return -2;
   if (bCreateFex && !(fdFex = fopen(acFexFile, "wb")))
      return -3;

   fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file %s", acRollFile);
      return -3;
   }

   nBytesRead = iRecLen;

   // Copy skip record 
   memset(acBuf, ' ', iRecLen);

   bEof = false;
  // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRollFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRollFile[strlen(acRollFile)-1] = cFileCnt | 0x30;
         if (!_access(acRollFile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRollFile);
            fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         } else
            break;
      }

      // Check extract options
      if (bNoUnsPrcls && acBuf[OFF_UNS_FLG] == 'Y')
         continue;

      // Initialize
      memset(acOutBuf, ' ', iOutRollLen+3);

      // Form OSC record
      createFixRec(acOutBuf, acBuf, &oscLayout[0], iNumOsc);

      // Append Std Usecode
      int iOutlen = iOutRollLen;

      // Add LF to EOL
      acOutBuf[iOutlen] = 10;

      // Write output record
      iWritten = fwrite(acOutBuf, 1, iOutlen+1, fdOut);
      if (iWritten < iOutlen)
      {
         LogMsg("***** Writing error occurs: %d\n", GetLastError());
         break;
      }

      if (bCreateVal)
      {
         // Initialize
         memset(acOutBuf, ' ', iOutValLen);

         // Form Values record
         createFixRec(acOutBuf, acBuf, &valLayout[0], iNumVal);

         // Write output record
         iWritten = fwrite(acOutBuf, 1, iOutValLen, fdVal);
      }

      if (bCreateFex)
      {
         // Initialize
         memset(acOutBuf, ' ', iOutFexLen);

         // Form Exe record
         createFixRec(acOutBuf, acBuf, &fexLayout[0], iNumFex);

         // Write output record
         iWritten = fwrite(acOutBuf, 1, iOutFexLen, fdFex);
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fdVal)
      fclose(fdVal);
   if (fdFex)
      fclose(fdFex);
   if (fhIn)
      CloseHandle(fhIn);

   LogMsg("Total output records        : %u", lCnt);
   printf("\n");

   return lCnt;
}

/******************************** doExpProp8 *********************************
 *
 * Return < 0 if error occured.  Otherwise number of records extracted.
 *
 *****************************************************************************/

int doExpProp8(LPCSTR pCnty)
{
   char     acBuf[MAX_RECSIZE], acOutBuf[MAX_RECSIZE], acOutFile[_MAX_PATH];
   char     cFileCnt=1;

   DWORD    nBytesRead;
   BOOL     bRet, bEof;
   int      iTmp;
   long     lCnt=0;

   HANDLE   fhIn;
   FILE     *fdOut;

   fdOut= (FILE *)NULL;
   strcpy(acRollFile, acRawTmpl);
   replStrAll(acRollFile, "[cntycode]", myCounty.acCntyCode);

   strcpy(acOutFile, g_sP8Tmpl);
   replStrAll(acOutFile, "[cntycode]", myCounty.acCntyCode);

   // Open Output file
   LogMsg("Open Prop8 file %s", acOutFile);
   if (!(fdOut = fopen(acOutFile, "w")))
      return -1;

   LogMsg("Open input file %s", acRollFile);
   fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
          FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhIn == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open input file %s", acRollFile);
      return -3;
   }

   nBytesRead = iRecLen;

   // Copy skip record 
   memset(acBuf, ' ', iRecLen);

   bEof = false;
   iCntO=0;

  // Merge loop
   while (!bEof)
   {
      bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);
#ifdef _DEBUG
      //if (!memcmp(acBuf, "49709209", 9))
      //   iWritten = 0;
#endif
      // Check for EOF
      if (!bRet)
      {
         LogMsg("***** Error reading input file %s (%f)", acRollFile, GetLastError());
         break;
      }

      if (!nBytesRead)
      {
         // EOF
         cFileCnt++;
         acRollFile[strlen(acRollFile)-1] = cFileCnt | 0x30;
         if (!_access(acRollFile, 0))
         {
            CloseHandle(fhIn);
            fhIn = 0;

            // Open next Input file
            LogMsg("Open input file %s", acRollFile);
            fhIn = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
                  FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);

            if (fhIn == INVALID_HANDLE_VALUE)
               break;
            bRet = ReadFile(fhIn, acBuf, iRecLen, &nBytesRead, NULL);

         } else
            break;
      }

      acOutBuf[0] = 0;
      if (acBuf[OFF_PROP8_FLG] == 'Y')
         memcpy(acOutBuf, acBuf, SIZ_APN_S);
      else
      {
         iTmp = atoin(&acBuf[OFF_TAX_CODE], SIZ_TAX_CODE);
         if (iTmp > 799 && iTmp < 900)
            memcpy(acOutBuf, acBuf, SIZ_APN_S);
      }

      if (acOutBuf[0] > 0)
      {
         myTrim(acOutBuf, SIZ_APN_S);
         strcat(acOutBuf, "\n");
         fputs(acOutBuf, fdOut);
         iCntO++;
      }

      if (!(++lCnt % 1000))
         printf("\r%u", lCnt);
   }
   // Close files
   if (fdOut)
      fclose(fdOut);
   if (fhIn)
      CloseHandle(fhIn);

   printf("\n");
   LogMsgD("Number of records processed : %u", lCnt);
   LogMsgD("Total output records        : %u", iCntO);

   return lCnt;
}

/********************************** initCounty *******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int initCounty(LPCSTR pCnty, LPCSTR pProdType)
{
   char  acTmp[_MAX_PATH], acCityFile[_MAX_PATH];
   int   iRet, iTmp;
   bool  bNoSale=false;

   LogMsg(" ----- Initialize product type: %s for %s", pProdType, pCnty);

   // For webimport, check for special delimiter on county (i.e. SFX)
   if (bWebImport||bFullText)
   {
      GetPrivateProfileString(pCnty, "Delimiter", "", sDelim, _MAX_PATH, acIniFile);
      if (sDelim[0] == 0)
         GetPrivateProfileString("Data", "Delimiter", "|", sDelim, _MAX_PATH, acIniFile);
   } else if (!sDelim[0])
      GetPrivateProfileString("Data", "Delimiter", "|", sDelim, _MAX_PATH, acIniFile);
   cDelim = sDelim[0];

   // Set debug flag
   GetPrivateProfileString(pCnty, "Debug", "N", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bDebug = true;
   else
      bDebug = false;

   // Get county info
   GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
   printf("Loading county table: %s\n", acTmp);
   iRet = loadCountyInfoCsv((char *)pCnty, acTmp);

   if (iRet == 1)
   {
      // Load city table
      GetIniString("Data", "CityFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acCityFile, acTmp, pCnty);
      printf("Loading city table: %s\n", acCityFile);
      iRet = LoadExCities(acCityFile);
      if (iRet < 1)
      {
         LogMsg("***** Error loading city table %s", acCityFile);
         return iRet;
      }
   }

   // Check Full Exe option
   if (bBulkCompatible && !bExtrAll)
   {
      GetPrivateProfileString(pCnty, "FullExe", "N", acTmp, _MAX_PATH, acIniFile);
      if (acTmp[0] == 'Y')
         bCreateFex = true;
   }

   // Set DocLinks
   iTmp = GetPrivateProfileString(pCnty, "AddDocLink", "", acTmp, _MAX_PATH, acIniFile);
   if (iTmp < 1)
      GetPrivateProfileString(pProdType, "AddDocLink", "", acTmp, _MAX_PATH, acIniFile);  
   if (acTmp[0] == 'Y')
      bAddDocLinks=true;
   else
      bAddDocLinks=false;

   g_iMaxImport = GetPrivateProfileInt(pCnty, "MaxImport", 0, acIniFile);
   if (!g_iMaxImport)
      g_iMaxImport = GetPrivateProfileInt("WebImport", "MaxImport", 5000, acIniFile);

   // Get raw file name
   iRet = GetIniString(acOutType, "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
   iRecLen = GetPrivateProfileInt(acOutType, "RecSize", 1934, acIniFile);
   if (!iRet)
   {
      if (g_sExtrOpt[2] == 'r')
      {
         GetIniString("Data", "R01File", "", acRawTmpl, _MAX_PATH, acIniFile);
         iRecLen = GetPrivateProfileInt("Data", "RSize", 1900, acIniFile);
      } else if (g_sExtrOpt[2] == 's')
      {
         GetIniString("Data", "S01File", "", acRawTmpl, _MAX_PATH, acIniFile);
         iRecLen = GetPrivateProfileInt("Data", "SSize", 1900, acIniFile);
      } else if (g_sExtrOpt[2] == 'g')
      {
         GetIniString("Data", "G01File", "", acRawTmpl, _MAX_PATH, acIniFile);
         iRecLen = GetPrivateProfileInt("Data", "GSize", 1934, acIniFile);
      } else if (g_sExtrOpt[2] == 'v')
      {
         GetIniString("Data", "V01File", "", acRawTmpl, _MAX_PATH, acIniFile);
         iRecLen = GetPrivateProfileInt("Data", "VSize", 1900, acIniFile);
      } else
      {
         GetIniString("Data", "RawFile", "", acRawTmpl, _MAX_PATH, acIniFile);
         iRecLen = GetPrivateProfileInt("Data", "RecSize", 1934, acIniFile);
      }
   }
   
   if (iLdrYear > 1995)
   {
      if (bPQ3)
         sprintf(acTmp, "_%d", iLdrYear);
      else
         sprintf(acTmp, "_LDR%d", iLdrYear);
   } else
      acTmp[0] = 0;
   replStrAll(acRawTmpl, "[year]", acTmp);

   // Get OutPath
   GetIniString(pProdType, "OutPath", "", acOutPath, _MAX_PATH, acIniFile);

   // Get ZipPath
   GetIniString(pProdType, "ZipPath", "", acZipPath, _MAX_PATH, acIniFile);

   // Layout folder
   GetIniString(pProdType, "LayoutPath", "", acLayoutPath, _MAX_PATH, acIniFile);

   // Get input layout
   iRet = 0;
   if (bBulkCompatible)
      iRet = GetIniString(pCnty, "InrecLayout", "", acTmp, _MAX_PATH, acIniFile);
   if (!iRet)
      GetIniString(pProdType, "InrecLayout", "", acTmp, _MAX_PATH, acIniFile);
   iRet = LoadInputRecLayout(acTmp, &extrFlds[0], &iTmp);
   //if (iTmp > iRecLen)
   //   LogMsg("*** Warning: Input record differs from defined.  Please check INI file (%d)", iTmp);

   // Check No sale flag for counties that use NDC sale data
   GetIniString(pCnty, "NoSale", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y' && !_memicmp(pProdType, "Bulk", 4))
   {
      bNoSale=true;   
      GetIniString(pProdType, "NSOSCTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else if (bCreateEO)
   {
      iRet = GetIniString(pCnty, "EOSCTbl", "", acTmp, _MAX_PATH, acIniFile);
      if (!iRet)
         GetIniString(pProdType, "EOSCTbl", "", acTmp, _MAX_PATH, acIniFile);
   } else
      GetIniString(pProdType, "LayoutTbl", "", acTmp, _MAX_PATH, acIniFile);

   // Get output layout
   iNumOsc = LoadPQExtrLayout(acTmp, &oscLayout[0], &iOutRollLen);
   if (iNumOsc > 0)
   {
      if (iLdrYear > 1995 && (bWebImport||bFullText))
      {
         if (bWebImport)
            sprintf(acTmp, "\\LDR%d", iLdrYear);
         else if (bFullText)
            sprintf(acTmp, "_%d", iLdrYear);
         else
            sprintf(acTmp, "\\%d", iLdrYear);
      } else
         acTmp[0] = 0;

      if (bCreateEO)
         GetIniString(pProdType, "EOSCTmpl", "", g_sOscTmpl, _MAX_PATH, acIniFile);
      else
         GetIniString(pProdType, "LayoutTmpl", "", g_sOscTmpl, _MAX_PATH, acIniFile);
      replStr(g_sOscTmpl, "[year]", acTmp);

      if (bBulkCompatible)
      {
         GetIniString(pProdType, "SummaryTmpl", "", g_sSumTmpl, _MAX_PATH, acIniFile);

         // Create O file
         if (bCreateEO)
         {
            iRet = GetIniString(pCnty, "EOTbl", "", acTmp, _MAX_PATH, acIniFile);
            if (!iRet)
               iRet = GetIniString(pProdType, "EOTbl", "", acTmp, _MAX_PATH, acIniFile);
            GetIniString(pProdType, "EOTmpl", "", g_sOTmpl, _MAX_PATH, acIniFile);
         } else
         {
            iRet = GetIniString(pCnty, "OwnerTbl", "", acTmp, _MAX_PATH, acIniFile);
            if (!iRet)
               iRet = GetIniString(pProdType, "OwnerTbl", "", acTmp, _MAX_PATH, acIniFile);
            GetIniString(pProdType, "OwnerTmpl", "", g_sOTmpl, _MAX_PATH, acIniFile);
         }
         if (iRet > 0)
            iNumO = LoadPQExtrLayout(acTmp, &oLayout[0], &iOutRollLen);

         // Create OC file
         if (bCreateEO)
         {
            iRet = GetIniString(pCnty, "EOCTbl", "", acTmp, _MAX_PATH, acIniFile);
            if (!iRet)
               GetIniString(pProdType, "EOCTbl", "", acTmp, _MAX_PATH, acIniFile);
            GetIniString(pProdType, "EOCTmpl", "", g_sOCTmpl, _MAX_PATH, acIniFile);
         } else
         {
            iRet = GetIniString(pCnty, "OCTbl", "", acTmp, _MAX_PATH, acIniFile);
            if (!iRet)
               GetIniString(pProdType, "OCTbl", "", acTmp, _MAX_PATH, acIniFile);
            GetIniString(pProdType, "OCTmpl", "", g_sOCTmpl, _MAX_PATH, acIniFile);
         }

         iRet = LoadPQExtrLayout(acTmp, &ocLayout[0], &iTmp);
         if (iRet > 0)
            iNumOC = iRet;
         else
         {
            bCreateOC =  false;
            LogMsg("*** Skip OC extract: %s\n", acTmp);
         }
      }

      if (bBulkCompatible && !bPQ3)
      {
         GetIniString(pProdType, "ValueTmpl", "", g_sValTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "ValueTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &valLayout[0], &iTmp);
         if (iRet > 0)
            iNumVal = iRet;
         else
         {
            bCreateVal =  false;
            LogMsg("*** Skip Value extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "ExemptTmpl", "", g_sExeTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "ExemptTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &exeLayout[0], &iTmp);
         if (iRet > 0)
            iNumExe = iRet;
         else
         {
            bCreateExe =  false;
            LogMsg("*** Skip EXE extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "FullExemptTmpl", "", g_sFexTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "FullExemptTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &fexLayout[0], &iTmp);
         if (iRet > 0)
            iNumFex = iRet;
         else
         {
            bCreateFex =  false;
            LogMsg("*** Skip Full Exempt extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "LegalTmpl", "", g_sLglTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "LegalTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &lglLayout[0], &iTmp);
         if (iRet > 0)
            iNumLgl = iRet;
         else
         {
            bCreateLgl =  false;
            LogMsg("*** Skip LEGAL extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "GeoTmpl", "", g_sGeoTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "GeoTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &geoLayout[0], &iTmp);
         if (iRet > 0)
            iNumGeo = iRet;
         else
         {
            bCreateGeo =  false;
            LogMsg("*** Skip GEO extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "AltApnTmpl", "", g_sAltTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "AltApnTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &altLayout[0], &iTmp);
         if (iRet > 0)
            iNumAlt = iRet;
         else
         {
            bCreateAlt =  false;
            LogMsg("*** Skip ALTAPN extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "PrclTmpl", "", g_sPrclTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "PrclTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &prclLayout[0], &iTmp);
         if (iRet > 0)
            iNumPrcl = iRet;
         else
         {
            bCreatePrcl =  false;
            LogMsg("*** Skip Parcel extract: %s\n", acTmp);
         }

         // Create OS file
         if (bCreateEO)
         {
            GetIniString(pProdType, "EOSTmpl", "", g_sOSTmpl, _MAX_PATH, acIniFile);
            iRet = GetIniString(pCnty, "EOSTbl", "", acTmp, _MAX_PATH, acIniFile);
            if (!iRet)
               GetIniString(pProdType, "EOSTbl", "", acTmp, _MAX_PATH, acIniFile);
         } else
         {
            GetIniString(pProdType, "OSTmpl", "", g_sOSTmpl, _MAX_PATH, acIniFile);
            if (bNoSale)
               GetIniString(pProdType, "NSOSTbl", "", acTmp, _MAX_PATH, acIniFile);
            else
               GetIniString(pProdType, "OSTbl", "", acTmp, _MAX_PATH, acIniFile);
         }

         iRet = LoadPQExtrLayout(acTmp, &osLayout[0], &iTmp);
         if (iRet > 0)
            iNumOS = iRet;
         else
         {
            bCreateOS =  false;
            LogMsg("*** Skip OS extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "XCTmpl", "", g_sXCTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "XCTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &xcLayout[0], &iTmp);
         if (iRet > 0)
            iNumXC = iRet;
         else
         {
            bCreateXC =  false;
            LogMsg("*** Skip XC extract: %s\n", acTmp);
         }

         GetIniString(pProdType, "MailTmpl", "", g_sMailTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "MailTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &mailLayout[0], &iTmp);
         if (iRet > 0)
            iNumMail = iRet;
         else
         {
            bCreateMail =  false;
            LogMsg("*** Skip Mail extract: %s\n", acTmp);
         }

         //if (bCreateO1)
         //{
         //   GetIniString(pProdType, "O1Tmpl", "", g_sO1Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "O1Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &o1Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumO1 = iRet;
         //   else
         //   {
         //      bCreateO1 =  false;
         //      LogMsg("*** Skip O1 extract: %s\n", acTmp);
         //   }

         //   GetIniString(pProdType, "OC1Tmpl", "", g_sOC1Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "OC1Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &oc1Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumOC1 = iRet;
         //   else
         //   {
         //      bCreateO1 =  false;
         //      LogMsg("*** Skip O1 & OC1 extract: %s\n", acTmp);
         //   }

         //   GetIniString(pProdType, "OSC1Tmpl", "", g_sOSC1Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "OSC1Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &osc1Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumOSC1 = iRet;
         //   else
         //   {
         //      bCreateO1 =  false;
         //      LogMsg("*** Skip O1, OC1, & OSC1 extract: %s\n", acTmp);
         //   }
         //}

         //if (bCreateO2)
         //{
         //   GetIniString(pProdType, "O2Tmpl", "", g_sO2Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "O2Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &o2Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumO2 = iRet;
         //   else
         //   {
         //      bCreateO2 =  false;
         //      LogMsg("*** Skip O2 extract: %s\n", acTmp);
         //   }

         //   GetIniString(pProdType, "OC2Tmpl", "", g_sOC2Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "OC2Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &oc2Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumOC2 = iRet;
         //   else
         //   {
         //      bCreateO2 =  false;
         //      LogMsg("*** Skip O2 & OC2 extract: %s\n", acTmp);
         //   }

         //   GetIniString(pProdType, "OSC2Tmpl", "", g_sOSC2Tmpl, _MAX_PATH, acIniFile);
         //   GetIniString(pProdType, "OSC2Tbl", "", acTmp, _MAX_PATH, acIniFile);
         //   iRet = LoadPQExtrLayout(acTmp, &osc2Layout[0], &iTmp);
         //   if (iRet > 0)
         //      iNumOSC2 = iRet;
         //   else
         //   {
         //      bCreateO2 =  false;
         //      LogMsg("*** Skip O2, OC2, & OSC2 extract: %s\n", acTmp);
         //   }
         //}

         GetIniString(pProdType, "ZoneTmpl", "", g_sZoneTmpl, _MAX_PATH, acIniFile);
         GetIniString(pProdType, "ZoneTbl", "", acTmp, _MAX_PATH, acIniFile);
         iRet = LoadPQExtrLayout(acTmp, &zoneLayout[0], &iTmp);
         if (iRet > 0)
            iNumZone = iRet;
         else
         {
            bCreateZ =  false;
            LogMsg("*** Skip Z extract: %s\n", acTmp);
         }
      }
   }

   if (bWebImport || bITrac || bFullText)
   {
      iTmp = GetPrivateProfileString(pCnty, "SDOCDATE", "", acTmp, _MAX_PATH, acIniFile);
      if (iTmp < 1)
         GetPrivateProfileString("Data", "SDOCDATE", "", acTmp, _MAX_PATH, acIniFile);  
      iDocDate = GetLayoutName(acTmp);
      iDocLink = GetLayoutName("DOCLINKS");
      iDocLinx = GetLayoutName("DOCLINKX");
      iDoc1Num = GetLayoutName("SALE1DN");
      iDoc2Num = GetLayoutName("SALE2DN");
      iDoc3Num = GetLayoutName("SALE3DN");
      iXferNum = GetLayoutName("TRANSDOC");
      iDoc1Date= GetLayoutName("SALE1RD");
      iDoc2Date= GetLayoutName("SALE2RD");
      iDoc3Date= GetLayoutName("SALE3RD");
      iXferDate= GetLayoutName("TRANSDATE");
   } else
      iDocDate = 0;

   // Get default option for geo point
   GetPrivateProfileString(pProdType, "Geompt", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bAddGeompt = true;
   else
      bAddGeompt = false;

   GetPrivateProfileString(pProdType, "RecMonth", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bAddRecMonth = true;
   else
      bAddRecMonth = false;

   // Get CHK file template
   iRet = GetIniString("Data", "ChkTmpl", "", g_sChkTmpl, _MAX_PATH, acIniFile);

   // Simulate -Xo option - remove 06/26/2021 spn
   //iRet = GetPrivateProfileString(pCnty, "Extr4D", "", acTmp, _MAX_PATH, acIniFile);
   //if (!_memicmp(acTmp, "o1", 2))
   //   bCreateO1 = true;
   //else if (!_memicmp(acTmp, "o2", 2))
   //   bCreateO2 = true;

   // Get default option for PQZ
   GetPrivateProfileString(pProdType, "AddPQZ", "", acTmp, _MAX_PATH, acIniFile);
   if (acTmp[0] == 'Y')
      bAddPQZ = true;
   else
      bAddPQZ = false;

   return iNumOsc;
}

/************************************** Usage ********************************
 *
 * Options:
 *    -C : County code
 *    -Au: Append standard USECODE
 *    -D : Delimiter (comma, vertical bar, ...)
 *    -Eg: extract GRGR for sql import\n");
 *    -Es: extract sale for sql import\n");
 *    -F : Output fix length records
 *    -H : Header(Y/N) 
 *    -Q : Remove single quote from specfied fields
 *    -Nu: Don't extract unsecured parcels
 *    -Ug: Use G01 file for input (default)
 *    -Ur: Use R01 file for input
 *    -Us: Use S01 file for input
 *    -Vi: verify import CHK file.  If CHK file is too big, extract whole file and set AutoImport='Y'
 *    -Xa: extract Alternate APN
 *    -Xp: extract APN_D only
 *    -Xd: extract long legal description
 *    -Xe: extract Exemption
 *    -Xf: extract Full Exemption parcels only
 *    -Xl: extract long/lat
 *    -Xv: extract Values
 *    -Xm: extract M module
 *    -Xoc:extract Owners & Chars data
 *    -Xos:extract Owners & Sales data
 *    -Xxc:extract extended Chars data
 *    -Xeo:Extract EO,EOC,EOSC for ESRI database
 *
 *    -Y<yyyy>: extract ldr year
 *    -Z[r] : Zip output to zip folder specified in INI file
 *    -T<Bulk|WebImport|ITrac> : specify type of output format (default -TBulk)
 *
 *****************************************************************************/
 
void Usage()
{
   printf("\nUsage: CddExtr -C<County code> [options]\n");
   printf("Options:\n");
   printf("\t-D : specify delimiter (default '|')\n");  
   printf("\t-E{s|g}: extract sale or GRGR import\n");
   printf("\t-F : output fixed length file\n");
   printf("\t-H : include header (default 'Y')\n");
   printf("\t-Nu: no known unsecured parcels\n");
   printf("\t-Q : ignore quote\n");
   printf("\t-T<Bulk|WebImport|ITrac> : specify type of output format (default -TBulk)\n");
   printf("\t-Vi: verify import CHK file.\n\n");
   printf("\t-Xall: extract all modules\n");
   printf("\t-Xa: extract Alternate APN\n");
   printf("\t-X8: extract APN with Prop8\n");
   printf("\t-Xd: extract long legal description\n");
   printf("\t-Xe: extract Exemption\n");
   printf("\t-Xf: extract Full Exemption parcels only\n");
   printf("\t-Xl: extract long/lat\n");
   printf("\t-Xm: extract M module\n");
   printf("\t-Xp: extract APN_D only\n");
   printf("\t-Xv: extract Values\n");
   printf("\t-Xz: extract Z module\n");
   printf("\t-Xeo:Extract EO,EOC,EOSC for ESRI database\n");
   printf("\t-Xmi:extract multiple improvement data\n\n");
   //printf("\t-Xo1:extract special O module which includes AltApn\n");
   //printf("\t-Xo2:extract special O module which includes PrevApn\n\n");
   printf("\t-Xoc:extract Owners & Chars data\n");
   printf("\t-Xos:extract Owners & Sales data\n");
   printf("\t-Xxc:extract extended Chars data\n\n");
   printf("\t-U{g|r|s}: Use G01 file for input\n");
   printf("\t-Y<yyyy>: extract ldr year\n");
   printf("\t-Z[r] : Zip output to zip folder specified in INI file\n");
   printf("\t-O : overwrite log file\n");
   exit(1);
}

/********************************** ParseCmd() *******************************
 *
 *
 *****************************************************************************/

void ParseCmd(int argc, char* argv[])
{
   char  chOpt;
   char	*pszParam;

   bFixLength = false;
   bInclHdr = true;
   bOverwriteLogfile = false;
   bIgnoreQuote = false;
   bNoUnsPrcls = false;
   bZip = false;
   bRemZip = false;
   bCreateExe = false;
   bCreateFex = false;
   bCreateVal = false;
   bCreateLgl = false;
   bCreateGeo = false;
   bCreateAlt = false;
   bCreatePrcl= false;
   bCreateOC  = false;
   bCreateOS  = false;
   //bCreateO1  = false;
   //bCreateO2  = false;
   bExtrAll   = false;
   bCreateXC  = false;
   bCreateMI  = false;
   bCreateMail= false;
   bCreateZ   = false;
   bCreateEO  = false;
   bExtrGrgr  = false;

   // Default delimiter
   sDelim[0] = 0;

   // Default output type
   strcpy(acOutType, "Bulk");
   bBulkCompatible = true;
   bWebImport = false;
   bITrac = false;
   bChkWebImport = false;
   bPQ3 = false;
   bFullText = false;
   iSqlImpType = 0;  // 1=sale, 2=grgr 

   // Use G01 as default
   g_sExtrOpt[2] = 'g';
   acApnFile[0] = 0;

   if (argv[1][0] != '-' && strlen(argv[1]) == 3)
   {
      strcpy(myCounty.acCntyCode, argv[1]);
      return;
   }

   while (1)
   {
      chOpt = GetOption(argc, argv, "A:C:D:E:FH:I:N:OQT:U:V:X:Y:Z:?", &pszParam);
      if (chOpt > 1)
      {
         // chOpt is valid argument
         switch (chOpt)
         {
            case 'A':   // to be implemented
               break;

            case 'C':   // county code
               if (pszParam != NULL)
                  strcpy(myCounty.acCntyCode, pszParam);
               else
               {
                  printf("Missing county code\n");
                  Usage();
               }
               break;
            case 'D':   // Delimiter ,
               if (pszParam != NULL)
               {
                  strcpy(sDelim, pszParam);
                  cDelim = sDelim[0];
               } else
               {
                  LogMsgD("***** Missing delimiter.  Use default '|'\n");
                  Usage();
               }
               break;

            case 'E':   // Extract for import
               if (pszParam != NULL)
               {
                  if (*pszParam == 's')
                     iSqlImpType = EXTR_SALE;
                  else if (*pszParam == 'g')
                     iSqlImpType = EXTR_GRGR;
               } else
               {
                  LogMsgD("***** Missing extract type for import.\n");
                  Usage();
               }
               break;

            case 'F':
               bFixLength = true;
               break;

            case 'H':   // Record header
               if (pszParam != NULL && toupper(*pszParam) == 'N')
                  bInclHdr = false;
               break;

            case 'I':   // county code
               if (pszParam != NULL)
                  strcpy(acApnFile, pszParam);
               else
               {
                  printf("Missing APN input file\n");
                  Usage();
               }
               break;

            case 'N':   // Skip option
               if (pszParam)
               {
                  if (*pszParam == 'u')
                     bNoUnsPrcls = true;
                  else
                  {
                     LogMsgD("***** Invalid sub-option -N%s.  Currently only -Nu is supported.\n", pszParam);
                     Usage();
                  }
               } else
               {
                  LogMsgD("***** Missing sub-option on -N\n");
                  Usage();
               }
               break;

            case 'X':   // Extra modules
               if (pszParam)
               {
                  if (!_stricmp(pszParam, "all"))
                  {
                     bCreateAlt = true;
                     bCreateFex = true;
                     bCreateVal = true;
                     bCreateLgl = true;
                     bCreateGeo = true;
                     bCreateAlt = true;
                     bExtrAll   = true;
                     bCreatePrcl= true;
                     bCreateOC  = true;
                     bCreateOS  = true;
                     bCreateXC  = true;
                     bCreateMI  = true;
                     bCreateMail= true;
                     bCreateZ   = true;
                     bCreateExe = true; 
                  } 
                  else if (!_stricmp(pszParam, "eo"))
                     bCreateEO = true;
                  else if (!_stricmp(pszParam, "mi"))
                     bCreateMI = true;
                  //else if (!_stricmp(pszParam, "o1"))
                  //{
                  //   bCreateO1 = true;
                  //   bCreateO2 = false;
                  //} else 
                  //if (!_stricmp(pszParam, "o2"))
                  //{
                  //   bCreateO2 = true;
                  //   bCreateO1 = false;
                  //} 
                  else if (!_stricmp(pszParam, "oc"))
                     bCreateOC = true;
                  else if (!_stricmp(pszParam, "os"))
                     bCreateOS = true;
                  else if (!_stricmp(pszParam, "xc"))
                     bCreateXC = true;
                  else if (!_stricmp(pszParam, "gr"))
                     bExtrGrgr = true;
                  else if (*pszParam == 'f')
                     bCreateFex = true;               // Extract fully exempt parcels only
                  else if (*pszParam == 'e')
                     bCreateExe = true;               // Extract exemptions
                  else if (*pszParam == 'v')
                     bCreateVal = true;
                  else if (*pszParam == 'd')
                     bCreateLgl = true;
                  else if (*pszParam == 'l')
                     bCreateGeo = true;
                  else if (*pszParam == 'a')
                     bCreateAlt = true;               // Extract AltApn or FeeParcel
                  else if (*pszParam == 'p')
                     bCreatePrcl = true;              // Extract APN_D only
                  else if (*pszParam == 'm')
                     bCreateMail = true;
                  else if (*pszParam == 'z')
                     bCreateZ = true;
                  else 
                     g_sExtrOpt[0] = *pszParam;
               } else
               {
                  LogMsgD("***** Missing sub-option on -X\n");
                  Usage();
               }
               break;

            case 'T':   // Output type
               if (pszParam != NULL)
               {
                  strcpy(acOutType, pszParam);
                  if (_stricmp(acOutType, "Bulk"))
                     bBulkCompatible = false;
                  if (!_stricmp(acOutType, "WebImport"))
                     bWebImport = true;
                  else if (!_stricmp(acOutType, "ITrac"))
                     bITrac = true;
                  else if (!_stricmp(acOutType, "PQ3"))
                     bPQ3 = true;
                  else if (!_stricmp(acOutType, "FullText"))
                     bFullText = true;
               } else
               {
                  LogMsgD("***** Missing output type.  Default to Bulk format\n");
                  Usage();
               }
               break;

            case 'U':   // Specify input
               if (pszParam != NULL)
                  g_sExtrOpt[2] = *pszParam;
               break;

            case 'V':   // Verify CHK file
               if (pszParam != NULL && *pszParam == 'i')
                  bChkWebImport = true;
               break;

            case 'O':   // Overwrite log file
               bOverwriteLogfile = true;
               break;

            case 'Q':
               bIgnoreQuote = true;
               break;

            case 'Y':   // LDR year
               if (pszParam != NULL)
               {
                  iLdrYear = atol(pszParam);
               } else
               {
                  LogMsgD("***** Missing LDR year.\n");
                  Usage();
               }
               break;

            case 'Z':   // Zip up output file
               bZip = true;
               if (pszParam && *pszParam == 'r')
                  bRemZip = true;
               break;

            case '?':   // usage info
            default:
               Usage();
               break;
         }
      }
      if (chOpt == 0)
      {
         // end of argument list
         break;
      }
      if ((chOpt == 1) || (chOpt == -1))
      {
         // standalone param or error
         LogMsg("Argument [%s] not recognized\n", pszParam);
         break;
      }
   }

   if (!strcmp(acOutType, "ITrac") && iLdrYear < 2000)
   {
      LogMsg("Invalid LDR year [%d].  Please retry", iLdrYear);
      exit(1);
   }
}

/********************************** Main() ***********************************
 *
 *
 *****************************************************************************/

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
   int   iRet;
   char  acTmp[256], acToday[16], acVersion[256], acOutFile[_MAX_PATH];

   // initialize MFC and print and error on failure
   if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
   {
      cerr << _T("Fatal Error: MFC initialization failed") << endl;
      return 1;
   }

   iRet = LoadString(theApp.m_hInstance, APP_VERSION_INFO, acVersion, 64);
   printf("%s\n\n", acVersion);

   if (argc < 2)
      Usage();

   // Get today date - yyyymmdd
   getDate(acToday, 0);
   lToday = atoi(acToday);

   strcat(acVersion, " Options: ");
   for (iRet = 1; iRet < argc; iRet++)
   {
      strcat(acVersion, argv[iRet]);
      strcat(acVersion, " ");
   }

   // Parse command line
   ParseCmd(argc, argv);

   // Get INI file name
   sprintf(acIniFile, "%s\\%s.ini", _getcwd((char *)&acTmp[0], _MAX_PATH), theApp.m_pszExeName);
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\CddExtr\\CdrExtr.ini");

   // Initialize
   LogMsgD("loading %s\n", acIniFile);
   iRet = initGlobal();
   if (iRet)
   {
      LogMsg("***** Error in initGlobal(). Please check INI file.");
      close_log();
      return 1;
   }

   // If not found INI file in working folder, check default location
   if (_access(acIniFile, 0))
      strcpy(acIniFile, "C:\\Tools\\cddextr\\CddExtr.ini");

   GetIniString("System", "LogPath", "", acTmp, _MAX_PATH, acIniFile);
   sprintf(acLogFile, "%s\\CddExtr_%s.log", acTmp, acToday);
   printf("Open log file: %s\n", acLogFile);
   if (bOverwriteLogfile)
      open_log(acLogFile, "w");
   else
      open_log(acLogFile, "a+");

   LogMsg(acVersion);

   // Determine type of extract
   if (iSqlImpType > 0)
   {
      // Get county info
      GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
      printf("Loading county table: %s\n", acTmp);
      iRet = loadCountyInfoCsv(myCounty.acCntyCode, acTmp);
      if (iRet == 1)
      {
         iApnLen = myCounty.iApnLen;
         if (iLdrYear < 2000)
            iLdrYear = atoin(acToday, 4);

         if (bBulkCompatible)
         {
            GetIniString("Bulk", "STmpl", "", g_sSTmpl, _MAX_PATH, acIniFile);
	         sprintf(acOutFile, g_sSTmpl, myCounty.acCntyName);

            // Extract for bulk customer
            iRet = createBulkSale(myCounty.acCntyCode, acOutFile, iLdrYear, iSqlImpType);
         } else
         {
            // Extract for SQL import
            iRet = createSaleCsv(myCounty.acCntyCode, iLdrYear, iSqlImpType);
         }
      }

      close_log();
      return 0;
   }

   // Setup list of counties to process
   iCounties = 0;
   if (myCounty.acCntyCode[0] > ' ')
   {
      lstCnty[iCounties++] = myCounty.acCntyCode;
      bAuto = false;
   } else
   {
      bAuto = true;
      iCounties = getCntyList();
   }

   if (bExtrGrgr)
   {
      char sDbName[64], sGrgrFile[_MAX_PATH], sGrGrTmpl[_MAX_PATH];
      int   iTmp;

      GetIniString("System", "CountyTbl", ".\\CountyInfo.csv", acTmp, _MAX_PATH, acIniFile);
      printf("Loading county table: %s\n", acTmp);
      iRet = loadCountyInfoCsv(lstCnty[0].GetBuffer(), acTmp);
      sprintf(sDbName, "UPD%s", myCounty.acYearAssd);

      GetIniString("Data", "SqlGrgrFile", "", acTmp, _MAX_PATH, acIniFile);
      sprintf(acOutFile, acTmp, sDbName, myCounty.acCntyCode);
      GetIniString("Data", "GrGrOut", "", sGrGrTmpl, _MAX_PATH, acIniFile);
      sprintf(sGrgrFile, sGrGrTmpl, myCounty.acCntyCode, myCounty.acCntyCode);
      iTmp = GetPrivateProfileInt(myCounty.acCntyCode, "GrGrType", 1, acIniFile);
      iTmp = createSaleImport(NULL, myCounty.acCntyCode, sGrgrFile, acOutFile, iTmp, bInclHdr);
   } else
   if (iCounties > 0)
   {
      int iIdx = 0;
      __int64 nRet = 0;

      while (iIdx < iCounties)
      {
         // Init product specific variables
         iRet = initCounty(lstCnty[iIdx], acOutType);
         if (iRet < 1)
         {
            LogMsg("***** Error initializing %s in CddExtr\n", lstCnty[iIdx]);
            break;
         }

         // May need to reload layout if other task are running before doExpCsv()
         //if (bAuto && bBulkCompatible)
         //{
         //   // Reload layout
         //   iRet = LoadLayout(&lstLayout[iIdx]);
         //}

         if (bFixLength)
            iRet = doExpFix(lstCnty[iIdx], bZip);
         else
         {
            if (acApnFile[0] > ' ')
               iRet = doExpCsvList(lstCnty[iIdx], false);
            else
               iRet = doExpCsv(lstCnty[iIdx], bZip);
         }

         if (iRet > 0 && bBulkCompatible)
         {
            // Reset county status
            sprintf(acTmp, "EXECUTE spUpdAutoExtract '%s', 'E'", lstCnty[iIdx]);
            iRet = execSqlCmd(acTmp);
            if (!iRet)
               LogMsg("Successfully update extract flag (%s)", acTmp);

            if (bChkWebImport)
            {
               char sChkFile[_MAX_PATH], sSavType[16];

               // Form CHK file name
               sprintf(sChkFile, g_sChkTmpl, lstCnty[iIdx], lstCnty[iIdx]);

               // Get record count
               nRet = getFileSize(sChkFile);
               iRet = (int)(nRet / 24);
               if (iRet > g_iMaxImport)
               {
                  bWebImport = true;
                  bBulkCompatible = false;
                  strcpy(sSavType, acOutType);
                  strcpy(acOutType, "WebImport");

                  // Extract for web import
                  iRet = initCounty(lstCnty[iIdx], acOutType);
                  iRet = doExpCsv(lstCnty[iIdx], false);

                  // to be done ...
                  //if (iRet > 0)
                  //{
                  //   // Change import flag to 'Y'
                  //   sprintf(acTmp, "EXECUTE spUpdAutoImport '%s', 'Y'", lstCnty[iIdx]);
                  //   iRet = execSqlCmd(acTmp);
                  //}

                  bBulkCompatible=true;
                  bWebImport = false;
                  strcpy(acOutType, sSavType);
               }
            }
         }

         // Extract prop8 data
         if (g_sExtrOpt[0] == '8')
         {
            iRet = doExpProp8(lstCnty[iIdx]);
         }

         // Extract MI file from standard ???_Char.dat
         if (bCreateMI)
         {
            iCntMI = createCharCsv();
         }

         iIdx++;
      }
   } else
   {
      LogMsg("*** No county available for processing");
   }

   // Close log
   close_log();

   return 0;
}
