USE [Spatial]
GO
/****** Object:  StoredProcedure [dbo].[spCreateIndex]    Script Date: 05/28/2013 15:45:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[spCreateIndex]
   @CntyTbl [varchar](3)
WITH EXECUTE AS CALLER
AS
Declare @sql as nvarchar(400)

BEGIN
   set @sql=N'CREATE NONCLUSTERED INDEX [iHO_FL] ON [dbo].[' + @CntyTbl + '_Roll] ([HO_FL] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iS_STRNUM] ON [dbo].[' + @CntyTbl + '_Roll] ([S_STRNUM] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iS_DIR] ON [dbo].[' + @CntyTbl + '_Roll] ([S_DIR] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iS_SUFFIX] ON [dbo].[' + @CntyTbl + '_Roll] ([S_SUFFIX] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iS_CITY] ON [dbo].[' + @CntyTbl + '_Roll] ([S_CITY] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iS_ZIP] ON [dbo].[' + @CntyTbl + '_Roll] ([S_ZIP] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iGROSS_VAL] ON [dbo].[' + @CntyTbl + '_Roll] ([GROSS_VAL] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iUSE_STD] ON [dbo].[' + @CntyTbl + '_Roll] ([USE_STD] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iUSE_CO] ON [dbo].[' + @CntyTbl + '_Roll] ([USE_CO] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iZONING] ON [dbo].[' + @CntyTbl + '_Roll] ([ZONING] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iRATIO] ON [dbo].[' + @CntyTbl + '_Roll] ([RATIO] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iTRA] ON [dbo].[' + @CntyTbl + '_Roll] ([TRA] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iN_TRACT] ON [dbo].[' + @CntyTbl + '_Roll] ([N_TRACT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE1_DATE] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE1_DATE] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE2_DATE] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE2_DATE] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE3_DATE] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE3_DATE] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE1_AMT] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE1_AMT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE1_DOC] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE1_DOC] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE2_DOC] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE2_DOC] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSALE3_DOC] ON [dbo].[' + @CntyTbl + '_Roll] ([SALE3_DOC] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iXFER_DOC] ON [dbo].[' + @CntyTbl + '_Roll] ([XFER_DOC] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iBEDS] ON [dbo].[' + @CntyTbl + '_Roll] ([BEDS] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iYR_BLT] ON [dbo].[' + @CntyTbl + '_Roll] ([YR_BLT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iYR_EFF] ON [dbo].[' + @CntyTbl + '_Roll] ([YR_EFF] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSTORIES] ON [dbo].[' + @CntyTbl + '_Roll] ([STORIES] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iFIRE_PL] ON [dbo].[' + @CntyTbl + '_Roll] ([FIRE_PL] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iBLDG_QUALITY] ON [dbo].[' + @CntyTbl + '_Roll] ([BLDG_QUALITY] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iBATH_F] ON [dbo].[' + @CntyTbl + '_Roll] ([BATH_F] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iLOT_ACRES] ON [dbo].[' + @CntyTbl + '_Roll] ([LOT_ACRES] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iLOT_SQFT] ON [dbo].[' + @CntyTbl + '_Roll] ([LOT_SQFT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iPOOL] ON [dbo].[' + @CntyTbl + '_Roll] ([POOL] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iHEAT] ON [dbo].[' + @CntyTbl + '_Roll] ([HEAT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iBLDG_CLS] ON [dbo].[' + @CntyTbl + '_Roll] ([BLDG_CLS] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iBLDG_SQFT] ON [dbo].[' + @CntyTbl + '_Roll] ([BLDG_SQFT] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iAIR_COND] ON [dbo].[' + @CntyTbl + '_Roll] ([AIR_COND] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iM_STRNUM] ON [dbo].[' + @CntyTbl + '_Roll] ([M_STRNUM] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iM_CITY] ON [dbo].[' + @CntyTbl + '_Roll] ([M_CITY] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iM_ZIP] ON [dbo].[' + @CntyTbl + '_Roll] ([M_ZIP] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iM_ST] ON [dbo].[' + @CntyTbl + '_Roll] ([M_ST] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iUNITS] ON [dbo].[' + @CntyTbl + '_Roll] ([UNITS] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [iSDOCDATE] ON [dbo].[' + @CntyTbl + '_Roll] ([SDOCDATE] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   set @sql=N'CREATE NONCLUSTERED INDEX [RECMONTH] ON [dbo].[' + @CntyTbl + '_Roll] ([RECMONTH] ASC)WITH (STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
   SET @sql=N'CREATE SPATIAL INDEX [' + @CntyTbl + '_spatialPT] ON [dbo].[' + @CntyTbl + '_Roll]
   (
      [GEOMpt]
   )USING  GEOGRAPHY_GRID WITH (GRIDS =(LEVEL_1 = HIGH,LEVEL_2 = HIGH,LEVEL_3 = HIGH,LEVEL_4 = HIGH),
   CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]'
   EXEC sp_executesql @sql
END
