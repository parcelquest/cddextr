USE [Spatial]
GO
/****** Object:  StoredProcedure [dbo].[spUpdateGeompt]    Script Date: 05/10/2010 17:29:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spUpdateGeompt]
   @Cnty [varchar](3)
WITH EXECUTE AS CALLER
AS
Declare @sql as nvarchar(400)
BEGIN
   SET @sql=N'UPDATE ' + @Cnty + '_Roll SET
      Longitude = CAST(b.Longitude AS float),
      Latitude = CAST(b.Latitude AS float),
      GEOMpt = geography::STPointFromText(''POINT ('' + b.Longitude + '' '' + b.Latitude + '')'',4326)
      FROM ' + @Cnty + '_Roll r INNER JOIN ' + @Cnty + '_Basemap b on r.APN = b.APN'
   EXEC sp_executesql @sql
END