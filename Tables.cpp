/******************************************************************************
 *
 * 01/13/2016 Fix GetSfxStr() not to use index in Xlat table.
 *            This will fix suffix problem reported by Travis on 1/11/2016.
 * 02/27/2017 Modify Quality2Code() to cover full range of value
 * 06/14/2017 Modify Quality2Code() to fix special case ".D"
 * 07/08/2017 Fix bug in GetSfxStr() that may crash when using with custom suffix list.
 * 03/10/2019 Add Code2City(int iCityCode) that returns pointer to city name.
 * 08/01/2019 Move parseStreet() & parseCitySt() to R01.cpp
 * 02/09/2021 Modify Load_N2CC() to support both different N2CC format
 * 05/26/2021 Add Abbr2CZ()
 * 09/13/2021 Modify Abbr2Code() to return 0 if not found.
 * 03/30/2022 Modify LoadXrefTable() to add iNameLen.  Add XrefNameIndex() to search on name instead of Code.
 * 07/14/2022 Update log msg
 ******************************************************************************/

#include "stdafx.h"
#include "Prodlib.h"
#include "Utils.h"
#include "Logs.h"

#define  _TABLES_DEF 
#include "Tables.h"
#include "R01.h"

IDX_TBL  tblPrclStat[] =
{
   "ACTIVE",    "A",
   "INACTIVE",  "I",
   "OTHER",     "O",
   "PENDING",   "P",
   "RETIRED",   "R",
   "SBE",       "S",
   "TRANSFERED","T",
   "VOIDED",    "V",
   "",""
};

CITY_INFO   myCityList[MAX_CITIES];
SUFFIX      asSuffixTbl[MAX_SUFFIX];
STRDIR      asStrDir[MAX_DIRS+1];
LU_ENTRY    asAttr[MAX_ATTR_ENTRIES];

int         iNumCities, iNumSuffixes, iNumXrefs;
int         iBadCity, iBadSuffix;

extern   bool bEnCode;
extern   bool bUseSfxXlat;

/************************************ InitDirs *******************************
 *
 * Initialize direction
 *
 *****************************************************************************/

void InitDirs()
{
   strcpy(asStrDir[0].acName, "N");
   asStrDir[0].pDir = asStrDir[0].acName;
   strcpy(asStrDir[1].acName, "S");
   asStrDir[1].pDir = asStrDir[1].acName;
   strcpy(asStrDir[2].acName, "E");
   asStrDir[2].pDir = asStrDir[2].acName;
   strcpy(asStrDir[3].acName, "W");
   asStrDir[3].pDir = asStrDir[3].acName;
   strcpy(asStrDir[4].acName, "NE");
   asStrDir[4].pDir = asStrDir[4].acName;
   strcpy(asStrDir[5].acName, "NW");
   asStrDir[5].pDir = asStrDir[5].acName;
   strcpy(asStrDir[6].acName, "SE");
   asStrDir[6].pDir = asStrDir[6].acName;
   strcpy(asStrDir[7].acName, "SW");
   asStrDir[7].pDir = asStrDir[7].acName;
   strcpy(asStrDir[8].acName, "NORTH");
   asStrDir[8].pDir = asStrDir[0].acName;
   strcpy(asStrDir[9].acName, "SOUTH");
   asStrDir[9].pDir = asStrDir[1].acName;
   strcpy(asStrDir[10].acName, "EAST");
   asStrDir[10].pDir = asStrDir[2].acName;
   strcpy(asStrDir[11].acName, "WEST");
   asStrDir[11].pDir = asStrDir[3].acName;
   strcpy(asStrDir[12].acName, "NO");
   asStrDir[12].pDir = asStrDir[0].acName;
   strcpy(asStrDir[13].acName, "SO");
   asStrDir[13].pDir = asStrDir[1].acName;

   asStrDir[14].pDir = NULL;
   asStrDir[14].acName[0] = 0;
}

char *GetStrDir(char *pDir)
{
   int   iTmp;

   if (!pDir || !*pDir)
      return NULL;

   iTmp = 0;
   while (iTmp < MAX_DIRS)
   {
      if (!strcmp(pDir, asStrDir[iTmp].acName))
         break;
      iTmp++;
   }

   if (iTmp < MAX_DIRS)
      return asStrDir[iTmp].pDir;
   return NULL;
}

/************************************ LoadCities *****************************
 *
 * Load N2C city table
 * iOldCityID can be a place holder for zipcode.
 *
 *****************************************************************************/

int LoadCities(char *pCityFile)
{
   char  acTmp[_MAX_PATH], *pTmp, *apItems[4];
   int   iRet=0, iTmp, iCnt;
   FILE  *fd;

   LogMsg0("Loading City table: %s", pCityFile);

   fd = fopen(pCityFile, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp <= iRet-1; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            iCnt = ParseString(acTmp, ',', 4, apItems);
            if (iCnt == 3)
            {  // BN,BANTA,2
               strcpy(myCityList[iTmp].acAbbr, apItems[0]);
               strcpy(myCityList[iTmp].acCityName, apItems[1]);
               myCityList[iTmp].iLen = strlen(apItems[1]);
               myCityList[iTmp].iCityID = atoi(apItems[2]);
               myCityList[iTmp].iOldCityID = 0;
            } else if (iCnt == 2)
            {  // BANTA,1
               myCityList[iTmp].acAbbr[0] = 0;
               strcpy(myCityList[iTmp].acCityName, apItems[0]);
               myCityList[iTmp].iLen = strlen(apItems[0]);
               myCityList[iTmp].iCityID = atoi(apItems[1]);
               myCityList[iTmp].iOldCityID = 0;
            } else if (iCnt == 4)
            {  // BN,BANTA,2,1
               strcpy(myCityList[iTmp].acAbbr, apItems[0]);
               strcpy(myCityList[iTmp].acCityName, apItems[1]);
               myCityList[iTmp].iLen = strlen(apItems[1]);
               myCityList[iTmp].iCityID = atoi(apItems[2]);
               myCityList[iTmp].iOldCityID = atoi(apItems[3]);
            } else 
               break;
            strcpy(myCityList[iTmp].acOfficialName, myCityList[iTmp].acCityName);
         } else
            break;
      }
      if (iTmp != iRet)
      {
         LogMsg("***** Bad number of city loaded %d.  Please verify %s", iTmp, pCityFile);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening city table %s", pCityFile);

   iNumCities = iRet -1;
   return iNumCities;
}

/************************************ LoadCities *****************************
 *
 * Load N2CX city table. This file contains official city name on the 3rd column
 * Format: City Code, Official city name, City ID, Previous City ID or Zip code
 *
 *****************************************************************************/

int Load_N2CX(char *pCityFile)
{
   char  acTmp[_MAX_PATH], *pTmp, *apItems[8];
   int   iRet=0, iTmp, iCnt;
   FILE  *fd;

   LogMsg0("Loading City table: %s", pCityFile);

   fd = fopen(pCityFile, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp <= iRet-1; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            iCnt = ParseString(acTmp, ',', 4, apItems);
            if (iCnt == 3)
            {  // AGUA,AGUA DULCE,3
               strcpy(myCityList[iTmp].acCityName, apItems[0]);
               myCityList[iTmp].iLen = strlen(apItems[0]);
               strcpy(myCityList[iTmp].acOfficialName, apItems[1]);
               myCityList[iTmp].iCityID = atoi(apItems[2]);
               myCityList[iTmp].iOldCityID = 0;
               myCityList[iTmp].acAbbr[0] = 0;
            } else if (iCnt == 4)
            {  // AGUA,AGUA DULCE,3,90210
               strcpy(myCityList[iTmp].acCityName, apItems[0]);
               myCityList[iTmp].iLen = strlen(apItems[0]);
               strcpy(myCityList[iTmp].acOfficialName, apItems[1]);
               myCityList[iTmp].iCityID = atoi(apItems[2]);
               myCityList[iTmp].iOldCityID = atoi(apItems[3]);
               myCityList[iTmp].acAbbr[0] = 0;
            } else 
               break;
         } else
            break;
      }
      if (iTmp != iRet)
      {
         LogMsg("***** Bad number of city loaded %d.  Please verify %s", iTmp, pCityFile);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening city table %s", pCityFile);

   iNumCities = iRet -1;
   return iNumCities;
}

/************************************ LoadCities *****************************
 *
 * Load N2CC city table
 * - Type1: Official CityName, old code, city code
 * - Type2: Mail CityName, old code, city code, Official CityName
 *
 *****************************************************************************/

int Load_N2CC(char *pCityFile)
{
   char  acTmp[_MAX_PATH], *pTmp, *apItems[6];
   int   iRet=0, iTmp, iFldCnt;
   FILE  *fd;

   LogMsg0("Loading City table: %s", pCityFile);

   fd = fopen(pCityFile, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);
      if (iRet > MAX_CITIES)
      {
         LogMsg("Please change MAX_CITIES in CityInfo.h to %d then rerun", iRet+1);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp <= iRet-1; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && *pTmp >= '0')
         {
            iFldCnt = ParseString(acTmp, ',', 6, apItems);
            if (iFldCnt < 3)
            {
               LogMsg("***** Invalid entry: [%s] (%d)", acTmp, iFldCnt);
               break;
            }

            strcpy(myCityList[iTmp].acCityName, apItems[0]);
            myCityList[iTmp].iLen = strlen(apItems[0]);
            myCityList[iTmp].iOldCityID = atoi(apItems[1]);
            myCityList[iTmp].iCityID = atoi(apItems[2]);
            if (iFldCnt > 3)
               strcpy(myCityList[iTmp].acOfficialName, apItems[3]);
            else
               strcpy(myCityList[iTmp].acOfficialName, myCityList[iTmp].acCityName);
         } else
            break;
      }

      if (iTmp != iRet)
      {
         LogMsg("***** Bad number of city loaded %d.  Please verify %s", iTmp, pCityFile);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening city table %s", pCityFile);

   return iRet-1;
}

/************************************ LoadCities *****************************
 *
 * Load city table
 * 10/23/2015  Reorder field population to avoid overwrite.
 *
 *****************************************************************************/

int LoadExCities(char *pCityFile)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iRet, iTmp;
   FILE  *fd;

   LogMsg0("Loading City table: %s", pCityFile);

   fd = fopen(pCityFile, "r");
   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iNumCities = atoi(acTmp);
      iRet = iNumCities;
      if (iNumCities > MAX_CITIES)
      {
         LogMsg("***** Please change MAX_CITIES in CityInfo.h to %d then rerun", iNumCities+1);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp <= iNumCities-1; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && *pTmp > '0')
         {
            myCityList[iTmp].iLen = strlen(myTrim(acTmp));
            strcpy(myCityList[iTmp].acCityName, pTmp);
            strcpy(myCityList[iTmp].acOfficialName, pTmp);
            myCityList[iTmp].iCityID = iTmp;
         } else
            break;
      }

      if (iTmp != iNumCities)
      {
         LogMsg("***** Bad number of city loaded %d.  Please verify %s", iTmp, pCityFile);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening city table %s", pCityFile);

   return iRet-1;
}

/******************************** LoadSuffixTbl ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int LoadSuffixTbl(char *pFilename, bool bUseXlat)
{
   char  acTmp[_MAX_PATH], *pTmp, *apItems[4];
   int   iRet=0, iTmp, iCnt;
   FILE  *fd;

   LogMsg0("Loading suffix table: %s", pFilename);

   fd = fopen(pFilename, "r");
   asSuffixTbl[0].acSuffix[0] = 0;              // Initialize first entry
   asSuffixTbl[0].acOrgSfx[0] = 0;
   asSuffixTbl[0].iLen = 0;

   if (fd)
   {
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iRet = atoi(acTmp);
      if (iRet < 10 || iRet >= MAX_SUFFIX)
      {
         LogMsg("***** Suffix table may be bad.  Please verify: %s", pFilename);
         fclose(fd);
         return 0;
      }

      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      for (iTmp = 1; iTmp < iRet; iTmp++)
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            myTrim(acTmp);
            if (bUseXlat)
            {
               iCnt = ParseString(acTmp, ',', 4, apItems);
               if (iCnt > 2)
                  asSuffixTbl[iTmp].iSfxID = atoi(apItems[2]);
               else
                  asSuffixTbl[iTmp].iSfxID = 0;

               strcpy(asSuffixTbl[iTmp].acOrgSfx, apItems[1]);
               strcpy(asSuffixTbl[iTmp].acSuffix, apItems[0]);
               asSuffixTbl[iTmp].iLen = strlen(apItems[0]);
            } else
            {
               asSuffixTbl[iTmp].iSfxID = 0;
               strcpy(asSuffixTbl[iTmp].acSuffix, acTmp);
               asSuffixTbl[iTmp].iLen = strlen(asSuffixTbl[iTmp].acSuffix);
            }
         } else
            break;
      }
      if (iTmp != iRet)
      {
         LogMsg("***** Bad number of suffixes loaded %d.  Please verify %s", iTmp, pFilename);
         iRet = 0;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening suffix table %s", pFilename);

   iNumSuffixes = iRet -1;
   return iNumSuffixes;
}

/********************************** City2Code ********************************
 *
 * Return > 0 if found.
 *
 *****************************************************************************/

int City2Code(char *pCityName, char *pCityCode, char *pApn)
{
   int   iTmp, iRet=0;
   char  acTmp[64], acCityCode[32];

   if (!bEnCode)
   {
      strcpy(pCityCode, pCityName);
      return iRet;
   }

   strcpy(acTmp, pCityName);
   blankRem(acTmp);
   acCityCode[0] = 0;
   memset(pCityCode, ' ', SIZ_S_CITY);

   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_strnicmp(acTmp, myCityList[iTmp].acCityName, myCityList[iTmp].iLen))
      {
         iRet = sprintf(acCityCode, "%d", myCityList[iTmp].iCityID);
         memcpy(pCityCode, acCityCode, iRet);
         iRet = iTmp;
         break;
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      if (*pCityName > ' ')
      {
         if (pApn && *pApn)
         {
            blankPad(acTmp, 30);
            LogMsg0("Invalid city name %.30s [%.14s]", acTmp, pApn);
         } else
            LogMsg0("Invalid city name %s", pCityName);
         iBadCity++;
      }
   }
   *(pCityCode+SIZ_S_CITY) = 0;

   return iRet;
}

/********************************** City2CodeEx ******************************
 *
 * Return city code and official city name
 *
 *****************************************************************************/

int City2CodeEx(char *pInCity, char *pCityCode, char *pOutCity, char *pApn)
{
   int   iTmp, iRet=0;
   char  acTmp[64], acCityCode[32];

   strcpy(acTmp, pInCity);
   blankRem(acTmp);
   acCityCode[0] = 0;
   *pOutCity = 0;
   memset(pCityCode, ' ', 4);

   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_strnicmp(acTmp, myCityList[iTmp].acCityName, myCityList[iTmp].iLen))
      {
         iRet = sprintf(acCityCode, "%d", myCityList[iTmp].iCityID);
         memcpy(pCityCode, acCityCode, iRet);
         strcpy(pOutCity, myCityList[iTmp].acOfficialName);
         iRet = iTmp;
         break;
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      if (*pInCity > ' ')
      {
         if (pApn && *pApn)
         {
            blankPad(acTmp, 30);
            LogMsg0("Invalid city name %.30s [%.14s]", acTmp, pApn);
         } else
            LogMsg0("Invalid city name %s", pInCity);
         iBadCity++;
      }
   }
   *(pCityCode+3) = 0;

   return iRet;
}

/********************************** City2Code2 *******************************
 *
 * Used by parseAdr1_5() to support MPA.  This is useful where city name is 
 * chopped off liked in MPA.
 *
 *****************************************************************************/

int City2Code2(char *pCityName, char *pCityCode, char *pApn)
{
   int   iTmp, iCmpLen, iRet=0;
   char  acTmp[64], acCityCode[32];

   if (!bEnCode)
   {
      strcpy(pCityCode, pCityName);
      return iRet;
   }

   memset(pCityCode, ' ', SIZ_S_CITY);
   strcpy(acTmp, pCityName);
   iCmpLen = blankRem(acTmp);
   if (iCmpLen == SIZ_M_CITY)
   {
      for (iTmp = 1; iTmp <= iNumCities; iTmp++)
      {
         if (!_memicmp(acTmp, myCityList[iTmp].acCityName, iCmpLen))
         {
            iCmpLen = sprintf(acCityCode, "%d", myCityList[iTmp].iCityID);
            memcpy(pCityCode, acCityCode, iCmpLen);
            iRet = iTmp;
            break;
         }
      }
   } else
   {
      for (iTmp = 1; iTmp <= iNumCities; iTmp++)
      {
         if (!strncmp(acTmp, myCityList[iTmp].acCityName, myCityList[iTmp].iLen))
         {
            iCmpLen = sprintf(acCityCode, "%d", myCityList[iTmp].iCityID);
            memcpy(pCityCode, acCityCode, iCmpLen);
            iRet = iTmp;
            break;
         }
      }
   }

   *(pCityCode+SIZ_S_CITY) = 0;

   return iRet;
}

/********************************** City2Zip *********************************
 *
 * iOldCityID is actually zipcode in N2CZ table.  0 is not available.
 *
 *****************************************************************************/

char *City2Zip(char *pCityName, char *pZipCode)
{
   int   iTmp, iRet;
   char  acTmp[64], acCityCode[32], *pRet;

   strcpy(acTmp, pCityName);
   blankRem(acTmp);
   acCityCode[0] = 0;
   strcpy(pZipCode, "     ");

   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_strnicmp(acTmp, myCityList[iTmp].acCityName, myCityList[iTmp].iLen))
      {
         if (myCityList[iTmp].iOldCityID > 0)
         {
            iRet = sprintf(acCityCode, "%.5d", myCityList[iTmp].iOldCityID);
            memcpy(pZipCode, acCityCode, iRet);
         } else
            iTmp = iNumCities+1;
         break;
      }
   }

   if (iTmp > iNumCities)
      pRet = NULL;
   else
   {
      *(pZipCode+5) = 0;
      pRet = pZipCode;
   }

   return pRet;
}

/********************************** GetCityName ******************************
 *
 *
 *****************************************************************************/

char *GetCityName(int iCityCode)
{
   char *pRet = NULL;

   for (int iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (iCityCode == myCityList[iTmp].iCityID)
      {
         pRet = (char *)&myCityList[iTmp].acOfficialName[0];
         break;
      }
   }
   return pRet;
}


/********************************** Abbr2Code ********************************
 *
 * Return 0 if not found.
 *
 *****************************************************************************/

int Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName, char *pApn)
{
   int  iTmp;
   char *pAbbr = myTrim(pCityAbbr);

   *pCityCode = 0;
   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_stricmp(pAbbr, myCityList[iTmp].acAbbr))
      {
         if (!bEnCode)
            sprintf(pCityCode, "%s  ", myCityList[iTmp].acCityName);          
         else
            sprintf(pCityCode, "%d  ", myCityList[iTmp].iCityID);

         if (pCityName)
         {
            memset(pCityName, ' ', SIZ_S_CITY);
            sprintf(pCityName, "%s", myCityList[iTmp].acCityName);          
         }
         break;
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      if (*pCityAbbr > ' ')
      {
         if (pApn && *pApn >= '0')
            LogMsg0("Abbr2Code()->Invalid city abbr %s, APN=%.14s", pCityAbbr, pApn);
         else
            LogMsg0("Abbr2Code()->Invalid city abbr %s.", pCityAbbr);
         iBadCity++;
      }
      memset(pCityCode, ' ', SIZ_S_CITY);
      pCityCode[SIZ_S_CITY] = 0;
      if (pCityName)
      {
         memset(pCityName, ' ', SIZ_S_CITY);
         *(pCityName+SIZ_S_CITY) = 0;
      }

      iTmp = 0;
   }

   return iTmp;
}

/********************************** Abbr2Code ********************************
 *
 * Special version that requires matching CityAbbr and zipcode for city name (EDX)
 *
 *****************************************************************************/

int Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName, int iZipCode, char *pApn)
{
   int  iTmp;
   char *pAbbr = myTrim(pCityAbbr);

   *pCityCode = 0;
   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_stricmp(pAbbr, myCityList[iTmp].acAbbr))
      {
         if (iZipCode > 0 && iZipCode == myCityList[iTmp].iOldCityID)
         {
            sprintf(pCityCode, "%d  ", myCityList[iTmp].iCityID);
            if (pCityName)
               sprintf(pCityName, "%s", myCityList[iTmp].acCityName);          
            break;
         }
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      if (*pCityAbbr > ' ')
      {
         if (pApn && *pApn >= '0')
            LogMsg0("Abbr2Code()->Invalid city abbr %s, APN=%.14s", pCityAbbr, pApn);
         else
            LogMsg0("Abbr2Code()->Invalid city abbr %s.", pCityAbbr);
         iBadCity++;
      }
      memset(pCityCode, ' ', SIZ_S_CITY);
      pCityCode[SIZ_S_CITY] = 0;
      if (pCityName)
      {
         memset(pCityName, ' ', SIZ_S_CITY);
         *(pCityName+SIZ_S_CITY) = 0;
      }
      iTmp = 0;
   }

   return iTmp;
}

/********************************** Abbr2CZ **********************************
 *
 * Special version that returns zip code if available
 * Return -1 if not found
 *
 *****************************************************************************/

int Abbr2CZ(char *pCityAbbr, char *pCityCode, char *pCityName, char *pZipCode, char *pApn)
{
   int  iTmp;
   char *pAbbr = myTrim(pCityAbbr);

   *pCityCode = 0;
   *pZipCode = 0;
   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_stricmp(pAbbr, myCityList[iTmp].acAbbr))
      {
         if (!bEnCode)
            sprintf(pCityCode, "%s  ", myCityList[iTmp].acCityName);          
         else
            sprintf(pCityCode, "%d  ", myCityList[iTmp].iCityID);

         if (pCityName)
         {
            memset(pCityName, ' ', SIZ_S_CITY);
            sprintf(pCityName, "%s", myCityList[iTmp].acCityName);          
         }

         if (myCityList[iTmp].iOldCityID >= 90000)
            sprintf(pZipCode, "%d", myCityList[iTmp].iOldCityID);          

         break;
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      if (*pCityAbbr > ' ')
      {
         if (pApn && *pApn >= '0')
            LogMsg0("Abbr2Code()->Invalid city abbr %s, APN=%.14s", pCityAbbr, pApn);
         else
            LogMsg0("Abbr2Code()->Invalid city abbr %s.", pCityAbbr);
         iBadCity++;
      }
      memset(pCityCode, ' ', SIZ_S_CITY);
      pCityCode[SIZ_S_CITY] = 0;
      if (pCityName)
      {
         memset(pCityName, ' ', SIZ_S_CITY);
         *(pCityName+SIZ_S_CITY) = 0;
      }
      iTmp = -1;
   }

   return iTmp;
}

/********************************** Abbr2City *******************************
 *
 * 12/19/2010 Trim pCityAbbr before compare.
 *
 *****************************************************************************/

void Abbr2City(char *pCityAbbr, char *pCityName)
{
   int iTmp;
   char *pAbbr = myTrim(pCityAbbr);

   *pCityName = 0;
   if (*pAbbr <= ' ')
      return;

   for (iTmp = 1; iTmp <= iNumCities; iTmp++)
   {
      if (!_stricmp(pAbbr, myCityList[iTmp].acAbbr))
      {
         strcpy(pCityName, myCityList[iTmp].acCityName);          
         break;
      }
   }

   if (iTmp > iNumCities)
   {
      // Not found
      LogMsg0("Abbr2City()->Invalid city abbr %s.", pAbbr);
      iBadCity++;
   }
}

/*********************************** Sfx2Code *******************************
 *
 *
 *****************************************************************************/

void Sfx2Code(char *pSuffix, char *pSfxCode)
{
   int iTmp;
 
   if (!*pSuffix || *pSuffix == ' ')
   {
      strcpy(pSfxCode, "     ");
      return;
   }

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      //if (!memcmp(pSuffix, asSuffixTbl[iTmp].acSuffix, asSuffixTbl[iTmp].iLen))
      if (!_stricmp(pSuffix, asSuffixTbl[iTmp].acSuffix))
      {
         if (asSuffixTbl[iTmp].iSfxID > 0)
            sprintf(pSfxCode, "%d     ", asSuffixTbl[iTmp].iSfxID);
         else
            sprintf(pSfxCode, "%d     ", iTmp);
         break;
      }
   }

   if (iTmp > iNumSuffixes)
   {
      // Not found
      LogMsg0("Invalid suffix: %s", pSuffix);
      strcpy(pSfxCode, "     ");
      iBadSuffix++;
   }
}

/********************************* GetSfxCode ********************************
 *
 * Return suffix index.  If not found, return 0.
 *
 *****************************************************************************/

int GetSfxCode(char *pSuffix)
{
   int   iTmp, iRet=0;

   if (!pSuffix || *pSuffix <= ' ')
      return iRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      //if (!memcmp(pSuffix, asSuffixTbl[iTmp].acSuffix, asSuffixTbl[iTmp].iLen))
      if (!_stricmp(pSuffix, asSuffixTbl[iTmp].acSuffix))
      {
         if (asSuffixTbl[iTmp].iSfxID > 0)
            iRet = asSuffixTbl[iTmp].iSfxID;
         else
            iRet = iTmp;
         break;
      }
   }

   return iRet;
}

STRSFX *GetSfxCode(char *pSuffix, STRSFX *pSfxTbl)
{
   int     iRet=0;
   STRSFX  *pRet = NULL;

   if (!*pSuffix || *pSuffix == ' ')
      return NULL;

   while (pSfxTbl->iLen > 0)
   {
      if (!strcmp(pSuffix, pSfxTbl->pOrgSfx))
      {
         pRet = pSfxTbl;
         break;
      }
      pSfxTbl++;
   }

   return pRet;
}

/********************************* GetSfxCodeX *******************************
 *
 * Return suffix index and translated suffix string.  If not found, return 0.
 * To use this function, make sure to set UseSfxDev=Y in ini file under each
 * county section.
 *
 *****************************************************************************/

int GetSfxCodeX(char *pOrgSfx, char *pSuffix)
{
   int   iTmp, iRet=0;

   myTrim(pOrgSfx);
   *pSuffix = 0;
   if (!*pOrgSfx || *pOrgSfx == ' ')
      return iRet;

   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pOrgSfx, asSuffixTbl[iTmp].acOrgSfx) )
      {
         strcpy(pSuffix, asSuffixTbl[iTmp].acSuffix); 
         if (asSuffixTbl[iTmp].iSfxID > 0)
            iRet = asSuffixTbl[iTmp].iSfxID;
         else
            iRet = GetSfxCode(pSuffix);
         break;
      }
   }

   return iRet;
}

int GetSfxDev(char *pSuffix)
{
   int   iTmp, iRet=0;

   if (!*pSuffix || *pSuffix == ' ')
      return iRet;

   iTmp = remChar(pSuffix, 44);
   for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
   {
      if (!strcmp(pSuffix, asSuffixTbl[iTmp].acOrgSfx) )
      {
         if (asSuffixTbl[iTmp].iSfxID > 0)
            iRet = asSuffixTbl[iTmp].iSfxID;
         else
         {
            char  acTmp[32];
            strcpy(acTmp, asSuffixTbl[iTmp].acSuffix); 
            iRet = GetSfxCode(acTmp);
         }
         break;
      }
   }

   return iRet;
}

/********************************* GetSfxCode ********************************
 *
 * Return suffix string.
 *
 *****************************************************************************/

char  *GetSfxStr(int iSfxIdx)
{
   char *pRet = NULL;

   if (iSfxIdx > 0)
   {
      if (!bUseSfxXlat && iSfxIdx <= iNumSuffixes)
         pRet = (char *)&asSuffixTbl[iSfxIdx].acSuffix[0];
      else
      {
         int iTmp;
         for (iTmp = 1; iTmp <= iNumSuffixes; iTmp++)
         {
            if (iSfxIdx == asSuffixTbl[iTmp].iSfxID)
            pRet = (char *)&asSuffixTbl[iTmp].acSuffix[0];
         }
      }
   }
   return pRet;
}

/************************************ LoadLUTable *****************************
 *
 * 
 *
 *****************************************************************************/

int LoadLUTable(char *pLUFile, char *pTableName, LU_ENTRY *pTable, int iMaxEntries)
{
   char  acTmp[_MAX_PATH], *pTmp;
   int   iTmp;
   FILE  *fd;
   LU_ENTRY *pAttrTbl;

   fd = fopen(pLUFile, "r");
   if (fd)
   {
      //iTmp = 0;
      // Find table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && !_stricmp(myTrim(acTmp), pTableName))
            break;
         //iTmp++;
      }

      // Determine local or external table
      if (!pTable)
         pAttrTbl = (LU_ENTRY *)&asAttr[0];
      else
         pAttrTbl = pTable;

      iTmp = 0;
      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp && *pTmp > ' ')
         {
            strcpy(pAttrTbl->acIndex, myTrim(acTmp));
            pTmp = strchr(pAttrTbl->acIndex, ' ');
            *pTmp = 0;                             // Mark end of index string
            iTmp++;
            pAttrTbl++;
            if (iTmp >= iMaxEntries)
            {
               pAttrTbl->acIndex[0] = 0;
               pAttrTbl->acValue[0] = 0;
               break;
            }
         } else
            break;
      }

      fclose(fd);
   } else
      LogMsg("***** Error opening lookup table %s", pLUFile);

   return iTmp;
}

/********************************* Code2Desc *********************************
 *
 * Using code lookup in lookup table to find description and return string value.
 * If not found, return empty string.
 *
 *****************************************************************************/

char *Code2Value(char *pCode, LU_ENTRY *pTable, char *pValue)
{
   int      iRet=-1, iTmp=0;
   LU_ENTRY *pLkupTbl;
   char     *pRet;

   pLkupTbl = pTable;   
   pRet = "";

   if (pValue)
      *pValue = 0;

   while (pLkupTbl->acIndex[0] > ' ')
   {
      if (!strcmp(pCode, pLkupTbl->acIndex))
      {
         pRet = pLkupTbl->acValue;
         if (pValue)
            strcpy(pValue, pRet);
         break;
      }
      pLkupTbl++;
   }
   return pRet;
}

/********************************** Value2Code *******************************
 *
 * Return -1 if not found, else index number
 *
 *****************************************************************************/

int Value2Code(char *pValue, char *pCode, LU_ENTRY *pTable)
{
   int      iRet=-1, iTmp=0;
   LU_ENTRY *pAttrTbl;

   if (!pTable)
      pAttrTbl = (LU_ENTRY *)&asAttr[0];
   else
      pAttrTbl = pTable;
   
   *pCode = 0;
   while (pAttrTbl->acIndex[0] > ' ')
   {
      if (!_stricmp(pValue, pAttrTbl->acValue))
      {
         strcpy(pCode, pAttrTbl->acIndex);
         iRet = iTmp;
         break;
      }
      iTmp++;
      pAttrTbl++;
   }
   return iRet;
}

/********************************* Quality2Code ******************************
 *
 * Rounding down to the nearest .5 so it will match with table calue - 10/31/2013.
 * Return -1 if not found, else index number
 *
 *****************************************************************************/

int Quality2Code(char *pValue, char *pCode, LU_ENTRY *pTable)
{
   int      iRet=-1, iTmp=0;
   LU_ENTRY *pAttrTbl;
   char     acTmp[16];
   double   dTmp;

   if (!pTable)
      pAttrTbl = (LU_ENTRY *)&asAttr[0];
   else
      pAttrTbl = pTable;
   
   *pCode = 0;
   if (*pValue >= '0' && *pValue <= '9')
   {

      // Special case - .D is translate to *1000 (7.D=7000)
      if (*(pValue+2) == 'D' && *(pValue+1) == '.')
         *(pValue+2) = 0;

      dTmp = atof(pValue);
      if (dTmp > 99.9)
         dTmp /= 100.0;
      else if (dTmp > 13.0)
         dTmp /= 10.0;

      // Round down - Start out with most popular value
      if (dTmp > 4.5 && dTmp <= 5.0)
         dTmp = 5.0;
      else if (dTmp > 5.0 && dTmp <= 5.5)
         dTmp = 5.5;
      else if (dTmp > 5.5 && dTmp <= 6.0)
         dTmp = 6.0;
      else if (dTmp > 6.0 && dTmp <= 6.5)
         dTmp = 6.5;
      else if (dTmp > 6.5 && dTmp <= 7.0)
         dTmp = 7.0;
      else if (dTmp > 7.0 && dTmp <= 7.5)
         dTmp = 7.5;
      else if (dTmp > 7.5 && dTmp <= 8.0)
         dTmp = 8.0;
      else if (dTmp > 8.0 && dTmp <= 8.5)
         dTmp = 8.5;
      else if (dTmp > 8.5 && dTmp <= 9.0)
         dTmp = 9.0;
      else if (dTmp > 3.5 && dTmp <= 4.0)
         dTmp = 4.0;
      else if (dTmp > 4.0 && dTmp <= 4.5)
         dTmp = 4.5;
      else if (dTmp > 2.5 && dTmp <= 3.0)
         dTmp = 3.0;
      else if (dTmp > 3.0 && dTmp <= 3.5)
         dTmp = 3.5;
      else if (dTmp > 1.5 && dTmp <= 2.0)
         dTmp = 2.0;
      else if (dTmp > 2.0 && dTmp <= 2.5)
         dTmp = 2.5;
      else if (dTmp > 0.5 && dTmp <= 1.0)
         dTmp = 1.0;
      else if (dTmp > 1.0 && dTmp <= 1.5)
         dTmp = 1.5;
      else if (dTmp > 0.1 && dTmp <= 0.5)
         dTmp = 0.5;

      if (dTmp < 12.9)
         sprintf(acTmp, "%1.1f", dTmp);
      else
         return iRet;
   } else
      strcpy(acTmp, pValue);

   while (pAttrTbl->acIndex[0] > ' ')
   {
      if (!_stricmp(acTmp, pAttrTbl->acValue))
      {
         strcpy(pCode, pAttrTbl->acIndex);
         iRet = iTmp;
         break;
      }
      iTmp++;
      pAttrTbl++;
   }
   return iRet;
}

int Code2Quality(char *pCode, char *pQual, LU_ENTRY *pTable)
{
   int      iRet=-1, iTmp=0;
   LU_ENTRY *pAttrTbl;

   if (!pTable)
      pAttrTbl = (LU_ENTRY *)&asAttr[0];
   else
      pAttrTbl = pTable;
   
   *pQual = 0;

   while (pAttrTbl->acIndex[0] > ' ')
   {
      if (*pCode == pAttrTbl->acIndex[0])
      {
         strcpy(pQual, pAttrTbl->acValue);
         iRet = iTmp;
         break;
      }
      iTmp++;
      pAttrTbl++;
   }
   return iRet;
}

/********************************* LoadXrefTable *****************************
 *
 * Load a comma delimited file into XREFTBL table for translation
 * File format: Code, Value, Index.  
 * Use Xref*() functions to access and retrieve data.
 *
 *****************************************************************************/

int LoadXrefTable(char *pXrefFile, XREFTBL *pTable, int iMaxEntries)
{
   char     acTmp[_MAX_PATH], *pTmp, *apItems[8];
   int      iTmp=0, iCnt;
   FILE     *fd;
   XREFTBL  *pXrefTbl;

   LogMsg("Loading Xref table: %s", pXrefFile);
   fd = fopen(pXrefFile, "r");
   if (fd)
   {
      pXrefTbl = pTable;
      pTmp = fgets(acTmp, _MAX_PATH, fd);
      iCnt = atoi(pTmp);         // First row is number of entries
      if (iCnt > iMaxEntries)
         LogMsg("Table will be truncated due to entry count (%d) is greater than max (%d) allowed", iCnt, iMaxEntries);

      // Loading table
      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (!pTmp)
            break;

         iCnt = ParseString(acTmp, ',', 6, apItems);

         if (iCnt > 2)
         {
            strcpy(pXrefTbl->acCode, apItems[0]);
            strcpy(pXrefTbl->acName, apItems[1]);
            pXrefTbl->iIdxNum = atoi(apItems[2]);
            pXrefTbl->iCodeLen = strlen(apItems[0]);
            pXrefTbl->iNameLen = strlen(apItems[1]);

            if (iCnt > 3)
            {
               if (iCnt > 4)
                  strcpy(pXrefTbl->acOther, apItems[4]);
               strcpy(pXrefTbl->acFlags, apItems[3]);
            } else
            {
               memset(pXrefTbl->acOther, ' ', sizeof(pXrefTbl->acOther));
               memset(pXrefTbl->acFlags, ' ', sizeof(pXrefTbl->acFlags));
            }
            
            iTmp++;
            pXrefTbl++;
            if (iTmp >= iMaxEntries)
            {
               pXrefTbl->acCode[0] = 0;
               pXrefTbl->acName[0] = 0;
               pXrefTbl->acFlags[0] = 0;
               pXrefTbl->iIdxNum = 0;
               pXrefTbl->iCodeLen = 8;
               break;
            }
         } else
         {
            LogMsg0("Table entries are inconsistent at: %s (%d)", pTmp, iTmp);
            break;
         }
      }

      fclose(fd);
   } else
      LogMsg("Error opening XREF table %s", pXrefFile);

   return iTmp;
}

int XrefCode2Idx(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries)
{
   int      iRet=-1, iTmp;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return iRet;
   if (!pCode)
      return 0;
   
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (!_memicmp(pCode, pXrefTbl->acCode, pXrefTbl->iCodeLen))
      {
         iRet = pXrefTbl->iIdxNum;
         break;
      }
   }
   return iRet;
}

// Compare code and returns index
int XrefCodeIndex(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries)
{
   int      iRet=-1, iTmp;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return iRet;
   if (!pCode)
      return iRet;
   
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (!_memicmp(pCode, pXrefTbl->acCode, pXrefTbl->iCodeLen))
      {
         iRet = iTmp;
         break;
      }
   }
   return iRet;
}

// Compare name and returns index
int XrefNameIndex(XREFTBL *pTable, LPCSTR pName, int iMaxEntries)
{
   int      iRet=-1, iTmp;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return iRet;
   if (!pName)
      return iRet;
   
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (!_memicmp(pName, pXrefTbl->acName, pXrefTbl->iNameLen))
      {
         iRet = iTmp;
         break;
      }
   }
   return iRet;
}

// Find exact code
int XrefCodeIndexEx(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries)
{
   int      iRet=-1, iTmp, iLen;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return iRet;
   if (!pCode)
      return iRet;
   
   iLen = strlen(pCode);
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (iLen == pXrefTbl->iCodeLen && !_memicmp(pCode, pXrefTbl->acCode, pXrefTbl->iCodeLen))
      {
         iRet = iTmp;
         break;
      }
   }
   return iRet;
}

char *XrefCode2Name(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries)
{
   int      iTmp;
   char     *pRet=NULL;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return pRet;
   if (!pCode)
      return pRet;
   
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (!_memicmp(pCode, pXrefTbl->acCode, pXrefTbl->iCodeLen))
      {
         pRet = pXrefTbl->acName;
         break;
      }
   }
   return pRet;
}

char *XrefIdx2Name(XREFTBL *pTable, int iIdx, int iMaxEntries)
{
   int      iTmp;
   char     *pRet=NULL;
   XREFTBL  *pXrefTbl;

   if (!pTable)
      return pRet;
   if (!iIdx)
      return pRet;
   
   pXrefTbl = pTable;   
   for (iTmp = 0; iTmp < iMaxEntries; iTmp++,pXrefTbl++)
   {
      if (iIdx == pXrefTbl->iIdxNum)
      {
         pRet = pXrefTbl->acName;
         break;
      }
   }
   return pRet;
}

/*********************************** Code2Sfx *******************************
 *
 *
 ****************************************************************************/

void Code2Sfx(char *pSfxCode, char *pSuffix)
{
   int iTmp;

   iTmp = atoin(pSfxCode, 3);
   if (iTmp < 1 || iTmp > iNumSuffixes)
      strcpy(pSuffix, "        ");
   else
   {
      strcpy(pSuffix, asSuffixTbl[iTmp].acSuffix);
      blankPad(pSuffix, SIZ_S_SUFF);
   }
}

/*********************************** Code2City ******************************
 *
 *
 ****************************************************************************/

void Code2City(char *pCityCode, char *pCity)
{
   int iTmp;

   iTmp = atoin(pCityCode, 3);
   if (iTmp < 1 || iTmp > iNumCities)
      *pCity = 0;
   else
   {
      strcpy(pCity, myCityList[iTmp].acOfficialName);
   }
}

char *Code2City(int iCityCode)
{
   char *pRet;

   if (iCityCode < 1 || iCityCode > iNumCities)
      pRet = NULL;
   else
      pRet =  myCityList[iCityCode].acOfficialName;

   return pRet;
}

void Code2CityEx(char *pCityCode, char *pCity)
{
   int iTmp, iCity;

   iCity = atoin(pCityCode, 3);
   *pCity = 0;
   if (iCity > 0 && iCity <= iNumCities)
   {
      for (iTmp = 1; iTmp <= iNumCities; iTmp++)
      {
         if (iCity == myCityList[iTmp].iCityID)
         {
            strcpy(pCity, myCityList[iTmp].acOfficialName);
            break;
         }
      }
   }
}

/******************************* GetParcelStatus ****************************
 *
 *
 ****************************************************************************/

char *GetParcelStatus(char *pCode)
{
   int   iTmp=0;
   char *pRet="";

   while (*tblPrclStat[iTmp].pCode)
   {
      if (*pCode == *tblPrclStat[iTmp].pCode)
      {
         pRet = tblPrclStat[iTmp].pName;
         break;
      }
      iTmp++;
   }
   return pRet;
}
