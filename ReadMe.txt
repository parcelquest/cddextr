Usage:
   CddExtr -C<County code> -A<option> -X<option> -N<option> -D<Delimiter> -H<Header Y/N> -F -Q -O

 * Options:
 *    -C : County code
 *    -D : Delimiter (comma, vertical bar, ...)
 *    -F : Output fix length records
 *    -H : Header(Y/N)
 *    -Q : Remove single quote from specfied fields
 *    -Nu: Don't extract unsecured parcels
 *    -T:  Extracted module
 *    -Vi: Verify import file
 *    -Xa: Extract Alt Apn or Fee Parcel
 *    -Xl: Extract long/lat file
 *    -Xv: Extract value file
 *    -Xe: Extract exemption code
 *    -Xf: Extract full exemption
 *    -Xd: Extract long legal description file
 *    -XAll: Extract all web bulk module
 *    -Z[r]: Zip output files (option to remove existing zip file before compress)
 *
 * LDR extract:
 *    -C<XXX> -Q -Zr -XAll -T<Bulk|WebImport|ITrac>
 *
 * Normal extract:
 *    -C<XXX> -Q -Z -Xd -T<Bulk|WebImport|ITrac>

 Other options please CddExtr.ini

After LoadXXX, Products.State is set to 'P' if success or 'F' if failed or 'W' if program crashed
After BuildNdx, Products.State is set to 'B'
After verification, Products.State is set to 'V'
After Copy2Web, it is set to 'C'.  This process is run twice daily at 5:00AM and 10:15PM
CddExt should check for 'V' and schedule to run every hour after verification and before Copy2Web

CleanProd check for 'C' or 'V' to rename R01 to S01

*** bulk import ***
cddextr -C??? -Q -TWebImport
bcp spatial.dbo.amador_import in G:\CO_IMPORT\Web\Amador_Import.csv -c -e H:\CO_LOGS\CddExtr\Ama_Import.log -T -t"|" -r\n -a65535 -k -F2

*** right truncation error ***
1) Make sure you point to the right database (i.e. dev2\sql2008)
2) Data might be longer than actual field

*** QUOTED_IDENTIFIER error ***
1) Use -q on bcp command line


*** Extract Module ***
Based on our agreement that each new module will create a new data set, here are my
proposed layouts:

FIPS / APN / LongLegal = D
FIPS / APN / FULL-EXEMPT = E
FIPS / APN / Lat / Lon = Centroid "L"

Record Layout
-------------
1. Input for bulk extract: G01Rec.csv
2. Output bulk extract: Pq4Extr*.csv
3. Input for webimport extract: WebImport.csv
4. Output import extract: WIExtrLayout.csv


Notes
-----
06/16/2011
1) Modify CDDEXTR to pull HSENO from old place if new one is not yet populated.
2) All Load programs need modification to populate new HSENO

09/26/2012
1) Modify CDExtr to add DocLinks (done - just setting AddDocLink=Y in INI file)
2) Write program to compare MaxAutoExtr with LastRecsChg then
   - delete Chk file
   - extract whole file
   - Set flag to import
3) Modify ImportDB to import whole file first before doing update

12/03/2013
1) Replace G01Rec_1.csv with G01Rec.csv for CCX, GLE, LAS, SBX, SDX, YOL
2) Add XCTbl to BulkExtrLayout table in Production DB
3) Comment out LoadLayout() since it's redundant

8/1/2014 : Add DocLinks to XCA_Roll
1) From SMS, run "ALTER TABLE [dbo].[XCA_Roll] ADD [Doc1Link] varchar(64) NULL, [Doc2Link] varchar(64) NULL,[Doc3Link] varchar(64) NULL, [XferLink] varchar(64) NULL"
2) Modify cmdCreateXCATbl.sql to add new fields
3) Modify spInsertData store procedure to add new fields
5) Run CreateXCATbl.cmd

01/05/2015: Add -Xm option to create "M" file

06/30/2015: Add DBA to webimport output.  Add OS module

10/23/2015: Add O1 & O2 modules for Denise
1) Run CddExtr with -Xo1(for KER) or -Xo2(for TEH)
2) Rename O1 or O2 file to 4D
3) Edit the 4D file by changing first row ALT_APN or PREV_APN to MODAPN.
