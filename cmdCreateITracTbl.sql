USE spatial;
GO

IF exists (SELECT 1 FROM sys.tables WHERE name = '$(Cnty)_LDR$(Year)')
DROP TABLE $(Cnty)_LDR$(Year)
GO

EXEC spCreateITracRollTbl $(Cnty), '$(Year)';
GO
