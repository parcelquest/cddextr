-- This procedure needs revise in order to work.  The sql statement
-- is too long.  It has to be below 4000 bytes

USE [Spatial]
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[spCreateITracTbl]
   @Cnty [varchar](3),
   @LdrYear [varchar](2)
WITH EXECUTE AS CALLER
AS
Declare @sql as nvarchar(4000)

BEGIN
   set @sql=N'CREATE TABLE [dbo].[' + @Cnty + '_LDR' + @LdrYear + '] (
      [APN] [varchar](14) NOT NULL,
      [APN_D] [varchar](17) NULL,
      [ALT_APN] [varchar](14) NULL,
      [PREV_APN] [varchar](14) NULL,
      [CNTYNUM] [smallint] NULL,
      [CNTYCODE] [char](3) NULL,
      [STATUS] [char](1) NULL,
      [YR_ASSD] [smallint] NULL,
      [DEL_YR] [smallint] NULL,
      [TRA] [varchar](8) NULL,
      [NAME1] [varchar](52) NULL,
      [NAME2] [varchar](26) NULL,
      [NAMES] [varchar](96) NULL,
      [VEST] [varchar](16) NULL,
      [CARE_OF] [varchar](50) NULL,
      [ZONING] [varchar](10) NULL,
      [USE_STD] [varchar](3) NULL,
      [USE_CO] [varchar](8) NULL,
      [LEGAL] [varchar](100) NULL,
      [S_STRNUM] [int] NULL,
      [S_FRA] [varchar](4) NULL,
      [S_DIR] [varchar](2) NULL,
      [S_STREET] [varchar](24) NULL,
      [S_SUFFIX] [smallint] NULL,
      [S_UNIT] [varchar](6) NULL,
      [S_CITY] [smallint] NULL,
      [S_ST] [char](2) NULL,
      [S_ZIP] [varchar](5) NULL,
      [S_ZIP4] [varchar](4) NULL,
      [M_STRNUM] [varchar](7) NULL,
      [M_FRA] [varchar](3) NULL,
      [M_DIR] [varchar](2) NULL,
      [M_STREET] [varchar](24) NULL,
      [M_SUFFIX] [varchar](5) NULL,
      [M_UNIT] [varchar](6) NULL,
      [M_CITY] [varchar](17) NULL,
      [M_ST] [char](2) NULL,
      [M_ZIP] [varchar](5) NULL,
      [M_ZIP4] [varchar](4) NULL,
      [TRACT] [varchar](6) NULL,
      [BLOCK] [varchar](6) NULL,
      [LOT] [varchar](6) NULL,
      [RATIO] [smallint] NULL,
      [LAND_VAL] [bigint] NULL,
      [IMPR_VAL] [bigint] NULL,
      [OTHER_VAL] [bigint] NULL,
      [EXE_TOTAL] [bigint] NULL,
      [GROSS_VAL] [bigint] NULL,
      [TAX_AMT] [bigint] NULL,
      [MINERAL] [bigint] NULL,
      [FIXTR] [bigint] NULL,
      [HOMESITE] [bigint] NULL,
      [TIMBERVAL] [bigint] NULL,
      [TREEVINES] [bigint] NULL,
      [PERSPROP] [bigint] NULL,
      [BUSINV] [bigint] NULL,
      [PP_MH] [bigint] NULL,
      [GROWIMPR] [bigint] NULL,
      [CLCALAND] [bigint] NULL,
      [CLCAIMPR] [bigint] NULL,
      [OTH_IMPR] [bigint] NULL,
      [FIXTR_RP] [bigint] NULL,
      [HH_PP] [bigint] NULL,
      [MAP_DIV] [char](1) NULL,
      [PUBL_FLG] [char](1) NULL,
      [UNS_FLG] [char](1) NULL,
      [PROP8_FLG] [char](1) NULL,
      [FULLEXE_FLG] [char](1) NULL,
      [TIMBER] [char](1) NULL,
      [AG_PRE] [char](1) NULL,
      [HO_FL] [char](1) NULL,
      [MULTI_APN] [char](1) NULL,
      [SALE1_DATE] [int] NULL,
      [SALE1_DOC] [varchar](12) NULL,
      [SALE1_AMT] [bigint] NULL,
      [SALE1_CODE] [varchar](13) NULL,
      [SALE1_DOCTYPE] [smallint] NULL,
      [SALE2_DATE] [int] NULL,
      [SALE2_DOC] [varchar](12) NULL,
      [SALE2_AMT] [bigint] NULL,
      [SALE2_CODE] [varchar](13) NULL,
      [SALE2_DOCTYPE] [smallint] NULL,
      [SALE3_AMT] [bigint] NULL,
      [SALE3_DATE] [int] NULL,
      [SALE3_DOC] [varchar](12) NULL,
      [SALE3_CODE] [varchar](13) NULL,
      [SALE3_DOCTYPE] [smallint] NULL,
      [XFER_DATE] [int] NULL,
      [XFER_DOC] [varchar](12) NULL,
      [TRS] [varchar](14) NULL,
      [SELLER] [varchar](52) NULL,
      [LOT_ACRES] [int] NULL,
      [LOT_SQFT] [bigint] NULL,
      [YR_BLT] [smallint] NULL,
      [YR_EFF] [smallint] NULL,
      [BLDG_SQFT] [int] NULL,
      [UNITS] [smallint] NULL,
      [STORIES] [varchar](4) NULL,
      [BEDS] [smallint] NULL,
      [BATH_F] [smallint] NULL,
      [BATH_H] [smallint] NULL,
      [ROOMS] [smallint] NULL,
      [BLDG_CLS] [char](1) NULL,
      [BLDG_QUALITY] [char](1) NULL,
      [IMPR_COND] [char](1) NULL,
      [FIRE_PL] [varchar](2) NULL,
      [BLDGS] [smallint] NULL,
      [AIR_COND] [char](1) NULL,
      [HEAT] [char](1) NULL,
      [PARK_TYPE] [char](1) NULL,
      [PARK_SPACE] [smallint] NULL,
      [GAR_SQFT] [int] NULL,
      [WATER] [char](1) NULL,
      [SEWER] [char](1) NULL,
      [POOL] [char](1) NULL,
      [FRONT_VIEW] [char](1) NULL,
      [DOCLINKS] [varchar](80) NULL,
      [IMAPLINK] [varchar](16) NULL,
      [MAPLINK] [varchar](20) NULL,
      [S_ADDR_D] [varchar](52) NULL,
      [S_CTY_ST_D] [varchar](38) NULL,
      [M_ADDR_D] [varchar](52) NULL,
      [M_CTY_ST_D] [varchar](31) NULL,
      [NAME_SWAP] [varchar](52) NULL,
      [TD1_VAL] [int] NULL,
      [TD2_VAL] [int] NULL,
      [TD1CODE] [varchar](10) NULL,
      [TD2CODE] [varchar](10) NULL,
      [AR_CODE] [varchar](3) NULL,
      [N_SCR] [char](4) NULL,
      [N_LON] [varchar](10) NULL,
      [N_LAT] [varchar](9) NULL,
      [N_TRACT] [varchar](9) NULL,
      [N_DPBC] [char](2) NULL,
      [Longitude] [float] NULL,
      [Latitude] [float] NULL,
      [SDOCDATE] [int] NULL,
      [GEOMpt] [geography] NULL
   ) ON [PRIMARY]'
   EXEC sp_executesql @sql

   set @sql=N'ALTER TABLE [dbo].[' + @Cnty + '_LDR' + @LdrYear + '] WITH NOCHECK ADD
      CONSTRAINT [PK_' + @Cnty + '_LDR' + @LdrYear + '] PRIMARY KEY  CLUSTERED
      (
         [APN]
      )  ON [PRIMARY]'
   EXEC sp_executesql @sql

END