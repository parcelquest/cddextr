USE Spatial;
GO

IF exists (SELECT 1 FROM sys.tables WHERE name = '$(Cnty)_Roll')
DROP TABLE $(Cnty)_Roll
GO

EXEC spCreateRollTbl $(Cnty);
GO
