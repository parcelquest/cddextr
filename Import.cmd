@echo off
if "%1" == "" goto Usage
if "%2" == "" goto DefaultServer
bcp spatial.dbo.%1_Roll in \\Dev2\CO_IMPORT\Web\%1_Import.csv -c -e \\Dev2\CO_LOGS\CddExtr\%1_Import.log -T -t"|" -r\n -a65535 -k -F2 -q -U pq -P cdd -S %2
goto exitcmd

:DefaultServer
bcp spatial.dbo.%1_Roll in \\Dev2\CO_IMPORT\Web\%1_Import.csv -c -e \\Dev2\CO_LOGS\CddExtr\%1_Import.log -T -t"|" -r\n -a65535 -k -F2 -q
goto exitcmd
:Usage
@echo
@echo Usage: import {county code} [-S User3]
@echo
:exitcmd
