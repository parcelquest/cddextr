USE [Spatial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spDropIndex]
   @CntyTbl [varchar](3)
AS
Declare @sql as nvarchar(100)
BEGIN
   set @sql=N'DROP INDEX [iHO_FL] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iS_STRNUM] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iS_DIR] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iS_SUFFIX] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iS_CITY] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iS_ZIP] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iGROSS_VAL] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iUSE_STD] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iUSE_CO] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iZONING] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iRATIO] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iTRA] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iN_TRACT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE1_DATE] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE2_DATE] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE3_DATE] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE1_AMT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE1_DOC] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE2_DOC] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSALE3_DOC] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iXFER_DOC] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iBEDS] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iYR_BLT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iYR_EFF] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSTORIES] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iFIRE_PL] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iBLDG_QUALITY] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iBATH_F] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iLOT_ACRES] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iLOT_SQFT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iPOOL] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iHEAT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iBLDG_CLS] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iBLDG_SQFT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iAIR_COND] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iM_STRNUM] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iM_CITY] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iM_ZIP] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iM_ST] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iUNITS] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX [iSDOCDATE] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
   set @sql=N'DROP INDEX ['+@CntyTbl+'_spatialPT] ON [dbo].[' + @CntyTbl + '_Roll]'
   EXEC sp_executesql @sql
END