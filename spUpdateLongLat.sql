USE [eRecorder]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spUpdateGeompt]
   @Cnty [varchar](3)
AS
Declare @sql as nvarchar(400)
BEGIN
   SET @sql=N'UPDATE ' + @Cnty + ' SET
      Longitude = CAST(b.Longitude AS float),
      Latitude = CAST(b.Latitude AS float)
      FROM ' + @Cnty + ' r INNER JOIN sqlserver2.spatial.dbo.' + @Cnty + '_Basemap b on r.APN = b.APN'
   EXEC sp_executesql @sql
END

GO


