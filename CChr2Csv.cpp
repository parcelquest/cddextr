/*****************************************************************************
 *
 * 07/15/2014 14.0.0  Modify createCharCsv() to check for input file from county
 *                    section first then [Data] section.
 * 08/27/2014 14.1.0  Fix prepCharRec() to include fireplace code as well as number.
 * 07/01/2015 15.0.1  Modify createCharCsv() to return 0 if input file not available.
 * 05/17/2019 18.3.0  Fix createCharCsv()
 * 01/24/2020 19.2.3  Fix ug in prepCharRec() that set Bsmt_Sqft=Gar_Sqft.
 * 12/05/2022 22.1.0  Fix prepCharRec() & isMatchRoll() to keep variable APN size (RIV).
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Recdef.h"
#include "Tables.h"
#include "CharRec.h"
#include "OutRecs.h"
#include "doSort.h"
#include "Prodlib.h"

extern char  acIniFile[], acDocPath[], acRollFile[], acRawTmpl[], cDelim;
extern int   iApnLen, iRecLen;
extern COUNTY_INFO myCounty;

extern LU_ENTRY    luAirCond[];
extern LU_ENTRY    luPrclStat[];
extern LU_ENTRY    luCondition[];
extern LU_ENTRY    luHeating[];
extern LU_ENTRY    luUseCode[];
extern LU_ENTRY    luQuality[];
extern LU_ENTRY    luView[];
extern LU_ENTRY    luParkType[];
extern LU_ENTRY    luPoolType[];
extern LU_ENTRY    luWater[];
extern LU_ENTRY    luSewer[];
extern LU_ENTRY    luDeedType[];
extern LU_ENTRY    luStrType[];
extern LU_ENTRY    luRoofMat[];
extern LU_ENTRY    luTopo[];

HANDLE   fhRoll;
int      iCharApnLen;

/***************************** createCharOutrec *******************************
 *
 * Return 0 if success.  Otherwise error;
 *
 ******************************************************************************/

int createCharOutrec(LPSTR pOutrec, LPSTR pSql)
{
   char     acTmp[MAX_RECSIZE];
   OUT_REC  *pChar = (OUT_REC *)pOutrec;

   sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|"
                  "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|"
                  "%s|%s|%s|%s|%s|%s|%s|%s\n",
      myCounty.acFipsCode, pChar->Apn_D,
      pChar->UseCode, pChar->YearBuilt, pChar->YearEff,
      pChar->Bedrooms, pChar->FullBaths, pChar->HalfBaths, pChar->Rooms,
      pChar->NumFireplaces, 
      pChar->Pool,
      pChar->LotAcre, pChar->LotSqft, pChar->BldgSqft,
      pChar->Units, pChar->Stories, pChar->Bldgs,
      pChar->GarSqft,
      pChar->ParkType,
      pChar->ParkSpace,
      pChar->View,
      pChar->BldgClass, pChar->BldgQuality,
      //pChar->QualityClass,
      pChar->Cooling,
      pChar->Heating,
      pChar->HasWater, pChar->HasSewer,
      pChar->RoofType, 
      pChar->RoofMat,
      pChar->PatioSqft,
      pChar->DeckSqft,
      pChar->HasElevator,
      pChar->Topo,
      pChar->FlatWork, pChar->GuessHouseSqft, pChar->MiscSqft, pChar->PorchSqft,
      pChar->Sqft_Flr1, pChar->Sqft_Flr2, pChar->Sqft_Above2nd, pChar->BsmtSqft,
      pChar->MiscImpr_Code, 
      pChar->Bath_1Q, pChar->Bath_2Q, pChar->Bath_3Q, pChar->Bath_4Q,
      pChar->BldgSeqNo, pChar->UnitSeqNo);

   blankRem(acTmp);
   strcpy(pSql, acTmp);
   strcat(pSql, "\n");

   return 0;
}

/******************************* createCharHeader *****************************
 *
 *
 ******************************************************************************/

LPSTR createCharHeader(LPSTR sHdr, char cDelimiter)
{
   strcpy(sHdr,"FIPS|APN_D|COUNTYUSE|YRBLT|YREFF|BEDRMS|BATH|HALFBATH|ROOMS|FIREPLACE|POOL|"
               "LOTSIZE|LOTAREA|BLDGAREA|UNITS|STORIES|BLDGS|GARSIZE|PARKTYPE|PARKSPACE|VIEW|"
               "BLDGCLASS|QUALITY|AIRCOND|HEATING|WATER|SEWER|ROOF_TYPE|ROOF_MAT|PATIO_SF|"
               "DECK_SF|ELEVATOR|TOPO|FLATWORK_SF|GUESTHSE_SF|MISCIMPR_SF|PORCH_SF|FLOOR_1|"
               "FLOOR_2|FLOOR_3|BSMT_SF|MISC_IMPR|BATH_1Q|BATH_2Q|BATH_3Q|BATH_4Q|BLDGSEQNO|UNITSEQNO\n");

   return sHdr;
}

/******************************* createCharHeader *****************************
 *
 *
 ******************************************************************************/

void prepCharRec(LPSTR pInbuf, LPSTR pOutbuf)
{
   // Input record
   STDCHAR  *pInChar = (STDCHAR *)pInbuf;
   // Output record
   OUT_REC  *pOutChar =(OUT_REC *)pOutbuf;
   int      iTmp;
   char     acTmp[256];

   strcpy(pOutChar->Apn, pInChar->Apn);
   strcpy(pOutChar->Apn_D, myTrim(pInChar->Apn_D, IRCSIZ_APN));

#ifdef _DEBUG
   //if (!memcmp(pInChar->Apn_D, "689-040-017", 11))
   //   iTmp = 0;
#endif

   if (pInChar->LandUse[0] > ' ')
      strcpy(pOutChar->UseCode, myTrim(pInChar->LandUse, sizeof(pInChar->LandUse)));

   iTmp = atoin(pInChar->YrBlt, 4);
   if (iTmp > 1700)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->YearBuilt, acTmp);
   }
   iTmp = atoin(pInChar->YrEff, 4);
   if (iTmp > 1700)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->YearEff, acTmp);
   }

   iTmp = atoin(pInChar->Beds, sizeof(pInChar->Beds));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->Bedrooms, acTmp);
   }

   iTmp = atoin(pInChar->FBaths, sizeof(pInChar->FBaths));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->FullBaths, acTmp);
      strcpy(pOutChar->Bath_4Q, acTmp);
   }
   iTmp = atoin(pInChar->HBaths, sizeof(pInChar->HBaths));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->HalfBaths, acTmp);
      strcpy(pOutChar->Bath_2Q, acTmp);
   }
   iTmp = atoin(pInChar->Bath_1Q, sizeof(pInChar->Bath_1Q));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->Bath_1Q, acTmp);
   }
   iTmp = atoin(pInChar->Bath_3Q, sizeof(pInChar->Bath_3Q));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->Bath_3Q, acTmp);
   }

   iTmp = atoin(pInChar->Rooms, sizeof(pInChar->Rooms));
   if (iTmp > 0)
   {
      sprintf(acTmp, "%d", iTmp);
      strcpy(pOutChar->Rooms, acTmp);
   }

   if (pInChar->Fireplace[0] > ' ' || pInChar->Fireplace[1] > ' ')
   {
      memcpy(acTmp, pInChar->Fireplace, sizeof(pInChar->Fireplace));
      myTrim(acTmp, sizeof(pInChar->Fireplace));
      strcpy(pOutChar->NumFireplaces, acTmp);
   }

   memcpy(pOutChar->LotAcre,     pInChar->LotAcre,     sizeof(pInChar->LotAcre));
   memcpy(pOutChar->LotSqft,     pInChar->LotSqft,     sizeof(pInChar->LotSqft));
   memcpy(pOutChar->BldgSqft,    pInChar->BldgSqft,    sizeof(pInChar->BldgSqft));
   memcpy(pOutChar->Units,       pInChar->Units,       sizeof(pInChar->Units));
   memcpy(pOutChar->Stories,     pInChar->Stories,     sizeof(pInChar->Stories));
   memcpy(pOutChar->Bldgs,       pInChar->Bldgs,       sizeof(pInChar->Bldgs));
   memcpy(pOutChar->GarSqft,     pInChar->GarSqft,     sizeof(pInChar->GarSqft));
   memcpy(pOutChar->ParkSpace,   pInChar->ParkSpace,   sizeof(pInChar->ParkSpace));

   myTrim(pOutChar->LotAcre,     sizeof(pInChar->LotAcre));
   myTrim(pOutChar->LotSqft,     sizeof(pInChar->LotSqft));
   myTrim(pOutChar->BldgSqft,    sizeof(pInChar->BldgSqft));
   myTrim(pOutChar->Units,       sizeof(pInChar->Units));
   myTrim(pOutChar->Stories,     sizeof(pInChar->Stories));
   myTrim(pOutChar->Bldgs,       sizeof(pInChar->Bldgs));
   myTrim(pOutChar->GarSqft,     sizeof(pInChar->GarSqft));
   myTrim(pOutChar->ParkSpace,   sizeof(pInChar->ParkSpace));

   //strcpy(pOutChar->ParkType,    myTrim(pInChar->ParkType,    sizeof(pInChar->ParkType)));
   memcpy(acTmp, pInChar->ParkType, sizeof(pInChar->ParkType));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->ParkType, Code2Value(myTrim(acTmp, sizeof(pInChar->ParkType)), (LU_ENTRY *)&luParkType[0]));

   //strcpy(pOutChar->Pool,        myTrim(pInChar->Pool,        sizeof(pInChar->Pool)));
   memcpy(acTmp, pInChar->Pool, sizeof(pInChar->Pool));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->Pool, Code2Value(myTrim(acTmp, sizeof(pInChar->Pool)), (LU_ENTRY *)&luPoolType[0]));

   //strcpy(pOutChar->View,        myTrim(pInChar->View,        sizeof(pInChar->View)));
   memcpy(acTmp, pInChar->View, sizeof(pInChar->View));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->View, Code2Value(myTrim(acTmp, sizeof(pInChar->View)), (LU_ENTRY *)&luView[0]));

   memcpy(pOutChar->QualityClass, pInChar->QualityClass, sizeof(pInChar->QualityClass));
   myTrim(pOutChar->QualityClass, sizeof(pInChar->QualityClass));
   pOutChar->BldgClass[0] =      pInChar->BldgClass;

   //pOutChar->BldgQuality[0] =    pInChar->BldgQual;
   acTmp[0] = pInChar->BldgQual;
   acTmp[1] = 0;
	if (acTmp[0] > ' ')
      strcpy(pOutChar->BldgQuality, Code2Value(acTmp, (LU_ENTRY *)&luQuality[0]));

   //strcpy(pOutChar->Cooling,     myTrim(pInChar->Cooling,     sizeof(pInChar->Cooling)));
   memcpy(acTmp, pInChar->Cooling, sizeof(pInChar->Cooling));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->Cooling, Code2Value(myTrim(acTmp, sizeof(pInChar->Cooling)), (LU_ENTRY *)&luAirCond[0]));

   //strcpy(pOutChar->Heating,     myTrim(pInChar->Heating,     sizeof(pInChar->Heating)));
   memcpy(acTmp, pInChar->Heating, sizeof(pInChar->Heating));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->Heating, Code2Value(myTrim(acTmp, sizeof(pInChar->Heating)), (LU_ENTRY *)&luHeating[0]));
   
   //pOutChar->HasWater[0] =       pInChar->HasWater;
   acTmp[0] = pInChar->HasWater;
   acTmp[1] = 0;
	if (acTmp[0] > ' ')
      strcpy(pOutChar->HasWater, Code2Value(acTmp, (LU_ENTRY *)&luWater[0]));

   //pOutChar->HasSewer[0] =       pInChar->HasSewer;
   acTmp[0] = pInChar->HasSewer;
   acTmp[1] = 0;
	if (acTmp[0] > ' ')
      strcpy(pOutChar->HasSewer, Code2Value(acTmp, (LU_ENTRY *)&luSewer[0]));

   //strcpy(pOutChar->Topo,        myTrim(pInChar->Topo,        sizeof(pInChar->Topo)));
   memcpy(acTmp, pInChar->Topo, sizeof(pInChar->Topo));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->Topo, Code2Value(myTrim(acTmp, sizeof(pInChar->Topo)), (LU_ENTRY *)&luTopo[0]));

   // Roof type data not available
   memcpy(pOutChar->RoofType, pInChar->RoofType, sizeof(pInChar->RoofType));
   myTrim(pOutChar->RoofType, sizeof(pInChar->RoofType));
   //strcpy(acTmp, myTrim(pInChar->RoofType, sizeof(pInChar->RoofType)));
	//if (acTmp[0] > ' ')
   //   strcpy(pOutChar->RoofType, Code2Value(acTmp, (LU_ENTRY *)&luRoofType[0]));

   //strcpy(pOutChar->RoofMat,     myTrim(pInChar->RoofMat,     sizeof(pInChar->RoofMat)));
   memcpy(acTmp, pInChar->RoofMat, sizeof(pInChar->RoofMat));
	if (acTmp[0] > ' ')
      strcpy(pOutChar->RoofMat, Code2Value(myTrim(acTmp, sizeof(pInChar->RoofMat)), (LU_ENTRY *)&luRoofMat[0]));

   memcpy(pOutChar->PatioSqft,   pInChar->PatioSqft,   SIZ_CHAR_SQFT);
   myTrim(pOutChar->PatioSqft,   SIZ_CHAR_SQFT);
   memcpy(pOutChar->DeckSqft,    pInChar->DeckSqft,    SIZ_CHAR_SQFT);
   myTrim(pOutChar->DeckSqft,    SIZ_CHAR_SQFT);
   memcpy(pOutChar->FlatWork,    pInChar->FlatWrkSqft, SIZ_CHAR_SQFT);
   myTrim(pOutChar->FlatWork,    SIZ_CHAR_SQFT);
   memcpy(pOutChar->GuessHouseSqft, pInChar->GuestSqft,SIZ_CHAR_SQFT);
   myTrim(pOutChar->GuessHouseSqft, SIZ_CHAR_SQFT);
   memcpy(pOutChar->MiscSqft,    pInChar->MiscSqft,    SIZ_CHAR_SQFT);
   myTrim(pOutChar->MiscSqft,    SIZ_CHAR_SQFT);
   memcpy(pOutChar->PorchSqft,   pInChar->PorchSqft,   SIZ_CHAR_SQFT);
   myTrim(pOutChar->PorchSqft,   SIZ_CHAR_SQFT);
   memcpy(pOutChar->Sqft_Flr1,   pInChar->Sqft_1stFl,  SIZ_CHAR_SQFT);
   myTrim(pOutChar->Sqft_Flr1,   SIZ_CHAR_SQFT);
   memcpy(pOutChar->Sqft_Flr2,   pInChar->Sqft_2ndFl,  SIZ_CHAR_SQFT);
   myTrim(pOutChar->Sqft_Flr2,   SIZ_CHAR_SQFT);
   memcpy(pOutChar->Sqft_Above2nd, pInChar->Sqft_Above2nd, SIZ_CHAR_SQFT);
   myTrim(pOutChar->Sqft_Above2nd, SIZ_CHAR_SQFT);
   memcpy(pOutChar->BsmtSqft,    pInChar->BsmtSqft,    SIZ_CHAR_SQFT);
   myTrim(pOutChar->BsmtSqft,    SIZ_CHAR_SQFT);

   pOutChar->HasElevator[0] =    pInChar->HasElevator;
   memcpy(pOutChar->MiscImpr_Code, pInChar->MiscImpr_Code, sizeof(pInChar->MiscImpr_Code));
   myTrim(pOutChar->MiscImpr_Code, sizeof(pInChar->MiscImpr_Code));

   memcpy(pOutChar->BldgSeqNo,   pInChar->BldgSeqNo,   sizeof(pInChar->BldgSeqNo));
   myBTrim(pOutChar->BldgSeqNo,   sizeof(pInChar->BldgSeqNo));
   memcpy(pOutChar->UnitSeqNo,    pInChar->UnitSeqNo,    sizeof(pInChar->UnitSeqNo));
   myBTrim(pOutChar->UnitSeqNo,    sizeof(pInChar->UnitSeqNo));

}

/******************************* isMatchRoll **********************************
 *
 * Return: 1 if matched, 0 if not, -1 if EOF
 *
 ******************************************************************************/

int isMatchRoll(LPSTR pInrec)
{
   static char *pRec, sBuf[2048];
	DWORD    nBytesRead;
   BOOL     bRet;
   int      iRet, iTmp;
   STDCHAR  *pInChar = (STDCHAR *)pInrec;

   if (!pRec)
   {
      bRet = ReadFile(fhRoll, sBuf, iRecLen, &nBytesRead, NULL);
      pRec = &sBuf[0];
   }

#ifdef _DEBUG
   //if (!memcmp(pInChar->Apn_D, "689-040-017", 11))
   //   iRet = 0;
#endif

   do 
   {
      iTmp = iTrim(pInChar->Apn, 14);
      iRet = memcmp(sBuf, pInChar->Apn, iTmp);
      if (!iRet)
      {
         iRet = 1;
         memcpy(pInChar->LandUse, &sBuf[OFF_USE_CO], SIZ_USE_CO);
         memcpy(pInChar->Apn_D, &sBuf[OFF_APN_D], SIZ_APN_D);
         break;
      } else if (iRet > 0)
      {
         iRet = 0;
         break;
      }

      bRet = ReadFile(fhRoll, sBuf, iRecLen, &nBytesRead, NULL);
      if (nBytesRead != iRecLen || !bRet)
      {
         iRet = -1;
         break;
      }
   } while (bRet);

   return iRet;
}

/******************************* createCharImport *****************************
 *
 * Extract only char record that matches roll file
 *
 * Return: Number of output records, <0 if error
 *
 ******************************************************************************/

int createCharImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acTmp[MAX_RECSIZE], 
            acOutrec[MAX_RECSIZE], acInrec[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0, lApnMatch=0;
   FILE     *fdIn, *fdOut;

   // Input record
   STDCHAR  *pInChar = (STDCHAR *)&acInrec[0];
   // Output record
   OUT_REC  *pOutChar =(OUT_REC *)&acOutrec[0];

   LogMsg("Create Multi-Imprs file ...");
   strcpy(acInfile, pInfile);
   strcpy(acOutfile, pOutfile);

   // Open roll file
   LogMsg("Open input file %s", acRollFile);
	fhRoll = CreateFile(acRollFile, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN, NULL);
   if (fhRoll == INVALID_HANDLE_VALUE)
   {
      LogMsg("***** Error open roll file %s", acRollFile);
      return -3;
   }

   // Open char file
   LogMsg("Open char file %s", acInfile);
   try
   {
      fdIn = fopen(acInfile, "r");
   } catch (...)
   {
      LogMsg("***** Error opening input char file: %s (%d)\n", acInfile, _errno);
      return -1;
   }

   LogMsg("Open output file %s", acOutfile);
   try
   {
      fdOut = fopen(acOutfile, "w");
   } catch (...)
   {
      LogMsg("***** Error creating output file %s (%d)", acOutfile, _errno);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createCharHeader(acTmp, cDelim), fdOut);

   // Get 1st rec
   pTmp = fgets((char *)&acInrec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
#ifdef _DEBUG
      //if (!memcmp(pInChar->Apn, "002050008", 9))
      //   iRet = 0;
#endif

      // Only output records that matches roll file
      iRet = isMatchRoll(acInrec);
      if (iRet == 1)
      {
         lApnMatch++;
         memset(acOutrec, 0, sizeof(OUT_REC));
         prepCharRec(acInrec, acOutrec);

         iRet = createCharOutrec(acOutrec, acTmp);

         fputs(acTmp, fdOut);
      } else if (iRet == -1)
         break;   // EOF

      if (!(++lCnt % 1000))
         printf("\rChar records: %u", lCnt);

      // Get next record
      pTmp = fgets(acInrec, MAX_RECSIZE, fdIn);
   }

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);
   if (fhRoll)
      CloseHandle(fhRoll);

   LogMsg("Total char records processed: %u", lCnt);
   LogMsg("Total MI char records output: %u", lApnMatch);

   return lCnt;
}

/****************************** createCharCsv *********************************
 *
 * If ApnLen in CHAR file if different from roll file, set it in county section
 * of cddextr.ini. i.e.
 * [CCX]
 * CharApnLen=9.
 *
 ******************************************************************************/

int createCharCsv()
{
   char sCChrFile[_MAX_PATH], sOutFile[_MAX_PATH], sTmp[_MAX_PATH];
   int  iRet;

   iCharApnLen = GetPrivateProfileInt(myCounty.acCntyCode, "CharApnLen", myCounty.iApnLen, acIniFile);
   
	iRet = GetIniString("Bulk", "MITmpl", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
	   sprintf(sOutFile, sTmp, myCounty.acCntyName);
   else
   {
      LogMsg("***** MITmpl has not been defined in [Bulk] section in %s", acIniFile);
      return -1;
   }

   // Prepare input file
   iRet = GetIniString(myCounty.acCntyCode, "CChrFile", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet == 0)
      iRet = GetIniString("Data", "CChrFile", "", sTmp, _MAX_PATH, acIniFile);
   if (iRet > 0)
      sprintf(sCChrFile, sTmp, myCounty.acCntyCode, myCounty.acCntyCode);
   else
   {
      LogMsg("***** CChrFile has not been defined in [Data] section in %s", acIniFile);
      return -1;
   }
   
   if (_access(sCChrFile, 0))
   {
      LogMsg("*** Input file not available: %s.  MI module is not created.", sCChrFile);
      return 0;
   }

   // Check roll file
   strcpy(acRollFile, acRawTmpl);
   replStrAll(acRollFile, "[cntycode]", myCounty.acCntyCode);
   if (_access(acRollFile, 0))
   {
      LogMsg("***** Raw file not found: %s", acRollFile);
      return -1;
   }

   iRet = createCharImport(myCounty.acCntyCode, sCChrFile, sOutFile, true);

   return iRet;
}