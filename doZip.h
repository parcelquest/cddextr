#ifndef _DOZIP_H
#define _DOZIP_H
 
typedef struct _tFileItem
{
   char        szName[_MAX_PATH];
   long        lSize;
   SYSTEMTIME  stLastModified;
} FILE_ITEM;

void setUnzipToFolder(char *pStr);
void setReplaceIfExist(bool bVal);
void setPreservePath(bool bVal);
void setProcessSubfolder(bool bVal);

void doZipInit();
void doZipShutdown();
int  startZip(char *pZipFile, char *pFilesToProcess);
int  startUnzip(char *pZipFile, char *pFilesToProcess=NULL);
int  getZipFileContents(char *pZipFile, FILE_ITEM *pFileList, int iMaxCnt);

#endif
