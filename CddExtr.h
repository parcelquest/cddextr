
#if !defined(AFX_CDDEXTR_H__E9505725_5EA8_40BD_A0EA_A5F7FAC93962__INCLUDED_)
#define AFX_CDDEXTR_H__E9505725_5EA8_40BD_A0EA_A5F7FAC93962__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  EXTR_SALE      1
#define  EXTR_GRGR      2

typedef struct _tLayout
{
   CString  sCntyCode;
   CString  sInrecLayout;
   CString  sLayOutTbl;
   CString  sOwnerTbl;
   CString  sAltApnTbl;
   CString  sValueTbl;
   CString  sExemptTbl;
   CString  sFullExemptTbl;
   CString  sLegalTbl;
   CString  sGeoTbl;
   CString  sOCTbl;
   CString  sXCTbl;
   CString  sMITbl;
   /*
   char  sCntyCode[8];
   char  sInrecLayout[64];
   char  sLayOutTbl[64];
   char  sOwnerTbl[64];
   char  sAltApnTbl[64];
   char  sValueTbl[64];
   char  sExemptTbl[64];
   char  sFullExemptTbl[64];
   char  sLegalTbl[64];
   char  sGeoTbl[64];
   */
} INOUT_REC;

#define  CHAR_XLAT      0
#define  INT_XLAT       1
#define  DBL_XLAT       2
#define  SNG_XLAT       3
#define  GEO_XLAT       4
#define  NO_XLAT        8
#define  CITY_XLAT      10
#define  LEGAL_XLAT     11
#define  APN_D_XLAT     12
#define  CARE_XLAT      13
#define  TOPO_XLAT      14
#define  RFM_XLAT       15
#define  MADDR_XLAT     16
#define  MCTYST_XLAT    17
#define  SADDR_XLAT     18
#define  ZONE_XLAT      19
#define  SFX_XLAT       20
#define  FLOOR_XLAT     21
#define  LEGAL50_XLAT   22
#define  DOCTYPE_XLAT   30
#define  DOCNUM_XLAT    31
#define  DOCN_XLAT      32       // Correct DOCNUM for ALA PQ3 format
#define  DOCLNK_XLAT    33       // Copy DocLinks
#define  USE_XLAT       40
#define  USEC_XLAT      41
#define  YMD_XLAT       50
#define  S_UN_XLAT      60
#define  M_UN_XLAT      61
#define  EXEMPT_XLAT    80
#define  PSTAT_XLAT     81
#define  RATIO_XLAT     82
#define  VIEW_XLAT      83
#define  WATER_XLAT     84
#define  SEWER_XLAT     85
#define  POOL_XLAT      86
#define  QUALITY_XLAT   87
#define  PARKTYPE_XLAT  88
#define  AIR_XLAT       89
#define  HEAT_XLAT      90
#define  CONDITION_XLAT 91
#define  D1K_XLAT       92    // Devide by 1000
#define  QUOTE_REM      100
#define  ALL_REM        101

struct 
{
   char *Name;
   int   Value;
} XlatType[]=
{
   "CHAR_XLAT"      ,CHAR_XLAT,    
   "INT_XLAT"       ,INT_XLAT,     
   "DBL_XLAT"       ,DBL_XLAT,     
   "SNG_XLAT"       ,SNG_XLAT,     
   "LEGAL_XLAT"     ,LEGAL_XLAT,   
   "LEGAL50_XLAT"   ,LEGAL50_XLAT,   
   "CITY_XLAT"      ,CITY_XLAT, 
   "APN_D_XLAT"     ,APN_D_XLAT,
   "SFX_XLAT"       ,SFX_XLAT,     
   "FLOOR_XLAT"     ,FLOOR_XLAT,
   "DOCTYPE_XLAT"   ,DOCTYPE_XLAT, 
   "DOCNUM_XLAT"    ,DOCNUM_XLAT, 
   "DOCN_XLAT"      ,DOCN_XLAT, 
   "DOCLNK_XLAT"    ,DOCLNK_XLAT, 
   "USE_XLAT"       ,USE_XLAT,     
   "USEC_XLAT"      ,USEC_XLAT,     
   "YMD_XLAT"       ,YMD_XLAT,     
   "EXEMPT_XLAT"    ,EXEMPT_XLAT,  
   "PSTAT_XLAT"     ,PSTAT_XLAT,   
   "RATIO_XLAT"     ,RATIO_XLAT,   
   "VIEW_XLAT"      ,VIEW_XLAT,    
   "WATER_XLAT"     ,WATER_XLAT,   
   "SEWER_XLAT"     ,SEWER_XLAT,   
   "POOL_XLAT"      ,POOL_XLAT,    
   "QUALITY_XLAT"   ,QUALITY_XLAT, 
   "PARKTYPE_XLAT"  ,PARKTYPE_XLAT,
   "AIR_XLAT"       ,AIR_XLAT,     
   "HEAT_XLAT"      ,HEAT_XLAT,    
   "QUOTE_REM"      ,QUOTE_REM,
   "ALL_REM"        ,ALL_REM,
   "NO_XLAT"        ,NO_XLAT,
   "GEO_XLAT"       ,GEO_XLAT,
   "CARE_XLAT"      ,CARE_XLAT,
   "TOPO_XLAT"      ,TOPO_XLAT,
   "RFM_XLAT"       ,RFM_XLAT,
   "MADDR_XLAT"     ,MADDR_XLAT,
   "MCTYST_XLAT"    ,MCTYST_XLAT,
   "SADDR_XLAT"     ,SADDR_XLAT,
   "CONDITION_XLAT" ,CONDITION_XLAT, 
   "S_UN_XLAT"      ,S_UN_XLAT,
   "M_UN_XLAT"      ,M_UN_XLAT,
   "ZONE_XLAT"      ,ZONE_XLAT,
   "D1K_XLAT"       ,D1K_XLAT,
   "",-1
};

#define  _MAX_EXTRFLD   512

#define	FLD_INDX			0
#define	FLD_NAME_DESC	1
#define	FLD_NAME			2
#define	FLD_MAXLEN		3
#define  MAX_LO_FLDS	   8
#define	MAX_FLDS			512

typedef struct _tXrefTbl2
{
   int   iIdx;
   char  acNameDesc[64];
   char  acName[64];
   int	iMaxLen;
   int   iOffset;
} XREFTBL2;

typedef struct _tExtrFld
{
   char  fldName[32];
   int   iOffset;
   int   iLen;
   int   iIdx;
   int   iDataFmt;   // 0=char, 1=integer, 2=City translation, 3=Sfx translation
} EXTRFLD;

EXTRFLD extrFlds[MAX_FLDS];

#endif // !defined(AFX_CDDEXTR_H__E9505725_5EA8_40BD_A0EA_A5F7FAC93962__INCLUDED_)
