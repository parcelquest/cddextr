#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "Prodlib.h"

extern COUNTY_INFO myCounty;

/******************************** loadCountyInfo ******************************
 *
 * Initialize global variables
 *
 *****************************************************************************/

int loadCountyInfoCsv(char *pCntyCode, char *pCntyTbl)
{
   char     acTmp[_MAX_PATH], *pTmp, *pFlds[MAX_CNTY_FLDS];
   int      iRet=0, iTmp;
   FILE     *fd;

   fd = fopen(pCntyTbl, "r");
   if (fd)
   {
      // Skip blank line
      pTmp = fgets(acTmp, _MAX_PATH, fd);

      while (!feof(fd))
      {
         pTmp = fgets(acTmp, _MAX_PATH, fd);
         if (pTmp)
         {
            if (!memcmp(acTmp, pCntyCode, 3))
            {
               iTmp = ParseString(myTrim(acTmp), ',', MAX_CNTY_FLDS, pFlds);
               if (iTmp > 0)
               {
                  strcpy(myCounty.acCntyCode, pCntyCode);
                  strcpy(myCounty.acStdApnFmt, pFlds[FLD_APN_FMT]);
                  strcpy(myCounty.acSpcApnFmt[0], pFlds[FLD_SPC_FMT]);
                  strcpy(myCounty.acCase[0], pFlds[FLD_SPC_BOOK]);
                  strcpy(myCounty.acCntyID, pFlds[FLD_CNTY_ID]);
                  strcpy(myCounty.acCntyName, pFlds[FLD_CNTY_NAME]);
                  strcpy(myCounty.acYearAssd, pFlds[FLD_YR_ASSD]);

                  iRet = atoi(myCounty.acCntyID);
                  myCounty.iCntyID = iRet;
                  sprintf(myCounty.acFipsCode, "06%.3d", iRet*2-1);
                  myCounty.iApnLen = atoi(pFlds[FLD_APN_LEN]);
                  myCounty.iBookLen = atoi(pFlds[FLD_BOOK_LEN]);
                  myCounty.iPageLen = atoi(pFlds[FLD_PAGE_LEN]);
                  myCounty.iCmpLen = atoi(pFlds[FLD_CMP_LEN]);
                  iRet = 1;
               } else
               {
                  LogMsg("***** Bad county table file: %s", pCntyTbl);
               }

               break;
            }
         } else
            break;
      }

      fclose(fd);

      if (1 != iRet)
         LogMsg("***** County not found.  Please verify %s", pCntyTbl);
   } else
      LogMsg("***** Error opening county table %s", pCntyTbl);

   return iRet;
}

