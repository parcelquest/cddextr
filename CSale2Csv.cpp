/*****************************************************************************
 *
 * 05/17/2019 18.3.0  Modify createSaleHeader() to generate header for bulk sale.
 *                    Add createBulkSaleCsv() & createBulkSale() to create bulk sale file for customer.
 * 06/18/2019 18.4.0  Add log msg to createBulkSaleCsv()
 *
 *****************************************************************************/

#include "stdafx.h"
#include "Logs.h"
#include "Utils.h"
#include "CountyInfo.h"
#include "R01.h"
#include "Recdef.h"
#include "Tables.h"
#include "SaleRec.h"
#include "OutRecs.h"
#include "doSort.h"
#include "Prodlib.h"

extern   char  acIniFile[], acDocPath[];
extern   int   iApnLen;

/******************************* Tuo_MakeDocLink *******************************
 *
 * Format DocLink
 *
 ******************************************************************************/

void Tuo_MakeDocLink(LPSTR pDocLink, LPCSTR pDoc, LPCSTR pDate, LPCSTR pDocPath)
{
   int   iTmp, iDocNum;
   char  acTmp[256], acTmp1[256];

   *pDocLink = 0;
   if (*pDoc > ' ' && *pDate > ' ')
   {
      iTmp = atoin((char *)pDate, 4);
      iDocNum = atoin((char *)pDoc, 8);

      if (iTmp >= 2002 && iDocNum > 0)
         sprintf(acTmp1, "%.3s\\%.4s%06d", pDoc, pDate, iDocNum);
      else
         sprintf(acTmp1, "%.4s\\%.4s%.8s", pDoc, pDate, pDoc);

		sprintf(acTmp, "%s\\%.4s\\%s.pdf", pDocPath, pDate, acTmp1);

      if (!_access(acTmp, 0))
         strcpy(pDocLink, acTmp1);
   }
}

/******************************************************************************
 *
 * Return 0 if success.  Otherwise error;
 *
 ******************************************************************************/

int createSaleOutrec(SALES_REC *pSales, LPSTR pSql, LPSTR pDocLink)
{
   char     acTmp[MAX_RECSIZE];

   quoteRem(pSales->Seller1);
   quoteRem(pSales->Buyer1);
   quoteRem(pSales->Seller2);
   quoteRem(pSales->Buyer2);
   quoteRem(pSales->M_Addr1);
   quoteRem(pSales->M_Addr2);
   sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
               pSales->Apn,
               pSales->DocNum,
               pSales->DocDate,
               pSales->DocType,
               pSales->DocCode,
               pSales->SaleCode,
               pSales->Seller1,
               pSales->Buyer1,
               pSales->Seller2,
               pSales->Buyer2,
               pSales->Price,
               pSales->TaxAmt,
               pSales->GrpSale,
               pSales->GrpAsmt,
               pSales->XferType,
               pSales->NumXfer,
               pSales->M_Addr1,
               pSales->M_Addr2,
               pSales->M_Zip,
               pSales->ARCode, pDocLink);

   blankRem(acTmp);
   strcpy(pSql, acTmp);
   strcat(pSql, "\n");

   return 0;
}

int createSaleOutrec(SALES_REC *pSales, LPSTR pSql)
{
   char     acTmp[MAX_RECSIZE];

   quoteRem(pSales->Seller1);
   quoteRem(pSales->Buyer1);
   quoteRem(pSales->Seller2);
   quoteRem(pSales->Buyer2);
   quoteRem(pSales->M_Addr1);
   quoteRem(pSales->M_Addr2);
   sprintf(acTmp, "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s",
               pSales->Apn,
               pSales->DocNum,
               pSales->DocDate,
               pSales->DocType,
               pSales->DocCode,
               pSales->SaleCode,
               pSales->Seller1,
               pSales->Seller2,
               pSales->Buyer1,
               pSales->Buyer2,
               pSales->Price,
               pSales->TaxAmt,
               pSales->GrpSale,
               pSales->GrpAsmt,
               pSales->XferType,
               pSales->NumXfer,
               pSales->M_Addr1,
               pSales->M_Addr2,
               pSales->M_Zip);

   blankRem(acTmp);
   strcpy(pSql, acTmp);
   strcat(pSql, "\n");

   return 0;
}

LPSTR createSaleHeader(LPSTR sHdr, char cDelimiter, bool bBulkExtr=false)
{
   if (bBulkExtr)
      sprintf(sHdr, "Apn|DocNum|DocDate|DocType|DocTitle|SaleCode|Seller1|Seller2|Buyer1|Buyer2|"
         "Price|TaxAmt|GrpSale|GrpAsmt|XferType|NumXfer|M_Addr1|M_Addr2|M_Zip\n");
   else
      sprintf(sHdr, "Apn|DocNum|DocDate|DocType|DocCode|SaleCode|Seller1|Buyer1|Seller2|Buyer2|"
         "Price|TaxAmt|GrpSale|GrpAsmt|XferType|NumXfer|M_Addr1|M_Addr2|M_Zip|ARCode|DocLink\n");

   return sHdr;
}

/******************************************************************************
 *
 * Type contains bitmap value.  Following value can OR together for multiple types
 *
 * iType 1: generate Sale import (default)
 *       2: generate GrGr import
 *       4: move other APN to group asmt (for KER)
 *
 * Return: Number of output records
 *
 ******************************************************************************/

int createSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], 
            acTmp[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   SCSAL_REC *pSale;
   // Output record
   SALES_REC  mySales;

   if (iType & 1)
   {
      LogMsg("Create Import sale file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else if (iType & 2)
   {
      LogMsg("Create Import GrGr file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   try
   {
      fdIn = fopen(acInfile, "r");
   } catch (...)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s (%d)\n", acInfile, _errno);
      return -1;
   }

   try
   {
      fdOut = fopen(acOutfile, "w");
   } catch (...)
   {
      LogMsg("***** Error creating output file %s (%d)", acOutfile, _errno);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createSaleHeader(acTmp, '|'), fdOut);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);
   pSale = (SCSAL_REC *)&acRec[0];

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      memset(&mySales, 0, sizeof(SALES_REC));
      strncpy(mySales.Apn, pSale->Apn, iApnLen);

#ifdef _DEBUG
      //if (!memcmp(mySales.Apn, "077273017", 9))
      //   iRet = 0;
#endif

      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      myTrim(mySales.DocNum);
      replCharEx(mySales.DocNum, "+*`.{}&%$", ' ', 0, false);
      remChar(mySales.DocNum, ' ');
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
      strncpy(mySales.DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
      myTrim(mySales.DocType);
      strncpy(mySales.DocCode, pSale->DocCode, SALE_SIZ_DOCCODE);
      myTrim(mySales.DocCode);
      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      myTrim(mySales.SaleCode);

      if (pSale->Seller1[0] > ' ')
      {
         strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
         myTrim(mySales.Seller1);
      }
      if (pSale->Seller2[0] > ' ')
      {
         strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
         myTrim(mySales.Seller2);
      }

      if (pSale->Name1[0] > ' ')
      {
         strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer1);
      }
      if (pSale->Name2[0] > ' ')
      {
         strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer2);
      }

      if (pSale->MailAdr1[0] > ' ')
      {
         strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
         myTrim(mySales.M_Addr1);
         strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
         myTrim(mySales.M_Addr2);
      }

      if (iType & 4)
      {
         strncpy(mySales.GrpAsmt, pSale->OtherApn, iApnLen);
         mySales.GrpSale[0] = 'O';
      }

      long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
      if (lTmp > 0)
      {
         sprintf(mySales.NumXfer, "%d", lTmp);
         mySales.GrpSale[0] = 'Y';
      } else
      {
         if (pSale->NumOfPrclXfer[0] > ' ')
            mySales.GrpSale[0] = 'Y';
         else if(pSale->MultiSale_Flg > ' ')
            mySales.GrpSale[0] = 'Y';
      }

      long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      double dAmt = atofn(pSale->StampAmt, SALE_SIZ_STAMPAMT);
      if (dAmt > 0.0)
         sprintf(mySales.TaxAmt, "%.2f", dAmt);

      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      if (pSale->ARCode > ' ')
         mySales.ARCode[0] = pSale->ARCode;
      else if (iType & 2)
         mySales.ARCode[0] = 'R';
      else
         mySales.ARCode[0] = 'A';

      // Assume nonsale is transfer
      mySales.XferType[0] = pSale->XferType;

      acTmp[0] = 0;
      iRet = createSaleOutrec(&mySales, acTmp, "");

      fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   LogMsgD("\nTotal Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

int createSaleImport(void (*fn)(LPSTR, LPCSTR, LPCSTR, LPCSTR), LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], 
            acDocLink[128], acTmp[MAX_RECSIZE];
   BOOL     bEof;
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   SCSAL_REC *pSale;
   // Output record
   SALES_REC  mySales;

   if (iType & 1)
   {
      LogMsg("Create Import sale file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else if (iType & 2)
   {
      LogMsg("Create Import GrGr file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   fdIn = fopen(acInfile, "r");
   if (fdIn == NULL)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s\n", acInfile);
      return -1;
   }

   if (!(fdOut = fopen(acOutfile, "w")))
   {
      LogMsg("***** Error creating output file %s", acOutfile);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createSaleHeader(acTmp, '|'), fdOut);

   // Get 1st rec
   pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   bEof = (pTmp ? false:true);
   pSale = (SCSAL_REC *)&acRec[0];

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      memset(&mySales, 0, sizeof(SALES_REC));
      strncpy(mySales.Apn, pSale->Apn, iApnLen);

#ifdef _DEBUG
      //if (!memcmp(mySales.Apn, "077273017", 9))
      //   iRet = 0;
#endif

      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      myTrim(mySales.DocNum);
      replCharEx(mySales.DocNum, "+*`.{}&%$", ' ', 0, false);
      remChar(mySales.DocNum, ' ');
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
      strncpy(mySales.DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
      myTrim(mySales.DocType);
      strncpy(mySales.DocCode, pSale->DocCode, SALE_SIZ_DOCCODE);
      myTrim(mySales.DocCode);
      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      myTrim(mySales.SaleCode);

      if (pSale->Seller1[0] > ' ')
      {
         strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
         myTrim(mySales.Seller1);
      }
      if (pSale->Seller2[0] > ' ')
      {
         strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
         myTrim(mySales.Seller2);
      }

      if (pSale->Name1[0] > ' ')
      {
         strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer1);
      }
      if (pSale->Name2[0] > ' ')
      {
         strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer2);
      }

      if (pSale->MailAdr1[0] > ' ')
      {
         strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
         myTrim(mySales.M_Addr1);
         strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
         myTrim(mySales.M_Addr2);
      }

      if (iType & 4)
      {
         strncpy(mySales.GrpAsmt, pSale->OtherApn, iApnLen);
         mySales.GrpSale[0] = 'O';
      }

      long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
      if (lTmp > 0)
      {
         sprintf(mySales.NumXfer, "%d", lTmp);
         mySales.GrpSale[0] = 'Y';
      } else
      {
         if (pSale->NumOfPrclXfer[0] > ' ')
            mySales.GrpSale[0] = 'Y';
         else if(pSale->MultiSale_Flg > ' ')
            mySales.GrpSale[0] = 'Y';
      }

      long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      double dAmt = atofn(pSale->StampAmt, SALE_SIZ_STAMPAMT);
      if (dAmt > 0.0)
         sprintf(mySales.TaxAmt, "%.2f", dAmt);

      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      if (iType & 2)
         mySales.ARCode[0] = 'R';
      else
         mySales.ARCode[0] = 'A';

      // Assume nonsale is transfer
      mySales.XferType[0] = pSale->XferType;

      acTmp[0] = 0;
      if (fn)
         (*fn)((LPSTR)&acDocLink[0], (LPCSTR)&mySales.DocNum[0], (LPCSTR)&mySales.DocDate[0], (LPCSTR)&acDocPath[0]); 
      else
         acDocLink[0] = 0;
      iRet = createSaleOutrec(&mySales, acTmp, acDocLink);

      fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   LogMsgD("\nTotal Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

/******************************************************************************
 *
 * iType: 1=Sale, 2=GrGr
 *
 ******************************************************************************/

int createSaleCsv(LPCSTR CountyCode, int iYear, int iType)
{
   char sDbName[32], sCSalFile[_MAX_PATH], sOutFile[_MAX_PATH], sTmp[_MAX_PATH];
   int  iRet;

   sprintf(sDbName, "UPD%d", iYear);

	GetPrivateProfileString("WebImport", "SqlSalesFile", "", sTmp, _MAX_PATH, acIniFile);
	sprintf(sOutFile, sTmp, sDbName, CountyCode);

   // Prepare input file
   if (iType == 2)
	   iRet = GetPrivateProfileString("Data", "CumGrgr", "", sTmp, _MAX_PATH, acIniFile);
   else
	   iRet = GetPrivateProfileString("Data", "CumSale", "", sTmp, _MAX_PATH, acIniFile);
	sprintf(sCSalFile, sTmp, CountyCode, CountyCode);
   
   if (_access(sCSalFile, 0))
   {
      LogMsg("***** Input file not found: %s", sCSalFile);
      return -1;
   }

   if (strstr(CountyCode, "TUO"))
      iRet = createSaleImport(Tuo_MakeDocLink, CountyCode, sCSalFile, sOutFile, iType, false);
   else
      iRet = createSaleImport(CountyCode, sCSalFile, sOutFile, iType, false);

   return iRet;
}

/******************************************************************************
 *
 * iType: 1=Sale, 2=GrGr
 *
 ******************************************************************************/

int createBulkSaleCsv(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType, bool bInclHdr)
{
   char     *pTmp, acInfile[_MAX_PATH], acOutfile[_MAX_PATH], acRec[MAX_RECSIZE], 
            acTmp[MAX_RECSIZE];
   int      iRet, lCnt=0;
   FILE     *fdIn, *fdOut;

   // Input record
   SCSAL_EXT *pSale;
   // Output record
   SALES_REC  mySales;

   if (iType & 1)
   {
      LogMsg("Create Bulk sale file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "SaleOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else if (iType & 2)
   {
      LogMsg("Create Bulk GrGr file ...");
      if (pInfile && *pInfile > ' ')
         strcpy(acInfile, pInfile);
      else
      {
         GetPrivateProfileString("Data", "GrGrOut", "", acTmp, _MAX_PATH, acIniFile);
         sprintf(acInfile, acTmp, CountyCode, CountyCode, "sls");
      }
   } else
   {
      LogMsg("??? Incorrect file type: %d", iType);
      return -999;
   }

   strcpy(acOutfile, pOutfile);

   // Open Sales file
   LogMsg("Open Sales/GrGr file %s", acInfile);
   try
   {
      fdIn = fopen(acInfile, "r");
   } catch (...)
   {
      LogMsg("***** Error opening Sales/GrGr file: %s (%d)\n", acInfile, _errno);
      return -1;
   }

   LogMsg("Create output file %s", acOutfile);
   try
   {
      fdOut = fopen(acOutfile, "w");
   } catch (...)
   {
      LogMsg("***** Error creating output file %s (%d)", acOutfile, _errno);
      return -2;
   }

   // Write header
   if (bInclHdr)
      fputs(createSaleHeader(acTmp, '|', true), fdOut);

   // Get 1st rec
   pSale = (SCSAL_EXT *)&acRec[0];
   do
   {
      pTmp = fgets((char *)&acRec[0], MAX_RECSIZE, fdIn);
   } while (pTmp && memcmp(pSale->Apn, "0001", 4) < 0);

   // Merge loop
   while (pTmp && !feof(fdIn))
   {
      memset(&mySales, 0, sizeof(SALES_REC));
      strncpy(mySales.Apn, pSale->Apn, iApnLen);

#ifdef _DEBUG
      //if (!memcmp(mySales.Apn, "077273017", 9))
      //   iRet = 0;
#endif

      strncpy(mySales.DocNum, pSale->DocNum, SALE_SIZ_DOCNUM);
      myTrim(mySales.DocNum);
      strncpy(mySales.DocDate, pSale->DocDate, SALE_SIZ_DOCDATE);
      strncpy(mySales.DocType, pSale->DocType, SALE_SIZ_DOCTYPE);
      myTrim(mySales.DocType);
      if (iType == 2)
         strncpy(mySales.DocCode, pSale->DocTitle, SALE_SIZ_DOCTITLE);
      else
         strncpy(mySales.DocCode, pSale->DocCode, SALE_SIZ_DOCCODE);
      myTrim(mySales.DocCode);
      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);
      myTrim(mySales.SaleCode);

      if (pSale->Seller1[0] > ' ')
      {
         strncpy(mySales.Seller1, pSale->Seller1, SALE_SIZ_SELLER);
         myTrim(mySales.Seller1);
      }
      if (pSale->Seller2[0] > ' ')
      {
         strncpy(mySales.Seller2, pSale->Seller2, SALE_SIZ_SELLER);
         myTrim(mySales.Seller2);
      }

      if (pSale->Name1[0] > ' ')
      {
         strncpy(mySales.Buyer1, pSale->Name1, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer1);
      }
      if (pSale->Name2[0] > ' ')
      {
         strncpy(mySales.Buyer2, pSale->Name2, SALE_SIZ_BUYER);
         myTrim(mySales.Buyer2);
      }

      if (pSale->MailAdr1[0] > ' ')
      {
         strncpy(mySales.M_Addr1, pSale->MailAdr1, SALE_SIZ_M_ADR1);
         myTrim(mySales.M_Addr1);
         strncpy(mySales.M_Addr2, pSale->MailAdr2, SALE_SIZ_M_ADR2);
         myTrim(mySales.M_Addr2);
      }

      if (iType & 4)
      {
         strncpy(mySales.GrpAsmt, pSale->OtherApn, iApnLen);
         mySales.GrpSale[0] = 'O';
      }

      long lTmp = atoin(pSale->NumOfPrclXfer, SALE_SIZ_NOPRCLXFR);
      if (lTmp > 0)
      {
         sprintf(mySales.NumXfer, "%d", lTmp);
         mySales.GrpSale[0] = 'Y';
      } else
      {
         if (pSale->NumOfPrclXfer[0] > ' ')
            mySales.GrpSale[0] = 'Y';
         else if(pSale->MultiSale_Flg > ' ')
            mySales.GrpSale[0] = 'Y';
      }

      long lPrice = atoin(pSale->SalePrice, SALE_SIZ_SALEPRICE);
      if (lPrice > 0)
         sprintf(mySales.Price, "%d", lPrice);

      double dAmt = atofn(pSale->StampAmt, SALE_SIZ_STAMPAMT);
      if (dAmt > 0.0)
         sprintf(mySales.TaxAmt, "%.2f", dAmt);

      strncpy(mySales.SaleCode, pSale->SaleCode, SALE_SIZ_SALECODE);

      // Assume nonsale is transfer
      mySales.XferType[0] = pSale->XferType;

      acTmp[0] = 0;
      iRet = createSaleOutrec(&mySales, acTmp);

      fputs(acTmp, fdOut);

      if (!(++lCnt % 1000))
         printf("\rSales records: %u", lCnt);

      // Get next record
      pTmp = fgets(acRec, MAX_RECSIZE, fdIn);
   }

   LogMsgD("\nTotal Sales records : %u", lCnt);

   if (fdIn)
      fclose(fdIn);
   if (fdOut)
      fclose(fdOut);

   return lCnt;
}

/******************************************************************************
 *
 * Create bulk sale file for customer.  This extracts all sale data from SALE or GRGR data
 *
 ******************************************************************************/

int createBulkSale(LPCSTR CountyCode, LPCSTR pOutfile, int iYear, int iType)
{
   char sCSalFile[_MAX_PATH], sTmp[_MAX_PATH], sDataType[32];
   int  iRet;

   // Prepare input file
   if (iType == 2)
      strcpy(sDataType, "CumGrgr");
   else
      strcpy(sDataType, "CumSale");

   // If no county specific input template defined, use default
	iRet = GetIniString(CountyCode, sDataType, "", sTmp, _MAX_PATH, acIniFile);
   if (!iRet)
      iRet = GetIniString("Data", sDataType, "", sTmp, _MAX_PATH, acIniFile);
	sprintf(sCSalFile, sTmp, CountyCode, CountyCode);
   
   if (_access(sCSalFile, 0))
   {
      LogMsg("***** Input file not found: %s", sCSalFile);
      return -1;
   }

   // Create bulk file
   iRet = createBulkSaleCsv(CountyCode, sCSalFile, pOutfile, iType, true);

   return iRet;
}