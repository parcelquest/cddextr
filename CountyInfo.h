// County Info
#ifndef  MAX_CNTY_FLDS

#define  MAX_SPC_FMTS   2
#define  MAX_CNTY_FLDS  14

//CountyCode,CountyID,ApnLen,BookLen,PageLen,CmpLen,CountyName,ApnFormat,OtherFormat,Book,YrAssd

#define  FLD_CNTY_CODE  0
#define  FLD_CNTY_ID    1
#define  FLD_APN_LEN    2
#define  FLD_BOOK_LEN   3
#define  FLD_PAGE_LEN   4
#define  FLD_CMP_LEN    5
#define  FLD_CNTY_NAME  6
#define  FLD_APN_FMT    7
#define  FLD_SPC_FMT    8
#define  FLD_SPC_BOOK   9
#define  FLD_YR_ASSD    10

#define  R01_SIZ_MAPLINK   20
#define  R01_SIZ_CITY      24

typedef struct T_COUNTY_INFO
{
   char  acCntyCode[6];
   char  acFipsCode[6];
   char  acCntyID[4];
   char  acCntyName[20];
   char  acStdApnFmt[20];                 // Standard format
   char  acSpcApnFmt[MAX_SPC_FMTS][20];   // Special formats
   char  acCase[MAX_SPC_FMTS][32];        // Special cases
   char  acYearAssd[8];
   char  acFirstAPN[16];
   char  acLastAPN[16];
   int   iYearAssd;
   int   iApnLen;
   int   iBookLen;
   int   iPageLen;
   int   iCmpLen;
   int   iCntyID;
   int   iLastRecCnt;
   int   iRollDelay;
   int   iGrGrDelay;
   char  RollFreq;
   char  GrGrFreq;
   char  isReady;                          // Y/N
} COUNTY_INFO;

int loadCountyInfoCsv(char *pCntyCode, char *pCntyTbl);

#endif