#if !defined(AFX_OUTRECS_H_INCLUDED_)
#define AFX_OUTRECS_H_INCLUDED_

#define IRSIZ_APN							20	
#define IRSIZ_ORGAPN						20
#define IRSIZ_XREFAPN					20
#define IRSIZ_ALTAPN						20
#define IRSIZ_STATUS						2
#define IRSIZ_TAXYEAR					6
#define IRSIZ_ZONING						16
#define IRSIZ_ZONING2					16
#define IRSIZ_USECODE					16
#define IRSIZ_USECODE2					16
#define IRSIZ_NBHCODE					16
#define IRSIZ_TRANSFERDOC				16
#define IRSIZ_TRANSFERDATE				10
#define IRSIZ_NAME1						128
#define IRSIZ_NAME2						128
#define IRSIZ_SWAPNAME					128
#define IRSIZ_VESTING					128
#define IRSIZ_LEGAL						3072
#define IRSIZ_TAXABILITY				10
#define IRSIZ_DELQNUM					10
#define IRSIZ_DELQDATE					10
#define IRSIZ_IS_ETAL					2
#define IRSIZ_IS_AG						2
#define IRSIZ_IS_TIMBER					2
#define IRSIZ_TRACT						66
#define IRSIZ_BLOCK						66
#define IRSIZ_LOT							66
#define IRSIZ_CNAME1						256
#define IRSIZ_CNAME2						256
#define IRSIZ_BASECODE					6
#define IRSIZ_MH_LIC						12
#define IRSIZ_SPLITPARCEL				20
#define IRSIZ_REDEEMEDDATE          12
#define IRSIZ_SECDELINQFISCALYEAR   12
#define IRSIZ_EVENTTYPE					2
#define IRSIZ_FULLEXEMPT				2
#define IRSIZ_PROP8FLG					2
#define IRSIZ_MCAREOF					128
#define IRSIZ_MDBA						128
#define IRSIZ_MADDR1						512
#define IRSIZ_MADDR2						512
#define IRSIZ_MADDR3						512
#define IRSIZ_MADDR4						512
#define IRSIZ_MSTRNUM					66
#define IRSIZ_MSTRSUB					66
#define IRSIZ_MSTRDIR					66
#define IRSIZ_MSTRNAME					66
#define IRSIZ_MSTRTYPE					66
#define IRSIZ_MUNITNO					66
#define IRSIZ_MCITY						66
#define IRSIZ_MST							32
#define IRSIZ_MZIP						32
#define IRSIZ_MZIP4						6
#define IRSIZ_ACITYABBR					10
#define IRSIZ_ASITUS1					128
#define IRSIZ_ASITUS2					128
#define IRSIZ_ASTRNUM					66
#define IRSIZ_ASTRSUB					66
#define IRSIZ_ASTRDIR					66
#define IRSIZ_ASTRNAME					66
#define IRSIZ_ASTRTYPE					66
#define IRSIZ_AUNITNO					66
#define IRSIZ_ACITY						66
#define IRSIZ_AST							32
#define IRSIZ_AZIP						32
#define IRSIZ_AZIP4						6
#define IRSIZ_LOTSIZE					12
#define IRSIZ_SIZETYPE					2
  
typedef struct _tRollRec
{
   char  Apn								[IRSIZ_APN];				
   char  OrgApn							[IRSIZ_ORGAPN];				
   char  XrefApn							[IRSIZ_XREFAPN];				
   char  AltApn							[IRSIZ_ALTAPN];				
   char  Status							[IRSIZ_STATUS];				
   char  TaxYear							[IRSIZ_TAXYEAR];				
   char  Zoning							[IRSIZ_ZONING];				
   char  Zoning2							[IRSIZ_ZONING2];				
   char  UseCode							[IRSIZ_USECODE];				
   char  UseCode2							[IRSIZ_USECODE2];				
   char  NbhCode							[IRSIZ_NBHCODE];				
   char  TransferDoc						[IRSIZ_TRANSFERDOC];			
   char  TransferDate					[IRSIZ_TRANSFERDATE];		
   char  Name1								[IRSIZ_NAME1];				
   char  Name2								[IRSIZ_NAME2];				
   char  SwapName							[IRSIZ_SWAPNAME];				
   char  Vesting							[IRSIZ_VESTING];				
   char  Legal								[IRSIZ_LEGAL];				
   char  Taxability						[IRSIZ_TAXABILITY];			
   char  DelqNum							[IRSIZ_DELQNUM];				
   char  DelqDate							[IRSIZ_DELQDATE];				
   char  IsEtal							[IRSIZ_IS_ETAL];				
   char  IsAg								[IRSIZ_IS_AG];				
   char  IsTimber							[IRSIZ_IS_TIMBER];			
   char  Tract								[IRSIZ_TRACT];				
   char  Block								[IRSIZ_BLOCK];				
   char  Lot								[IRSIZ_LOT];				
   char  CName1							[IRSIZ_CNAME1];				
   char  CName2							[IRSIZ_CNAME2];				
   char  BaseCode							[IRSIZ_BASECODE];				
   char  Mh_Lic							[IRSIZ_MH_LIC];				
   char  SplitParcel						[IRSIZ_SPLITPARCEL];			
   char  RedeemedDate 					[IRSIZ_REDEEMEDDATE];                    
   char  SecDelinqFisc					[IRSIZ_SECDELINQFISCALYEAR];     
   char  EventType						[IRSIZ_EVENTTYPE];			
   char  FullExempt						[IRSIZ_FULLEXEMPT];			
   char  Prop8Flg							[IRSIZ_PROP8FLG];				
   char  MCareOf							[IRSIZ_MCAREOF];				
   char  MDba								[IRSIZ_MDBA];				
   char  MAddr1							[IRSIZ_MADDR1];				
   char  MAddr2							[IRSIZ_MADDR2];				
   char  MAddr3							[IRSIZ_MADDR3];				
   char  MAddr4							[IRSIZ_MADDR4];				
   char  MStrNum							[IRSIZ_MSTRNUM];				
   char  MStrSub							[IRSIZ_MSTRSUB];				
   char  MStrDir							[IRSIZ_MSTRDIR];				
   char  MStrName							[IRSIZ_MSTRNAME];			
   char  MStrSfx							[IRSIZ_MSTRTYPE];			
   char  MUnitNo							[IRSIZ_MUNITNO];				
   char  MCity								[IRSIZ_MCITY];				
   char  MState							[IRSIZ_MST];			
   char  MZip								[IRSIZ_MZIP];				
   char  MZip4								[IRSIZ_MZIP4];				
   char  ACityAbbr						[IRSIZ_ACITYABBR];			
   char  ASitus1							[IRSIZ_ASITUS1];				
   char  ASitus2							[IRSIZ_ASITUS2];				
   char  AStrNum							[IRSIZ_ASTRNUM];				
   char  AStrSub							[IRSIZ_ASTRSUB];				
   char  AStrDir							[IRSIZ_ASTRDIR];				
   char  AStrName							[IRSIZ_ASTRNAME];			
   char  AStrSfx							[IRSIZ_ASTRTYPE];			
   char  AUnitNo							[IRSIZ_AUNITNO];				
   char  ACity								[IRSIZ_ACITY];				
   char  AState							[IRSIZ_AST];			
   char  AZip								[IRSIZ_AZIP];				
   char  AZip4								[IRSIZ_AZIP4];				
   char  LotSize							[IRSIZ_LOTSIZE];		 
   char  SizeType							[IRSIZ_SIZETYPE];		 
} ROLL_REC;

#define IRVSIZ_APN 						20 
#define IRVSIZ_TYPE 						2 
#define IRVSIZ_TRA 						20 
#define IRVSIZ_LAND 						12 
#define IRVSIZ_IMPR 						12 
#define IRVSIZ_HOMESITE 				12     
#define IRVSIZ_GROWING 					12     
#define IRVSIZ_FIXTR 					12     
#define IRVSIZ_PERSPROP 				12     
#define IRVSIZ_BUSINV 					12     
#define IRVSIZ_PP_MH 					12     
#define IRVSIZ_CLCA_LAND 				12 
#define IRVSIZ_CLCA_IMPR 				12 
#define IRVSIZ_TREE_VINE 				12 
#define IRVSIZ_MINERAL 					12     
#define IRVSIZ_TIMBER 					12     
#define IRVSIZ_OTHER 					12     
#define IRVSIZ_OTHERTOTAL 				12 
#define IRVSIZ_GROSS 					12     
#define IRVSIZ_RATIO 					12     
#define IRVSIZ_HOFLG 					2     
#define IRVSIZ_EXETOTAL 				12     
#define IRVSIZ_EXECODE  				8     
#define IRVSIZ_EXEAMT 					12     
#define IRVSIZ_EXEYEAR  				8     
#define IRVSIZ_EXECODE1 				8     
#define IRVSIZ_EXEAMT1 					12     
#define IRVSIZ_EXEYEAR1 				8     
#define IRVSIZ_EXECODE2 				8     
#define IRVSIZ_EXEAMT2 					12     
#define IRVSIZ_EXEYEAR2 				8     
#define IRVSIZ_EXECODE3 				8     
#define IRVSIZ_EXEAMT3 					12     
#define IRVSIZ_EXEYEAR3 				8     
#define IRVSIZ_EXECODE4 				8     
#define IRVSIZ_EXEAMT4 					12     
#define IRVSIZ_EXEYEAR4 				8     
#define IRVSIZ_IMPRTYPE 				6     
#define IRVSIZ_LANDBASEYEAR 		   8 
#define IRVSIZ_BASEIMPR 				12     
#define IRVSIZ_IMPRBASEYEAR 		   8 
#define IRVSIZ_BASELAND 				12     
#define IRVSIZ_LEASEVAL 				12     
#define IRVSIZ_PRIORLAND 				12 
#define IRVSIZ_PRIORSTRUCT 		   12 
#define IRVSIZ_PRIORTREES 				12 
#define IRVSIZ_PRIORTRADE 				12 
#define IRVSIZ_PRIORPERS 	       	12 
#define IRVSIZ_PRIORHOEXE 		      12 
#define IRVSIZ_PRIOROTHEREXE 			12   	     
#define IRVSIZ_PRIORIMPR 				12 
#define IRVSIZ_PRIORBUSINV 	      12
#define IRVSIZ_BASEMINERAL 	      12
#define IRVSIZ_BASEFIXTR 				12 
#define IRVSIZ_BASEPERS 				12   	     
#define IRVSIZ_FIXTR_RP 				12   	     
#define IRVSIZ_TAXCODE 				   8   	     

typedef struct _tValuRec
{
   char  Apn								[IRVSIZ_APN]; 		  
   char  Type								[IRVSIZ_TYPE]; 		  
   char  Tra								[IRVSIZ_TRA]; 		  
   char  Land								[IRVSIZ_LAND]; 		  
   char  Impr								[IRVSIZ_IMPR]; 		  
   char  HomeSite							[IRVSIZ_HOMESITE]; 		   
   char  Growing							[IRVSIZ_GROWING]; 		   
   char  Fixtr								[IRVSIZ_FIXTR]; 	  
   char  PersProp							[IRVSIZ_PERSPROP]; 		   
   char  BusInv							[IRVSIZ_BUSINV]; 	  
   char  PP_MH								[IRVSIZ_PP_MH]; 	  
   char  CLCA_Land						[IRVSIZ_CLCA_LAND]; 	  
   char  CLCA_Impr						[IRVSIZ_CLCA_IMPR]; 	  
   char  TreeVines						[IRVSIZ_TREE_VINE]; 	  
   char  Mineral							[IRVSIZ_MINERAL]; 		   
   char  Timber							[IRVSIZ_TIMBER]; 	  
   char  Fixtr_RP							[IRVSIZ_FIXTR_RP]; 	  
   char  Other								[IRVSIZ_OTHER]; 	  
   char  OtherTotal						[IRVSIZ_OTHERTOTAL]; 	  
   char  Gross								[IRVSIZ_GROSS]; 	  
   char  Ratio								[IRVSIZ_RATIO]; 	  
   char  HO_Flg							[IRVSIZ_HOFLG]; 	  
   char  ExeTotal							[IRVSIZ_EXETOTAL]; 		   
   char  ExeCode1							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt1							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear1							[IRVSIZ_EXEYEAR]; 		   
   char  ExeCode2							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt2							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear2							[IRVSIZ_EXEYEAR]; 		   
   char  ExeCode3							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt3							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear3							[IRVSIZ_EXEYEAR]; 		   
   char  ExeCode4							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt4							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear4							[IRVSIZ_EXEYEAR]; 		   
   char  ExeCode5							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt5							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear5							[IRVSIZ_EXEYEAR]; 		   
   char  ExeCode6							[IRVSIZ_EXECODE]; 		   
   char  ExeAmt6							[IRVSIZ_EXEAMT]; 		   
   char  ExeYear6							[IRVSIZ_EXEYEAR]; 		   
   char  ImprType							[IRVSIZ_IMPRTYPE]; 		   
   char  LandBaseYear					[IRVSIZ_LANDBASEYEAR]; 	  
   char  BaseImpr							[IRVSIZ_BASEIMPR]; 		   
   char  ImprBaseYear					[IRVSIZ_IMPRBASEYEAR]; 	  
   char  BaseLand							[IRVSIZ_BASELAND]; 		   
   char  LeaseVal							[IRVSIZ_LEASEVAL]; 		   
   char  PriorLand						[IRVSIZ_PRIORLAND]; 	  
   char  PriorStruct						[IRVSIZ_PRIORSTRUCT]; 	  
   char  PriorTrees						[IRVSIZ_PRIORTREES]; 	  
   char  PriorTrade						[IRVSIZ_PRIORTRADE]; 	  
   char  PriorPers	       			[IRVSIZ_PRIORPERS]; 	  
   char  PriorHOExe						[IRVSIZ_PRIORHOEXE]; 	  
   char  PriorOtherExe	       		[IRVSIZ_PRIOROTHEREXE];   
   char  PriorImpr						[IRVSIZ_PRIORIMPR]; 	  
   char  PriorBusInv						[IRVSIZ_PRIORBUSINV]; 	  
   char  BaseMineral	       			[IRVSIZ_BASEMINERAL]; 	  
   char  BaseFixtr						[IRVSIZ_BASEFIXTR]; 	  
   char  BasePers							[IRVSIZ_BASEPERS]; 	      	 
   char  TaxCode                    [IRVSIZ_TAXCODE];
} VALU_REC;


#define IRSSIZ_APN							20
#define IRSSIZ_DOCNUM						20
#define IRSSIZ_DOCDATE						10
#define IRSSIZ_DOCCODE						32
#define IRSSIZ_SALECODE						4
#define IRSSIZ_SELLER						256   
#define IRSSIZ_BUYER							256
#define IRSSIZ_PRICE							10	
#define IRSSIZ_TAXAMT						10
#define IRSSIZ_GRPSALE						20
#define IRSSIZ_GRPASMT						20
#define IRSSIZ_XFERTYPE						10
#define IRSSIZ_NUMXFER						4
#define IRSSIZ_M_ADDR1						54
#define IRSSIZ_M_ADDR2						44
#define IRSSIZ_M_ZIP							14
#define IRSSIZ_ARCODE						2
	
typedef struct _tSalesRec
{
	char Apn[IRSSIZ_APN];
	char DocNum[IRSSIZ_DOCNUM];
	char DocDate[IRSSIZ_DOCDATE];
	char DocType[IRSSIZ_DOCCODE];
	char DocCode[IRSSIZ_DOCCODE];
	char SaleCode[IRSSIZ_SALECODE];
	char Seller1[IRSSIZ_SELLER];
	char Seller2[IRSSIZ_SELLER];
	char Buyer1[IRSSIZ_BUYER];
	char Buyer2[IRSSIZ_BUYER];
	char Price[IRSSIZ_PRICE];
	char TaxAmt[IRSSIZ_TAXAMT];
	char GrpSale[IRSSIZ_GRPSALE];
	char GrpAsmt[IRSSIZ_GRPASMT];
	char XferType[IRSSIZ_XFERTYPE];
	char NumXfer[IRSSIZ_NUMXFER];
	char M_Addr1[IRSSIZ_M_ADDR1];
	char M_Addr2[IRSSIZ_M_ADDR2];
	char M_Zip[IRSSIZ_M_ZIP];
	char ARCode[IRSSIZ_ARCODE];
} SALES_REC;


#define  IRCSIZ_APN 							20
#define  IRCSIZ_SEQ 							4
#define  IRCSIZ_FLAG   					   2
#define  IRCSIZ_HASSEPTIC					2
#define  IRCSIZ_HASSEWER					2	
#define  IRCSIZ_HASWELL						2
#define  IRCSIZ_HASGAS						2
#define  IRCSIZ_IMPRCOND					2
#define  IRCSIZ_CONSTRTYPE 				2
#define  IRCSIZ_NATURALGAS 				2
#define  IRCSIZ_TYPEACCESS 				2
#define  IRCSIZ_SBECLASS					7
#define  IRCSIZ_BLDGCLASS					2
#define  IRCSIZ_BLDGQUALITY 				7
#define  IRCSIZ_QUALITYCLASS 				12
#define  IRCSIZ_YRBLT						5
#define  IRCSIZ_YEAREFF						5
#define  IRCSIZ_BLDGS						6
#define  IRCSIZ_BLDGSQFT					12
#define  IRCSIZ_PARKTYPE					2
#define  IRCSIZ_PARKSPACE					12
#define  IRCSIZ_GARSQFT						12
#define  IRCSIZ_POOLSQFT					12
#define  IRCSIZ_PATIOSQFT					12
#define  IRCSIZ_ADDLSQFT					12
#define  IRCSIZ_BSMTSQFT					12
#define  IRCSIZ_DECKSQFT					12
#define  IRCSIZ_UNFINISHEDSQFT 			12
#define  IRCSIZ_GUESSHOUSESQFT 			12
#define  IRCSIZ_PORCHSQFT					12
#define  IRCSIZ_LOTSQFT						12
#define  IRCSIZ_NETRENTALAREA 			12
#define  IRCSIZ_WAREHOUSEAREA				12
#define  IRCSIZ_SHOPAREA					12
#define  IRCSIZ_REFRIGERATEAREA 			12
#define  IRCSIZ_OFFICEAREA 				12
#define  IRCSIZ_MEZZANINEAREA 			12
#define  IRCSIZ_MISCSQFT					12
#define  IRCSIZ_ROOFTYPE					2
#define  IRCSIZ_ROOFMAT						12
#define  IRCSIZ_FOUNDATION 				12
#define  IRCSIZ_POOLS						6
#define  IRCSIZ_POOL							6
#define  IRCSIZ_HEATING						2
#define  IRCSIZ_HEATINGSRC 				30
#define  IRCSIZ_COOLING						2
#define  IRCSIZ_COOLINGSRC 				30
#define  IRCSIZ_BEDS							5
#define  IRCSIZ_BATHS						5
#define  IRCSIZ_FP							12
#define  IRCSIZ_ROOMS						5
#define  IRCSIZ_STORIES						5
#define  IRCSIZ_UNITS						5
#define  IRCSIZ_VIEW							6
#define  IRCSIZ_FMLYROOM					6
#define  IRCSIZ_DINNINGROOM 				6
#define  IRCSIZ_GYM							6
#define  IRCSIZ_ENTRYHALL					6
#define  IRCSIZ_UTILROOM					6
#define  IRCSIZ_SUPROOM						6
#define  IRCSIZ_EXCESSTRAFFIC 			6
#define  IRCSIZ_NUISANCEFACTOR 			6
#define  IRCSIZ_KITCHEN						6
#define  IRCSIZ_DENLIBRARY 				6
#define  IRCSIZ_ATTICAREA					12
#define  IRCSIZ_STUDIOS						6
#define  IRCSIZ_LAUNDRYROOM 				6
#define  IRCSIZ_STORAGEROOM 				12
#define  IRCSIZ_RECROOM						6
#define  IRCSIZ_OTHERROOMS 				6
#define  IRCSIZ_DECK							6
#define  IRCSIZ_FIRESPLER					6
#define  IRCSIZ_BALCONY						6
#define  IRCSIZ_LIVINGROOM 				6
#define  IRCSIZ_FREIGHTELEVATOR 			6
#define  IRCSIZ_SF1STFL						12
#define  IRCSIZ_SF2NDFL						12
#define  IRCSIZ_SFABV2ND					12
#define  IRCSIZ_TRS							16
#define  IRCSIZ_VACANTLOT					12
#define  IRCSIZ_BLDGTYPE					12
#define  IRCSIZ_TOPO					      12
#define  IRCSIZ_SQFT					      12
#define  IRCSIZ_TEN  					   12
#define  IRCSIZ_TWENTY					   20

typedef struct _tCharRec
{  
   char  Apn 									[IRCSIZ_APN]; 			 
   char  FeeParcel							[IRCSIZ_APN]; 			 
   char  Seq 									[IRCSIZ_TEN]; 			 
   char  HasElec 								[IRCSIZ_TEN]; 		 
   char  HasSeptic							[IRCSIZ_TEN];		 
   char  HasSewer								[IRCSIZ_TEN];		 	
   char  HasWell								[IRCSIZ_TEN];		 
   char  HasGas                        [IRCSIZ_TEN];
   char  HasWater								[IRCSIZ_TEN];		 
   char  HasElevator 						[IRCSIZ_TEN]; 		 
   char  ImprCond								[IRCSIZ_TEN];		 
   char  ConstrType 							[IRCSIZ_TEN]; 		 
   char  NaturalGas 							[IRCSIZ_TEN]; 		 
   char  TypeAccess 							[IRCSIZ_TEN]; 		 
   char  SbeClass								[IRCSIZ_TEN];		 
   char  BldgClass							[IRCSIZ_TEN];		 
   char  BldgQuality 						[IRCSIZ_TEN]; 		 
   char  QualityClass 						[IRCSIZ_TEN]; 		 
   char  YearBuilt							[IRCSIZ_TEN];			 
   char  YearEff								[IRCSIZ_TEN];		 
   char  Bldgs									[IRCSIZ_TEN];			 
   char  BldgSqft								[IRCSIZ_SQFT];		 
   char  ParkType								[IRCSIZ_TEN];		 
   char  ParkSpace							[IRCSIZ_TEN];		 
   char  GarSqft								[IRCSIZ_SQFT];		 
   char  PoolSqft								[IRCSIZ_SQFT];		 
   char  PatioSqft							[IRCSIZ_SQFT];		 
   char  AddlSqft								[IRCSIZ_SQFT];		 
   char  BsmtSqft								[IRCSIZ_SQFT];		 
   char  DeckSqft								[IRCSIZ_SQFT];		 
   char  UnfinishedSqft 					[IRCSIZ_SQFT]; 	 
   char  GuessHouseSqft						[IRCSIZ_SQFT]; 	 
   char  PorchSqft							[IRCSIZ_SQFT];		 
   char  LotSqft								[IRCSIZ_SQFT];		 
   char  LotAcre								[IRCSIZ_SQFT];		 
   char  FlatWork                      [IRCSIZ_SQFT];
   char  NetRentalArea				 		[IRCSIZ_SQFT]; 		 
   char  WareHouseArea						[IRCSIZ_SQFT];		 
   char  ShopArea								[IRCSIZ_SQFT];		 
   char  RefrigerateArea				 	[IRCSIZ_SQFT]; 	 
   char  OfficeArea 							[IRCSIZ_SQFT]; 		 
   char  MezzanineArea 						[IRCSIZ_SQFT]; 		 
	char	VacantLot							[IRCSIZ_SQFT];
	char  MiscSqft								[IRCSIZ_SQFT];		 
   char  Sqft_Flr1							[IRCSIZ_SQFT];		 
   char  Sqft_Flr2							[IRCSIZ_SQFT];		 
   char  Sqft_Above2nd						[IRCSIZ_SQFT];		 
   char  RoofType								[IRCSIZ_TEN];		 
   char  RoofMat								[IRCSIZ_TEN];		 
   char  Foundation							[IRCSIZ_TEN]; 		 
   char  Pools									[IRCSIZ_TEN];			 // Original NumPools
   char  Pool									[IRCSIZ_TEN];			    // Pool code
   char  Heating								[IRCSIZ_TEN];		 
   char  HeatingSrc 							[IRCSIZ_TEN]; 		 
   char  Cooling								[IRCSIZ_TEN];		 
   char  CoolingSrc					 		[IRCSIZ_TEN]; 		 
   char  Bedrooms							   [IRCSIZ_TEN];			 
   char  FullBaths						   [IRCSIZ_TEN];			 
   char  HalfBaths						   [IRCSIZ_TEN];			 
   char  Baths_1Q						      [IRCSIZ_TEN];			 
   char  Baths_2Q						      [IRCSIZ_TEN];			 
   char  Baths_3Q						      [IRCSIZ_TEN];			 
   char  Baths_4Q						      [IRCSIZ_TEN];			 
   char  NumFireplaces						[IRCSIZ_TEN];			 
   char  Rooms									[IRCSIZ_TEN];			 
   char  Stories								[IRCSIZ_TEN];		 
   char  Units									[IRCSIZ_TEN];			 
   char  View									[IRCSIZ_TEN];			 
   char  FmlyRoom								[IRCSIZ_TEN];		 
   char  DinningRoom							[IRCSIZ_TEN]; 		 
   char  Gym									[IRCSIZ_TEN];			 
   char  EntryHall							[IRCSIZ_TEN];		 
   char  UtilRoom								[IRCSIZ_TEN];		 
   char  SupRoom								[IRCSIZ_TEN];		 
   char  ExcessTraffic 						[IRCSIZ_TEN]; 		 
   char  NuisanceFactor						[IRCSIZ_TEN]; 	 
   char  Kitchen								[IRCSIZ_TEN];		 
   char  DenLibrary 							[IRCSIZ_TEN]; 		 
   char  AtticArea							[IRCSIZ_TEN];		 
   char  Studios								[IRCSIZ_TEN];		 
   char  LaundryRoom					 		[IRCSIZ_TEN]; 		 
   char  StorageRoom					 		[IRCSIZ_TEN]; 		 
   char  RecRoom								[IRCSIZ_TEN];		 
   char  OtherRooms					 		[IRCSIZ_TEN]; 		 
   char  Deck									[IRCSIZ_TEN];			 
   char  FireSpler							[IRCSIZ_TEN];		 
   char  Balcony								[IRCSIZ_TEN];		 
   char  LivingRoom 							[IRCSIZ_TEN]; 		 
   char  FreightElevator					[IRCSIZ_TEN]; 	 
   char  TRS									[IRCSIZ_TRS];		
   char  BldgType                      [IRCSIZ_TEN];
   char  BldgSeqNo                     [IRCSIZ_TEN];
   char  UnitSeqNo                     [IRCSIZ_TEN];
   char  Zoning                        [IRCSIZ_TWENTY];
   char  UseCode							   [IRCSIZ_TWENTY];				
   char  Topo							      [IRCSIZ_TEN];				
   char  MiscImpr_Code                 [IRCSIZ_TEN];
   char  Apn_D 								[IRCSIZ_APN]; 			 
} CHAR_REC;

#define  OUTFLD_LEN        50
typedef struct _tOutRec
{  
   char  Apn 									[IRCSIZ_APN]; 			 
   char  FeeParcel							[IRCSIZ_APN]; 			 
   char  Seq 									[OUTFLD_LEN]; 			 
   char  HasElec 								[OUTFLD_LEN]; 		 
   char  HasSeptic							[OUTFLD_LEN];		 
   char  HasSewer								[OUTFLD_LEN];		 	
   char  HasWell								[OUTFLD_LEN];		 
   char  HasGas                        [OUTFLD_LEN];
   char  HasWater								[OUTFLD_LEN];		 
   char  HasElevator 						[OUTFLD_LEN]; 		 
   char  ImprCond								[OUTFLD_LEN];		 
   char  ConstrType 							[OUTFLD_LEN]; 		 
   char  NaturalGas 							[OUTFLD_LEN]; 		 
   char  TypeAccess 							[OUTFLD_LEN]; 		 
   char  SbeClass								[OUTFLD_LEN];		 
   char  BldgClass							[OUTFLD_LEN];		 
   char  BldgQuality 						[OUTFLD_LEN]; 		 
   char  QualityClass 						[OUTFLD_LEN]; 		 
   char  YearBuilt							[OUTFLD_LEN];			 
   char  YearEff								[OUTFLD_LEN];		 
   char  Bldgs									[OUTFLD_LEN];			 
   char  ParkType								[OUTFLD_LEN];		 
   char  ParkSpace							[OUTFLD_LEN];		 
   char  BldgSqft								[IRCSIZ_SQFT];		 
   char  GarSqft								[IRCSIZ_SQFT];		 
   char  PoolSqft								[IRCSIZ_SQFT];		 
   char  PatioSqft							[IRCSIZ_SQFT];		 
   char  AddlSqft								[IRCSIZ_SQFT];		 
   char  BsmtSqft								[IRCSIZ_SQFT];		 
   char  DeckSqft								[IRCSIZ_SQFT];		 
   char  UnfinishedSqft 					[IRCSIZ_SQFT]; 	 
   char  GuessHouseSqft						[IRCSIZ_SQFT]; 	 
   char  PorchSqft							[IRCSIZ_SQFT];		 
   char  LotSqft								[IRCSIZ_SQFT];		 
   char  LotAcre								[IRCSIZ_SQFT];		 
   char  FlatWork                      [IRCSIZ_SQFT];
   char  NetRentalArea				 		[IRCSIZ_SQFT]; 		 
   char  WareHouseArea						[IRCSIZ_SQFT];		 
   char  ShopArea								[IRCSIZ_SQFT];		 
   char  RefrigerateArea				 	[IRCSIZ_SQFT]; 	 
   char  OfficeArea 							[IRCSIZ_SQFT]; 		 
   char  MezzanineArea 						[IRCSIZ_SQFT]; 		 
	char	VacantLot							[IRCSIZ_SQFT];
	char  MiscSqft								[IRCSIZ_SQFT];		 
   char  Sqft_Flr1							[IRCSIZ_SQFT];		 
   char  Sqft_Flr2							[IRCSIZ_SQFT];		 
   char  Sqft_Above2nd						[IRCSIZ_SQFT];		 
   char  RoofType								[OUTFLD_LEN];		 
   char  RoofMat								[OUTFLD_LEN];		 
   char  Foundation							[OUTFLD_LEN]; 		 
   char  Pools									[OUTFLD_LEN];			 // Original NumPools
   char  Pool									[OUTFLD_LEN];			    // Pool code
   char  Heating								[OUTFLD_LEN];		 
   char  HeatingSrc 							[OUTFLD_LEN]; 		 
   char  Cooling								[OUTFLD_LEN];		 
   char  CoolingSrc					 		[OUTFLD_LEN]; 		 
   char  Bedrooms							   [OUTFLD_LEN];			 
   char  FullBaths						   [OUTFLD_LEN];			 
   char  HalfBaths						   [OUTFLD_LEN];			 
   char  Bath_1Q						      [OUTFLD_LEN];			 
   char  Bath_2Q						      [OUTFLD_LEN];			 
   char  Bath_3Q						      [OUTFLD_LEN];			 
   char  Bath_4Q						      [OUTFLD_LEN];			 
   char  NumFireplaces						[OUTFLD_LEN];			 
   char  Rooms									[OUTFLD_LEN];			 
   char  Stories								[OUTFLD_LEN];		 
   char  Units									[OUTFLD_LEN];			 
   char  View									[OUTFLD_LEN];			 
   char  FmlyRoom								[OUTFLD_LEN];		 
   char  DinningRoom							[OUTFLD_LEN]; 		 
   char  Gym									[OUTFLD_LEN];			 
   char  EntryHall							[OUTFLD_LEN];		 
   char  UtilRoom								[OUTFLD_LEN];		 
   char  SupRoom								[OUTFLD_LEN];		 
   char  ExcessTraffic 						[OUTFLD_LEN]; 		 
   char  NuisanceFactor						[OUTFLD_LEN]; 	 
   char  Kitchen								[OUTFLD_LEN];		 
   char  DenLibrary 							[OUTFLD_LEN]; 		 
   char  AtticArea							[OUTFLD_LEN];		 
   char  Studios								[OUTFLD_LEN];		 
   char  LaundryRoom					 		[OUTFLD_LEN]; 		 
   char  StorageRoom					 		[OUTFLD_LEN]; 		 
   char  RecRoom								[OUTFLD_LEN];		 
   char  OtherRooms					 		[OUTFLD_LEN]; 		 
   char  Deck									[OUTFLD_LEN];			 
   char  FireSpler							[OUTFLD_LEN];		 
   char  Balcony								[OUTFLD_LEN];		 
   char  LivingRoom 							[OUTFLD_LEN]; 		 
   char  FreightElevator					[OUTFLD_LEN]; 	 
   char  TRS									[OUTFLD_LEN];		
   char  BldgType                      [OUTFLD_LEN];
   char  BldgSeqNo                     [OUTFLD_LEN];
   char  UnitSeqNo                     [OUTFLD_LEN];
   char  Zoning                        [OUTFLD_LEN];
   char  UseCode							   [OUTFLD_LEN];				
   char  Topo							      [OUTFLD_LEN];				
   char  MiscImpr_Code                 [OUTFLD_LEN];
   char  Apn_D 								[IRCSIZ_APN]; 			 
} OUT_REC;

#define  IRTSIZ_APN							20
#define  IRTSIZ_TAXAMT1						16
#define  IRTSIZ_TAXAMT2						16
#define  IRTSIZ_PENAMT1						16
#define  IRTSIZ_PENAMT2						16
#define  IRTSIZ_PENDATE1					12
#define  IRTSIZ_PENDATE2					12
#define  IRTSIZ_PAIDAMT1					16
#define  IRTSIZ_PAIDAMT2					16
#define  IRTSIZ_TOTALPAID1					16
#define  IRTSIZ_TOTALPAID2					16
#define  IRTSIZ_TOTALFEES					16
#define  IRTSIZ_PAIDDATE1					12
#define  IRTSIZ_PAIDDATE2					12
#define  IRTSIZ_PERSPEN						16
#define  IRTSIZ_UNSDELPENAMTPAID	      16
#define  IRTSIZ_SECDELPENAMTPAID	      16
#define  IRTSIZ_PSIPENALTY					16
#define  IRTSIZ_PENCD1						16
#define  IRTSIZ_PENCD2						16
#define  IRTSIZ_PRIORTAXPAID1   	      16
#define  IRTSIZ_TAXYEAR						6

typedef struct _tTaxRec
{
   char  Apn					[IRTSIZ_APN];		   	
   char  TaxAmt				[IRTSIZ_TAXAMT1];		   	
   char  PenAmt1				[IRTSIZ_PENAMT1];		   	
   char  PenAmt2				[IRTSIZ_PENAMT2];		   	
   char  PenDate1				[IRTSIZ_PENDATE1];		
   char  PenDate2				[IRTSIZ_PENDATE2];		
   char  PaidAmt1				[IRTSIZ_PAIDAMT1];		
   char  PaidAmt2				[IRTSIZ_PAIDAMT2];		
   char  TotalPaid1			[IRTSIZ_TOTALPAID1];	   	
   char  TotalPaid2			[IRTSIZ_TOTALPAID2];	   	
   char  TotalFees			[IRTSIZ_TOTALFEES];	   	
   char  PaidDate1			[IRTSIZ_PAIDDATE1];	   	
   char  PaidDate2			[IRTSIZ_PAIDDATE2];	   	
   char  PersPen				[IRTSIZ_PERSPEN];		   	
   char  UnsDelPenAmtPaid	[IRTSIZ_UNSDELPENAMTPAID];	
   char  SecDelPenAmtPaid	[IRTSIZ_SECDELPENAMTPAID];	
   char  PsiPenalty			[IRTSIZ_PSIPENALTY];	   	
   char  Pencd1				[IRTSIZ_PENCD1];		   	
   char  Pencd2				[IRTSIZ_PENCD2];		   	
   char  PriorTaxPaid1     [IRTSIZ_PRIORTAXPAID1];     
   char  TaxAmt1			   [IRTSIZ_TAXAMT1];		   	
   char  TaxAmt2			   [IRTSIZ_TAXAMT1];		   	
   char  TaxYear				[IRTSIZ_TAXYEAR];	
   char  FeeParcel         [IRTSIZ_APN];
} TAX_REC;



#define IRSSIZ_GR_DOCNUM						16
#define IRSSIZ_GR_APN							20
#define IRSSIZ_GR_DOCDATE						8
#define IRSSIZ_GR_TITLE							70
#define IRSSIZ_GR_TAX							10   
#define IRSSIZ_GR_SALE							10
#define IRSSIZ_GR_NUMPG							4	
#define IRSSIZ_GR_NAMECNT						4
#define IRSSIZ_GR_GRANTOR						70
#define IRSSIZ_GR_GRANTEE						70
#define IRSSIZ_GR_REF							100

typedef struct _tGrGrRec
{
	char Apn[IRSSIZ_GR_APN];
	char DocNum[IRSSIZ_GR_DOCNUM];
	char DocDate[IRSSIZ_DOCDATE];
	char Title[IRSSIZ_GR_TITLE];
	char Tax[IRSSIZ_GR_TAX];
	char Sale[IRSSIZ_GR_SALE];
	char NumPg[IRSSIZ_GR_NUMPG];
	char NameCnt[IRSSIZ_GR_NAMECNT];
	char Grantor[20][IRSSIZ_GR_GRANTOR];
	char Grantee[20][IRSSIZ_GR_GRANTEE];
	char Ref[IRSSIZ_GR_REF];
} GRGR_REC;

typedef struct _tTR601Rec
{
   ROLL_REC Roll;
   VALU_REC Value;
} TR601_REC;

#endif