
#if !defined(AFX_PQEXTRACT_H__B09D3D8D_F429_45B6_BACD_9BC0B309D0CF__INCLUDED_)
#define AFX_PQEXTRACT_H__B09D3D8D_F429_45B6_BACD_9BC0B309D0CF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"

#define  ROLL_SIZE      899
#define  MAX_SUFFIX     256
#define  MAX_RECSIZE    4096
#define  MAX_FLD_TOKEN  64

#define  OPEN_ERR       0xF0000001
#define  READ_ERR       0xF0000002
#define  WRITE_ERR      0xF0000004
#define  NOTFOUND_ERR   0xF0000008
#define  BADRECSIZE_ERR 0xF0000010

#define  UPDATE_R01     0
#define  CREATE_R01     1
#define  CREATE_LIEN    2
#define  CLEAR_R01      4

#define  LOAD_LIEN      0x10000000
#define  LOAD_UPDT      0x01000000
#define  LOAD_GRGR      0x00100000
#define  LOAD_SALE      0x00010000
#define  LOAD_ATTR      0x00001000
#define  LOAD_ASSR      0x00000100

#define  EXTR_LIEN      0x20000000
#define  EXTR_CSAL      0x02000000
#define  EXTR_GRGR      0x00200000
#define  EXTR_SALE      0x00020000
#define  EXTR_ATTR      0x00002000
#define  EXTR_DTWR      0x00000200
#define  EXTR_CONF      0x00000020
#define  EXTR_REGN      0x00000002

#define  MERG_LIEN      0x40000000
#define  MERG_CSAL      0x04000000
#define  MERG_GRGR      0x00400000
#define  MERG_SALE      0x00040000
#define  MERG_ATTR      0x00004000
#define  MERG_SADR      0x00000400

#define  UPDT_SALE      0x00080000
#define  UPDT_ASSR      0x00000800

#define  CREOFF_PARCTYPE            1-1
#define  CREOFF_TRA                 5-1
#define  CREOFF_APN                 11-1
#define  CREOFF_5YR_PLAN            21-1
#define  CREOFF_PP_STMT             22-1
#define  CREOFF_PP_PENL             23-1
#define  CREOFF_INFL_FACTOR         24-1
#define  CREOFF_BUS_TYPE            27-1
#define  CREOFF_NOTIFY              30-1
#define  CREOFF_EXE_CLAIM           31-1
#define  CREOFF_BASE_CODE           32-1
#define  CREOFF_USE_CODE            34-1
#define  CREOFF_MH_LIC              40-1
#define  CREOFF_NAME_1              47-1
#define  CREOFF_NAME_2              76-1
#define  CREOFF_ADDR_1              105-1
#define  CREOFF_ADDR_2              134-1
#define  CREOFF_ROLL_YEAR           163-1
#define  CREOFF_LAND_BASE_YEAR      167-1
#define  CREOFF_IMPROV_BASE_YEAR    171-1
#define  CREOFF_PENL_AMT            175-1
#define  CREOFF_ZIP                 182-1
#define  CREOFF_FILLER1             187-1
#define  CREOFF_ZIP_4               188-1
#define  CREOFF_DEFAULT_NO          192-1
#define  CREOFF_DEFAULT_DATE        197-1    // YYMMDD
#define  CREOFF_BLDG_CLS            203-1
#define  CREOFF_BEDROOMS            208-1
#define  CREOFF_BATHS               209-1    // 9V99
#define  CREOFF_STRUCT_SQFT         212-1
#define  CREOFF_LOT_SQFT            220-1
#define  CREOFF_APPRAISER_ID        229-1
#define  CREOFF_LABEL               232-1
#define  CREOFF_REAPP_CODE          233-1
#define  CREOFF_ASSIGN_DATE         235-1      // YYMMDD
#define  CREOFF_NOTICE_DATE         241-1      // YYMMDD
#define  CREOFF_REAPP_PCT           247-1
#define  CREOFF_RECORDERS_BOOK1     251-1
#define  CREOFF_RECORDING_DATE1     259-1
#define  CREOFF_RECORDERS_BOOK2     267-1
#define  CREOFF_RECORDING_DATE2     275-1
#define  CREOFF_RECORDERS_BOOK3     283-1
#define  CREOFF_RECORDING_DATE3     291-1
#define  CREOFF_DBA                 299-1
#define  CREOFF_LGL_DESC1           328-1
#define  CREOFF_LGL_DESC2           359-1
#define  CREOFF_BRD_ORDER_NO        390-1
#define  CREOFF_UNITS               396-1
#define  CREOFF_TOT_EDU             399-1
#define  CREOFF_SF_REC              407-1
#define  CREOFF_PROBLEM_NO          412-1
#define  CREOFF_MH_DECAL            416-1
#define  CREOFF_MH_SERIAL           424-1
#define  CREOFF_REAPP_DATE          439-1
#define  CREOFF_STMP_AMT            447-1
#define  CREOFF_HLTV                464-1
#define  CREOFF_DIRECT_BILL         472-1
#define  CREOFF_EFF_YR              473-1
#define  CREOFF_YR_BUILT            477-1
#define  CREOFF_FLAG                481-1
#define  CREOFF_REAPP_REASON        482-1
#define  CREOFF_ADDR_CHG_DATE       486-1
#define  CREOFF_NAME_CHG_DATE       494-1
#define  CREOFF_VAL_CHG_DATE        502-1
#define  CREOFF_SITUS_STREETNO      510-1
#define  CREOFF_SITUS_STREETNAME    518-1
#define  CREOFF_NON_RES_NC          540-1
#define  CREOFF_NRNC_RBP            550-1
#define  CREOFF_SPECIAL_FEE1        560-1
#define  CREOFF_SPECIAL_FEE2        568-1
#define  CREOFF_BUS_ACCT            576-1
#define  CREOFF_BRD_ACTION          586-1
#define  CREOFF_SW_CHG              595-1
#define  CREOFF_TYPESQFT            605-1
#define  CREOFF_SPECIAL_FEE3        606-1
#define  CREOFF_SPECIAL_FEE4        614-1
#define  CREOFF_FIREPLACE           622-1
#define  CREOFF_AIR_COND            623-1
#define  CREOFF_FLOORS              624-1
#define  CREOFF_MISC_IMPROV         625-1
#define  CREOFF_GARAGE              626-1
#define  CREOFF_POOL                627-1
#define  CREOFF_SPA                 628-1
#define  CREOFF_HEATING             629-1
#define  CREOFF_SITUS_CITY          630-1
#define  CREOFF_COMMERCIAL_SQFT     660-1
#define  CREOFF_NEIGHBOR_CODE       666-1
#define  CREOFF_BOND_NO             669-1
#define  CREOFF_POSTPONE_FLG        675-1
#define  CREOFF_WORK_ORDER_NO       676-1
#define  CREOFF_WORK_ORDER_DATE     684-1
#define  CREOFF_PERNIT_NO           692-1
#define  CREOFF_APPR_REC_NO         703-1
#define  CREOFF_BASE_IMP            709-1
#define  CREOFF_BASE_LAND           719-1
#define  CREOFF_ZONE                729-1
#define  CREOFF_ROOMS               739-1
#define  CREOFF_ACRES               742-1
#define  CREOFF_NET_VAL             752-1
#define  CREOFF_LAND_VAL            762-1
#define  CREOFF_IMP_VAL             772-1
#define  CREOFF_PP_VAL              782-1
#define  CREOFF_EX_VAL              792-1
#define  CREOFF_FIXT_VAL            802-1
#define  CREOFF_EXEMP_CODE1         812-1
#define  CREOFF_EXEMP_CODE2         814-1
#define  CREOFF_EXEMP_CODE3         816-1
#define  CREOFF_EXEMP_CODE4         818-1
#define  CREOFF_EXEMP_VAL1          820-1
#define  CREOFF_EXEMP_VAL2          830-1
#define  CREOFF_EXEMP_VAL3          840-1
#define  CREOFF_EXEMP_VAL4          850-1
#define  CREOFF_MOBILE_VAL          860-1
#define  CREOFF_LEASE_VAL           870-1
#define  CREOFF_PERS_FIXT_VAL       880-1
#define  CREOFF_PERS_PEN_VAL        890-1

#define  CRESIZ_PARCTYPE            2
#define  CRESIZ_TRA                 6
#define  CRESIZ_APN                 10
#define  CRESIZ_BASE_CODE           2
#define  CRESIZ_USE_CODE            6
#define  CRESIZ_NAME_1              29
#define  CRESIZ_NAME_2              29
#define  CRESIZ_ADDR_1              29
#define  CRESIZ_ADDR_2              29
#define  CRESIZ_ROLL_YEAR           4
#define  CRESIZ_LAND_BASE_YEAR      4
#define  CRESIZ_IMPROV_BASE_YEAR    4
#define  CRESIZ_ZIP                 5
#define  CRESIZ_ZIP_4               4
#define  CRESIZ_BLDG_CLS            5
#define  CRESIZ_BEDROOMS            1
#define  CRESIZ_BATHS               3
#define  CRESIZ_STRUCT_SQFT         8
#define  CRESIZ_LOT_SQFT            9
#define  CRESIZ_RECORDERS_BOOK      8
#define  CRESIZ_RECORDING_DATE      8
#define  CRESIZ_DBA                 29
#define  CRESIZ_LGL_DESC            31
#define  CRESIZ_LGL_DESC1           31
#define  CRESIZ_LGL_DESC2           31
#define  CRESIZ_UNITS               3
#define  CRESIZ_SF_REC              5
#define  CRESIZ_STMP_AMT            17
#define  CRESIZ_EFF_YR              4
#define  CRESIZ_YR_BUILT            4
#define  CRESIZ_FLAG                1
#define  CRESIZ_SITUS_STREETNO      8
#define  CRESIZ_SITUS_STREETNAME    22
#define  CRESIZ_NON_RES_NC          10
#define  CRESIZ_TYPESQFT            1
#define  CRESIZ_SPECIAL_FEE3        8
#define  CRESIZ_SPECIAL_FEE4        8
#define  CRESIZ_FIREPLACE           1
#define  CRESIZ_AIR_COND            1
#define  CRESIZ_FLOORS              1
#define  CRESIZ_MISC_IMPROV         1
#define  CRESIZ_GARAGE              1
#define  CRESIZ_POOL                1
#define  CRESIZ_SPA                 1
#define  CRESIZ_HEATING             1
#define  CRESIZ_SITUS_CITY          30
#define  CRESIZ_COMMERCIAL_SQFT     6
#define  CRESIZ_BASE_IMP            10
#define  CRESIZ_BASE_LAND           10
#define  CRESIZ_ZONE                10
#define  CRESIZ_ROOMS               3
#define  CRESIZ_ACRES               10
#define  CRESIZ_NET_VAL             10
#define  CRESIZ_LAND_VAL            10
#define  CRESIZ_IMP_VAL             10
#define  CRESIZ_PP_VAL              10
#define  CRESIZ_EX_VAL              10
#define  CRESIZ_FIXT_VAL            10
#define  CRESIZ_EXEMP_CODE          2
#define  CRESIZ_EXEMP_CODE1         2
#define  CRESIZ_EXEMP_CODE2         2
#define  CRESIZ_EXEMP_CODE3         2
#define  CRESIZ_EXEMP_CODE4         2
#define  CRESIZ_EXEMP_VAL           10
#define  CRESIZ_EXEMP_VAL1          10
#define  CRESIZ_EXEMP_VAL2          10
#define  CRESIZ_EXEMP_VAL3          10
#define  CRESIZ_EXEMP_VAL4          10
#define  CRESIZ_MOBILE_VAL          10
#define  CRESIZ_LEASE_VAL           10
#define  CRESIZ_PERS_FIXT_VAL       10
#define  CRESIZ_PERS_PEN_VAL        10

/***********************************************************************************
 *
 * Redifile record layout
 *
 ***********************************************************************************/

typedef struct _tRedifile
{
   char  ParcType[CRESIZ_PARCTYPE];
   char  filler1[2];
   char  TRA[CRESIZ_TRA];
   char  APN[CRESIZ_APN];
   char  filler2[11];
   char  BaseCode[CRESIZ_BASE_CODE];
   char  UseCode[CRESIZ_USE_CODE];
   char  MH_Lic[7];
   char  Name1[CRESIZ_NAME_1];
   char  Name2[CRESIZ_NAME_2];
   char  M_Addr1[CRESIZ_ADDR_1];
   char  M_Addr2[CRESIZ_ADDR_2];
   char  RollYr[CRESIZ_ROLL_YEAR];
   char  LandBaseYr[CRESIZ_ROLL_YEAR];
   char  ImprBaseYr[CRESIZ_ROLL_YEAR];
   char  PenAmt[7];
   char  M_Zip[CRESIZ_ZIP];
   char  filler3;
   char  M_Zip4[CRESIZ_ZIP_4];
   char  DefaultNo[5];
   char  DefaultDate[6];   //MMDDYY
   char  BldgCls[CRESIZ_BLDG_CLS];
   char  Beds[1];
   char  Baths[CRESIZ_BATHS];
   char  StruSqft[CRESIZ_STRUCT_SQFT];
   char  LotSqft[CRESIZ_LOT_SQFT];
   char  filler5[22];
   char  RecBook1[CRESIZ_RECORDERS_BOOK];
   char  RecDate1[CRESIZ_RECORDING_DATE];
   char  RecBook2[CRESIZ_RECORDERS_BOOK];
   char  RecDate2[CRESIZ_RECORDING_DATE];
   char  RecBook3[CRESIZ_RECORDERS_BOOK];
   char  RecDate3[CRESIZ_RECORDING_DATE];
   char  Dba[CRESIZ_DBA];
   char  LglDesc1[CRESIZ_LGL_DESC];
   char  LglDesc2[CRESIZ_LGL_DESC];
   char  BrdOrderNo[6];
   char  NumOfUnits[CRESIZ_UNITS];
   char  filler6[48];
   char  StampAmt[CRESIZ_STMP_AMT];
   char  filler7[9];
   char  YrEff[CRESIZ_EFF_YR];
   char  YrBlt[CRESIZ_EFF_YR];
   char  filler8[29];
   char  S_StrNo[CRESIZ_SITUS_STREETNO];
   char  S_StrName[CRESIZ_SITUS_STREETNAME];
   char  filler9[82];
   char  Fp[1];
   char  Ac[1];
   char  Fl[1];
   char  MiscImpr[1];
   char  Garage[1];
   char  Pool[1];
   char  Spa[1];
   char  Heating[1];
   char  S_City[CRESIZ_SITUS_CITY];
   char  CommSqft[CRESIZ_COMMERCIAL_SQFT];
   char  NeighHood[3];
   char  filler10[40];
   char  BaseImpr[CRESIZ_BASE_IMP];
   char  BaseLand[CRESIZ_BASE_LAND];
   char  Zone[CRESIZ_ZONE];
   char  Rooms[CRESIZ_ROOMS];
   char  Acres[CRESIZ_ACRES];
   char  NetVal[CRESIZ_NET_VAL];
   char  LandVal[CRESIZ_LAND_VAL];
   char  ImprVal[CRESIZ_IMP_VAL];
   char  PPVal[CRESIZ_PP_VAL];
   char  ExVal[CRESIZ_EX_VAL];
   char  FixtVal[CRESIZ_FIXT_VAL];
   char  ExeCode1[CRESIZ_EXEMP_CODE];
   char  ExeCode2[CRESIZ_EXEMP_CODE];
   char  ExeCode3[CRESIZ_EXEMP_CODE];
   char  ExeCode4[CRESIZ_EXEMP_CODE];
   char  ExeVal1[CRESIZ_EXEMP_VAL];
   char  ExeVal2[CRESIZ_EXEMP_VAL];
   char  ExeVal3[CRESIZ_EXEMP_VAL];
   char  ExeVal4[CRESIZ_EXEMP_VAL];
   char  MobileVal[CRESIZ_MOBILE_VAL];
   char  LeaseVal[CRESIZ_LEASE_VAL];
   char  PersFixtVal[CRESIZ_PERS_FIXT_VAL];
   char  PersPenVal[CRESIZ_PERS_PEN_VAL];
} REDIFILE;

int CreateR01Rec(char *pCnty, char *pOutbuf, char *pRollRec, int iLen);
int MergeGrGrFile(char *pCnty);





#endif

