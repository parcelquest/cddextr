USE [Spatial]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[spCreateFTIndex]
   @CntyTbl [varchar](3)
WITH EXECUTE AS CALLER
AS
Declare @sql as nvarchar(400)

BEGIN
   SET @sql = N'DROP FULLTEXT INDEX ON ' + @CntyTbl + '_Roll;'
   EXEC sp_executesql @sql

   SET @sql = N'DROP FULLTEXT Catalog ' + @CntyTbl + '_RollFTCat;'
   EXEC sp_executesql @sql

   SET @sql = N'CREATE FULLTEXT Catalog ' + @CntyTbl + '_RollFTCat WITH ACCENT_SENSITIVITY=OFF AS DEFAULT;'
   EXEC sp_executesql @sql

   SET @sql=N'CREATE FULLTEXT INDEX ON ' + @CntyTbl + '_Roll (Names, S_Street, M_Street)
      KEY INDEX PK_' + @CntyTbl + '_Roll ON ' + @CntyTbl + '_RollFTCat WITH CHANGE_TRACKING AUTO,STOPLIST=OFF;'
   EXEC sp_executesql @sql
END
GO
