@echo CreateITracTbl %1 %2 %3
@echo off
if "%1" == "" goto Usage
if "%3" == "" goto DefaultServer
sqlcmd -S %3 -v Cnty=%1 Year='%2' -i c:\tools\cddextr\cmdCreateITracTbl.sql
bcp spatial.dbo.%1_LDR%2 in \\dev3\CO_IMPORT\ITrac\20%2\%1_Import.csv -c -e \\dev3\CO_Logs\CddExtr\%1_%2.log -T -t"|" -r\n -a65535 -k -F2 -q -U pq -P cdd -S %3
goto exitcmd
:DefaultServer
sqlcmd -S dev3\sql2008 -v Cnty=%1 Year='%2' -i c:\tools\cddextr\cmdCreateITracTbl.sql
bcp spatial.dbo.%1_LDR%2 in \\dev3\CO_IMPORT\ITrac\20%2\%1_Import.csv -c -e \\dev3\CO_Logs\CddExtr\%1_%2.log -T -t"|" -r\n -a65535 -k -F2 -q
goto exitcmd
:Usage
@echo ........................
@echo Usage: CreateITracTbl {county code} {LDR 2-digit year} [sqlserver]
@echo ........................

:exitcmd

