@echo CreateXCATbl %1 %2 %3
@echo off
if "%1" == "" goto DefaultServer
sqlcmd -S %1 -i c:\tools\cddextr\cmdCreateXCATbl.sql
goto exitcmd
:DefaultServer
sqlcmd -i c:\tools\cddextr\cmdCreateXCATbl.sql
goto exitcmd
:Usage
@echo ........................
@echo Usage: CreateXCATbl [sqlserver]
@echo ........................

:exitcmd

