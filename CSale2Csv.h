#ifndef CSALE2CSV_H
int createSaleImport(LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType=1, bool bInclHdr=false);
int createSaleImport(void (*fn)(LPSTR, LPCSTR, LPCSTR, LPCSTR), LPCSTR CountyCode, LPCSTR pInfile, LPCSTR pOutfile, int iType=1, bool bInclHdr=false);
int createSaleCsv(LPCSTR CountyCode, int iYear, int iType=1);
int createBulkSale(LPCSTR CountyCode, LPCSTR pOutfile, int iYear, int iType=1);
#endif