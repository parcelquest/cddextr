UPDATE ALA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM ALA_Roll r INNER JOIN ALA_Basemap b on r.APN = b.APN
GO

UPDATE ALP_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM ALP_Roll r INNER JOIN ALP_Basemap b on r.APN = b.APN
GO

UPDATE AMA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM AMA_Roll r INNER JOIN AMA_Basemap b on r.APN = b.APN
GO

UPDATE BUT_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM BUT_Roll r INNER JOIN BUT_Basemap b on r.APN = b.APN
GO

UPDATE CAL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM CAL_Roll r INNER JOIN CAL_Basemap b on r.APN = b.APN
GO

UPDATE COL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM COL_Roll r INNER JOIN COL_Basemap b on r.APN = b.APN
GO

UPDATE CCX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM CCX_Roll r INNER JOIN CCX_Basemap b on r.APN = b.APN
GO

UPDATE DNX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM DNX_Roll r INNER JOIN DNX_Basemap b on r.APN = b.APN
GO

UPDATE EDX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM EDX_Roll r INNER JOIN EDX_Basemap b on r.APN = b.APN
GO

UPDATE FRE_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM FRE_Roll r INNER JOIN FRE_Basemap b on r.APN = b.APN
GO

UPDATE GLE_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM GLE_Roll r INNER JOIN GLE_Basemap b on r.APN = b.APN
GO

UPDATE HUM_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM HUM_Roll r INNER JOIN HUM_Basemap b on r.APN = b.APN
GO

UPDATE IMP_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM IMP_Roll r INNER JOIN IMP_Basemap b on r.APN = b.APN
GO

UPDATE INY_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM INY_Roll r INNER JOIN INY_Basemap b on r.APN = b.APN
GO

UPDATE KER_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM KER_Roll r INNER JOIN KER_Basemap b on r.APN = b.APN
GO

UPDATE KIN_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM KIN_Roll r INNER JOIN KIN_Basemap b on r.APN = b.APN
GO

UPDATE LAK_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM LAK_Roll r INNER JOIN LAK_Basemap b on r.APN = b.APN
GO

UPDATE LAS_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM LAS_Roll r INNER JOIN LAS_Basemap b on r.APN = b.APN
GO

UPDATE LAX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM LAX_Roll r INNER JOIN LAX_Basemap b on r.APN = b.APN
GO

UPDATE MAD_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MAD_Roll r INNER JOIN MAD_Basemap b on r.APN = b.APN
GO

UPDATE MPA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MPA_Roll r INNER JOIN MPA_Basemap b on r.APN = b.APN
GO

UPDATE MEN_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MEN_Roll r INNER JOIN MEN_Basemap b on r.APN = b.APN
GO

UPDATE MER_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MER_Roll r INNER JOIN MER_Basemap b on r.APN = b.APN
GO

UPDATE MNO_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MNO_Roll r INNER JOIN MNO_Basemap b on r.APN = b.APN
GO

UPDATE MOD_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MOD_Roll r INNER JOIN MOD_Basemap b on r.APN = b.APN
GO

UPDATE MON_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MON_Roll r INNER JOIN MON_Basemap b on r.APN = b.APN
GO

UPDATE MRN_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM MRN_Roll r INNER JOIN MRN_Basemap b on r.APN = b.APN
GO

UPDATE NAP_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM NAP_Roll r INNER JOIN NAP_Basemap b on r.APN = b.APN
GO

UPDATE NEV_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM NEV_Roll r INNER JOIN NEV_Basemap b on r.APN = b.APN
GO

UPDATE ORG_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM ORG_Roll r INNER JOIN ORG_Basemap b on r.APN = b.APN
GO

UPDATE PLA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM PLA_Roll r INNER JOIN PLA_Basemap b on r.APN = b.APN
GO

UPDATE PLU_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM PLU_Roll r INNER JOIN PLU_Basemap b on r.APN = b.APN
GO

UPDATE RIV_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM RIV_Roll r INNER JOIN RIV_Basemap b on r.APN = b.APN
GO

UPDATE SAC_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SAC_Roll r INNER JOIN SAC_Basemap b on r.APN = b.APN
GO

UPDATE SBD_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SBD_Roll r INNER JOIN SBD_Basemap b on r.APN = b.APN
GO

UPDATE SBT_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SBT_Roll r INNER JOIN SBT_Basemap b on r.APN = b.APN
GO

UPDATE SBX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SBX_Roll r INNER JOIN SBX_Basemap b on r.APN = b.APN
GO

UPDATE SCL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SCL_Roll r INNER JOIN SCL_Basemap b on r.APN = b.APN
GO

UPDATE SCR_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SCR_Roll r INNER JOIN SCR_Basemap b on r.APN = b.APN
GO

UPDATE SDX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SDX_Roll r INNER JOIN SDX_Basemap b on r.APN = b.APN
GO

UPDATE SFX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SFX_Roll r INNER JOIN SFX_Basemap b on r.APN = b.APN
GO

UPDATE SHA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SHA_Roll r INNER JOIN SHA_Basemap b on r.APN = b.APN
GO

UPDATE SIE_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SIE_Roll r INNER JOIN SIE_Basemap b on r.APN = b.APN
GO

UPDATE SIS_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SIS_Roll r INNER JOIN SIS_Basemap b on r.APN = b.APN
GO

UPDATE SJX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SJX_Roll r INNER JOIN SJX_Basemap b on r.APN = b.APN
GO

UPDATE SLO_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SLO_Roll r INNER JOIN SLO_Basemap b on r.APN = b.APN
GO

UPDATE SMX_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SMX_Roll r INNER JOIN SMX_Basemap b on r.APN = b.APN
GO

UPDATE SOL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SOL_Roll r INNER JOIN SOL_Basemap b on r.APN = b.APN
GO

UPDATE SON_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SON_Roll r INNER JOIN SON_Basemap b on r.APN = b.APN
GO

UPDATE STA_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM STA_Roll r INNER JOIN STA_Basemap b on r.APN = b.APN
GO

UPDATE SUT_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM SUT_Roll r INNER JOIN SUT_Basemap b on r.APN = b.APN
GO

UPDATE TEH_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM TEH_Roll r INNER JOIN TEH_Basemap b on r.APN = b.APN
GO

UPDATE TRI_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM TRI_Roll r INNER JOIN TRI_Basemap b on r.APN = b.APN
GO

UPDATE TUL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM TUL_Roll r INNER JOIN TUL_Basemap b on r.APN = b.APN
GO

UPDATE TUO_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM TUO_Roll r INNER JOIN TUO_Basemap b on r.APN = b.APN
GO

UPDATE VEN_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM VEN_Roll r INNER JOIN VEN_Basemap b on r.APN = b.APN
GO

UPDATE YOL_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM YOL_Roll r INNER JOIN YOL_Basemap b on r.APN = b.APN
GO

UPDATE YUB_Roll SET
   Longitude = CAST(b.Longitude AS float),
   Latitude = CAST(b.Latitude AS float),
   GEOMpt = geography::STPointFromText('POINT (' + b.Longitude + ' ' + b.Latitude + ')',4326)
FROM YUB_Roll r INNER JOIN YUB_Basemap b on r.APN = b.APN
GO

