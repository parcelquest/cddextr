#ifndef T_CITY_INFO
#define  MAX_CITIES        600
#define  MAX_SUFFIX        256
#define  MAX_DIRS          14
#define  MAX_ATTR_ENTRIES  64
#define  MAX_SALETYPE      10

#define  MAX_AIRCOND       48
#define  MAX_PRCLSTAT      10
#define  MAX_CONDITION     10
#define  MAX_HEATING       48
#define  MAX_USECODE       64
#define  MAX_VIEW          48
#define  MAX_PARKTYPE      48
#define  MAX_POOLTYPE      20
#define  MAX_QUALITY       36
#define  MAX_WATER         16
#define  MAX_SEWER         16
#define  MAX_DEEDTYPE      80
#define  MAX_ROOFMAT       28
#define  MAX_TOPO          8

typedef struct t_CityInfo
{
   char  acCityName[32];
   char  acState[4];
   char  acAbbr[8];
   char  acOfficialName[32];
   int   iCityID;
   int   iOldCityID;
   int   iLen;
} CITY_INFO;

typedef struct _tTra2City
{
   char  acTra[8];
   char  acCity[32];
} TRA_CITY;

typedef struct _tSuffix
{
   char  acSuffix[8];
   char  acOrgSfx[12];
   int   iSfxID;
   int   iLen;
} SUFFIX;

typedef struct _tStrSfx
{
   char  *pStrSfx;
   char  *pOrgSfx;
   char  *pSfxCode;
   int   iLen;          // Length of sfxcode
} STRSFX;

typedef struct _tStrDir
{
   char  acName[8];
   char  *pDir;
   int   iDirID;
} STRDIR;

typedef struct _LookUpEntry
{
   char  acIndex[5];
   char  acValue[27];
} LU_ENTRY;

typedef struct _tXrefTbl
{
   int   iIdxNum;
   int   iCodeLen;
   int   iNameLen;
   char  acCode[64];
   char  acName[64];
   char  acOther[40];   // GrGr->NoneXfer (Y/N)
   char  acFlags[8];    // GrGr->NoneSale (Y/N)
   char  filler[16];
} XREFTBL;


typedef struct _tIdxTbl
{
   char  *pName;
   char  *pCode;
} IDX_TBL;

typedef struct _tIdxTbl2
{
   long  lVal;
   char  *pCode;
} IDX_TBL2;

typedef struct _tIdxTbl3
{
   long  lVal;
   char  *pCode;
   char  flag;
} IDX_TBL3;

typedef struct _tIdxTbl4
{
   char  *pName;
   char  *pCode;
   int   iNameLen;
   int   iCodeLen;
} IDX_TBL4;

typedef struct _tIdxTbl5
{
   char  *pName;
   char  *pCode;
   char  flag;
   int   iNameLen;
   int   iCodeLen;
} IDX_TBL5;

typedef struct _tIdxSale
{
   char  *DocTitle;
   char  *DocType; 
   char  isSale;
   char  isTransfer;
   int   DocTitleLen;
   int   DocTypeLen;
} IDX_SALE;

typedef struct _tRegIdx
{
   char  acStartBook[6];
   char  acEndBook[6];
   int   iReg;
} REG_IDX;

typedef struct _tFldIdx
{
   char  *pName;
   int   iType;            // 0=char, 1=LJ int, 2=RJ int, 3=date, 4=1000x, 5=Translate
   int   iNameLen;         // length of pName
   int   iValueLen;        // field length
   int   iOffset;          // offset to output record
} FLD_IDX;


#endif

int   LoadCities(char *pCityFile);
int   Load_N2CC(char *pCityFile);
int   LoadExCities(char *pCityFile);
int   LoadCities(char *pCityFile);     // Load ???_N2C.CSV
int   Load_N2CX(char *pCityFile);      // Load ???_N2CX.CSV

int   LoadSuffixTbl(char *pFilename, bool bUseXlat=false);
int   GetSfxCode(char *pSuffix);
int   GetSfxCodeX(char *pOrgSfx, char *pSuffix);
int   GetSfxDev(char *pSuffix);
int   LoadLUTable(char *pLUFile, char *pTableName, LU_ENTRY *pTable, int iMaxEntries);
int   Value2Code(char *pValue, char *pCode, LU_ENTRY *pTable);
int   LoadXrefTable(char *pXrefFile, XREFTBL *pTable, int iMaxEntries);
// Return code index value
int   XrefCode2Idx(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries);
// Compare Code, return index of value in table
int   XrefCodeIndex(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries);
// Compare Name, return index of value in table
int   XrefNameIndex(XREFTBL *pTable, LPCSTR pName, int iMaxEntries);
// Return index of value in table - Match exact code
int   XrefCodeIndexEx(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries);

int   Quality2Code(char *pValue, char *pCode, LU_ENTRY *pTable);
int   Code2Quality(char *pCode, char *pQual, LU_ENTRY *pTable);
char  *Code2Value(char *pCode, LU_ENTRY *pTable, char *pValue=NULL);

char  *GetSfxStr(int iSfxIdx);
char  *GetCityName(int iCityCode);
char  *GetStrDir(char *pDir);
char  *XrefCode2Name(XREFTBL *pTable, LPCSTR pCode, int iMaxEntries);
char  *XrefIdx2Name(XREFTBL *pTable, int iIdx, int iMaxEntries);

STRSFX *GetSfxCode(char *pSuffix, STRSFX *pSfxTbl);

int   City2Code(char *pCityName, char *pCityCode, char *pApn=NULL);
int   City2Code2(char *pCityName, char *pCityCode, char *pApn=NULL);
int   City2CodeEx(char *pInCity, char *pCityCode, char *pOutCity, char *pApn=NULL);

int   Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName=NULL, char *pApn=NULL);
int   Abbr2Code(char *pCityAbbr, char *pCityCode, char *pCityName, int iZipCode, char *pApn=NULL);
int   Abbr2CZ(char *pCityAbbr, char *pCityCode, char *pCityName, char *pZipCode, char *pApn=NULL);

void  Abbr2City(char *pCityAbbr, char *pCityName);
void  Sfx2Code(char *pSuffix, char *pSfxCode);
void  InitDirs();

void  Code2Sfx(char *pSfxCode, char *pSuffix);
void  Code2City(char *pCityCode, char *pCity);
void  Code2CityEx(char *pCityCode, char *pCity);
char *Code2City(int iCityCode);

char *GetParcelStatus(char *pCode);
char *City2Zip(char *pCityName, char *pZipCode);

#ifndef _TABLES_DEF
   extern int iBadCity, iBadSuffix;
#endif