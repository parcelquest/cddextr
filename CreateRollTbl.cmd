@echo CreateRollTbl %1 %2 %3
@echo off
if "%1" == "" goto Usage
if "%2" == "" goto DefaultServer
sqlcmd -S %2 -v Cnty=%1 -i c:\tools\cddextr\cmdCreateRollTbl.sql
sqlcmd -S %2 -v Cnty=%1 -i c:\tools\cddextr\cmdCreateIndex.sql
sqlcmd -S %2 -v Cnty=%1 -i c:\tools\cddextr\cmdCreateFTIndex.sql
bcp spatial.dbo.%1_Roll in \\%computername%\CO_IMPORT\Web\%1_Import.csv -c -e  C:\Tools\Logs\CddExtr\%1_Import.log -T -t"|" -r\n -a65535 -k -F2 -q -U pq -P cdd -S %2
sqlcmd -S %2 -v Cnty=%1 -i c:\tools\cddextr\cmdUpdateGeompt.sql
goto exitcmd
:DefaultServer
sqlcmd -v Cnty=%1 -i c:\tools\cddextr\cmdCreateRollTbl.sql
sqlcmd -v Cnty=%1 -i c:\tools\cddextr\cmdCreateIndex.sql
sqlcmd -v Cnty=%1 -i c:\tools\cddextr\cmdCreateFTIndex.sql
bcp spatial.dbo.%1_Roll in \\%computername%\CO_IMPORT\Web\%1_Import.csv -c -e  C:\Tools\Logs\CddExtr\%1_Import.log -T -t"|" -r\n -a65535 -k -F2 -q
sqlcmd -v Cnty=%1 -i c:\tools\cddextr\cmdUpdateGeompt.sql

goto exitcmd
:Usage
@echo ........................
@echo Usage: CreateRollTbl {county code} [sqlserver]
@echo ........................

:exitcmd

